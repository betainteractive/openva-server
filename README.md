openva-server
=============

Server application for Verbal Autopsy collection using WHO VA Forms (2016)

Current Version: 1.0

Using CHAMPS WHO VA Form Version: 2.0.0.4