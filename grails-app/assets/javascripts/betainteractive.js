function isIE(){
    return (navigator.appName=="Microsoft Internet Explorer");
}

function browserLayouts() {
    if (!isIE()) {
        var elem = document.getElementById("content");

        elem.style.paddingLeft = '6px';
        elem.style.paddingRight = '6px';

    }
}

function cssLayouts(){
    var content_elem = document.getElementById('container');
    var leftc  = document.getElementById('leftcolumn');
    var rightc = document.getElementById('rightcolumn');

    //alert("stylecH: "+content_elem.clientHeight+" sh: "+content_elem.style.height);
    if (isIE()){
        leftc.style.height = content_elem.clientHeight+'px';
        rightc.style.height = content_elem.clientHeight+'px';
    }else {
        var nr = new Number(content_elem.clientHeight);
        nr = nr+10;
        leftc.style.height = nr + 'px';
        rightc.style.height = nr + 'px';
        //alert('running '+nr);
    }

}


//Functions for Cascade Selects

function addToSelect(rselect, obj){
    var optx = document.createElement('option');

    if (obj){
        optx.text = obj.nome
        optx.value = obj.id
    }

    try {
        rselect.add(optx, null); // standards compliant; doesn't work in IE
    }catch(ex) {
        rselect.add(optx); // IE only
    }
}

function addToAndSelect(rselect, obj){
    var optx = document.createElement('option');

    if (obj){
        optx.text = obj.nome
        optx.value = obj.id

        try {
            rselect.add(optx, null); // standards compliant; doesn't work in IE
        }catch(ex) {
            rselect.add(optx); // IE only
        }

        rselect.value = obj.id
    }
}

//Implementar onload, para os selects
function clearSelectElement(rselect){
    if (rselect){
        rselect.options.length = 0

        // Clear all previous options
        var l = rselect.length

        //while (l > 0) {
        //    l--
        //    rselect.remove(l)
        //}
        //rselect.innerHTML("");

        var optx = document.createElement('option');
        optx.text = ''
        optx.value = null

        try {
            rselect.add(optx, null); // standards compliant; doesn't work in IE
        } catch(ex) {
            rselect.add(optx); // IE only
        }
    }
}

function clearSelect(elemId){
    var rselect = document.getElementById(elemId)
    clearSelectElement(rselect)
}

function updateElement(data, elemId) {
    // The response comes back as a bunch-o-JSON
    var value = data

    if (value) {
        var elem = document.getElementById(elemId)
        elem.html(value.nome)
    }else{
        alert("sem valores")
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * UpdateSelect, works on every select of the project
 * the_url - url to be executed
 * element_id - the id/name of the select component
 *

 *
 *
 */
$(window).load(
    function () {
        //alert("testing jquery");

        var id = $("#province\\.id").val();
        if (id != undefined){
            UpdateDistrict(id);
        }

        var idContinent = $("#continent\\.id").val();
        if (idContinent != undefined){
            UpdateCountry(idContinent);
        }

    }
);


$(document).ready(
    function(){
        var elementId = "";

        //Forms gerais,
        //Limpar as dependencias
        $("#province\\.id").change(
            function(){
                //clearSelect("distrito.id");
                clearSelect("administrativePost.id");
                clearSelect("locality.id");
            }
        );
        $("#district\\.id").change(
            function(){
                clearSelect("locality.id");
            }
        );
        /*
        $("#administrativePost\\.id").change(
            function(){
                clearSelect("bairro.id");
            }
        );
        */


    }
);

function UpdateCountry(elemid, id){
    var url = "../localization/countries/"+id;
    if (document.getElementById(elemid) == null) return;
    UpdateSelect(url, elemid);
}

function UpdateDistrict(elemid, id){
    var url = "../localization/districts/"+id;
    if (document.getElementById(elemid) == null) return;
    UpdateSelect(url, elemid);
}

function UpdateAdministrativePost(elemid, id){
    var url = "../localization/administrativePosts/"+id;
    if (document.getElementById(elemid) == null) return;
    UpdateSelect(url, elemid);
}

function UpdateLocality(elemid, id){
    var url = "../localization/localities/"+id;
    if (document.getElementById(elemid) == null) return;
    UpdateSelect(url, elemid);
}

function UpdateSelect(the_url, element_id){
    //alert("its working id update general x");
    //alert("url: "+the_url+", id: "+element_id)
    $.ajax({
        url: the_url,
        dataType:"json",
        success: function(json) {

            var rselect = document.getElementById(element_id);

            // Clear all previous options
            clearSelectElement(rselect)

            for (var i=0; i < json.length; i++) {
                var obj = json[i];
                
                var opt = document.createElement('option');
                opt.text = obj.name;
                opt.value = obj.id
                try {
                    rselect.add(opt, null) // standards compliant; doesn't work in IE
                }catch(ex) {
                    rselect.add(opt) // IE only
                }
            }
        },
        error: function(xhr){
            alert("Failed to save patient!: " +
                xhr.status + ", " + xhr.statusText );
        }

    });
}

function updateSelect(data, element_id){
    //alert("data: "+data+", id="+element_id)

    if (data){

        var rselect = document.getElementById(element_id);
    // Clear all previous options
        clearSelectElement(rselect)

        for (var i=0; i < data.length; i++) {
            var obj = data[i];

            var opt = document.createElement('option');
            opt.text = obj.name;
            opt.value = obj.id
            try {
                rselect.add(opt, null) // standards compliant; doesn't work in IE
            }catch(ex) {
                rselect.add(opt) // IE only
            }
        }
    }

}

function updateSelectSData(data, element_id){
    //alert("data: "+data+", id="+element_id)

    if (data){

        var rselect = document.getElementById(element_id);
        // Clear all previous options
        clearSelectElement(rselect)

        for (var i=0; i < data.length; i++) {
            var obj = data[i];

            var opt = document.createElement('option');
            opt.text = obj; //.name;
            opt.value = obj; //.id
            try {
                rselect.add(opt, null) // standards compliant; doesn't work in IE
            }catch(ex) {
                rselect.add(opt) // IE only
            }
        }
    }

}

function updateTagHorario(data, element_id){
    //alert("data: "+data+", id="+element_id)

    if (data){

        var selHour = document.getElementById(element_id+"_hour");
        var selMin = document.getElementById(element_id+"_minute");

        var trEntrada = document.getElementById("tr_entrada2");
        var trSaida = document.getElementById("tr_saida1");

        // Clear all previous options
        //clearSelectElement(rselect)
        //alert("data: "+data.checked+", cmp: "+trEntrada);

        if (data.checked==true){
            trEntrada.style.visibility = 'visible';
            trSaida.style.visibility = 'visible';
        }else{
            trEntrada.style.visibility = 'hidden';
            trSaida.style.visibility = 'hidden';
        }
    }

}