package net.openva.server

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/login/$action?"(controller: "login")
        "/logout/$action?"(controller: "logout")
        "/logout/index"(controller: "logout")

        "400"(view: '/error')
        "401"(view: '/error')
        "500"(view:'/error')

        //REST Api access
        "/api/export/individuals"(controller: "exportFiles", action: "individuals")
        "/api/export/users"(controller: "exportFiles", action: "users")
        "/api/export/vacontrols"(controller: "exportFiles", action: "vacontrols")
        "/api/export/stats"(controller: "exportFiles", action: "stats")

        "/api/export/individuals/zip"(controller: "exportFiles", action: "individualsZip")
        "/api/export/users/zip"(controller: "exportFiles", action: "usersZip")
        "/api/export/vacontrols/zip"(controller: "exportFiles", action: "vacontrolsZip")
        "/api/export/stats/zip"(controller: "exportFiles", action: "statsZip")

        "/api/login"(controller: "generalUtilities", action: "loginTest")
    }
}
