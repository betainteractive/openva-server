package net.openva.server.controllers

import grails.rest.RestfulController
import net.openva.io.SystemPath
import net.openva.server.Codes
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogStatus

class ExportFilesController {

    static responseFormats = ['xml']

    def exportFilesService

    def individuals = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"individuals.xml")
        render file: file
    }

    def users = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"users.xml")
        render file: file
    }

    def vacontrols = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"vacontrols.xml")
        render file: file
    }

    def stats = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"vastats.xml")
        render file: file
    }

    def individualsZip = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"individuals.zip")
        render file: file, fileName: "individuals.zip"
    }

    def usersZip = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"users.zip")
        render file: file, fileName: "users.zip"
    }

    def vacontrolsZip = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"vacontrols.zip")
        render file: file, fileName: "vacontrols.zip"
    }

    def statsZip = {
        def file = new File(SystemPath.getAbsoluteResourcesPath()+File.separator+"vastats.zip")
        render file: file, fileName: "vastats.zip"
    }

    def index = {

        def logReports = LogReport.executeQuery("select lr from LogReport lr where lr.group.groupId=?", [Codes.GROUP_GENERATE_FILES])

        render view: "index", model: [logReports : logReports]
    }

    def export = {
        LogReport logReport = null

        LogReport.withTransaction {
            logReport = LogReport.get(params.id)
            logReport.status = LogStatus.findByName(LogStatus.STARTED)
            logReport.start = new Date();
            logReport.save(flush: true)
        }


        println "log ${logReport.errors}"

        def id = logReport.id

        if (logReport.reportId==Codes.REPORT_GENERATE_USERS_ZIP_XML_FILES){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing export files to user xml/zip"
                    exportFilesService.generateUsersXML(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_GENERATE_INDIVIDUALS_ZIP_XML_FILES){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing export files to individuals xml/zip"
                    exportFilesService.generateLiveIndividualXML(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_GENERATE_VACONTROL_ZIP_XML_FILES){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing export files to vacontrol xml/zip"
                    exportFilesService.generateVaControlXML(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_GENERATE_STATS_ZIP_XML_FILES){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing export files to stats xml/zip"
                    exportFilesService.generateVaStatsXML(id)
                }
            }).start()
        }

        redirect action:'index'
    }
}
