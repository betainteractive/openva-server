package net.openva.server.controllers

import net.openva.server.Codes
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogStatus

class ImportDataController {

    def dynamicOdkSynchronizationService
    def importDataService

    def index = {
        def logReports = LogReport.executeQuery("select lr from LogReport lr where lr.group.groupId=? order by lr.reportId", [Codes.GROUP_IMPORT_DATA])

        render view: "index", model: [logReports : logReports]
    }

    def importFromOpenHds = {
        LogReport logReport = null

        LogReport.withTransaction {
            logReport = LogReport.get(params.id)
            logReport.status = LogStatus.findByName(LogStatus.STARTED)
            logReport.start = new Date();
            logReport.save(flush: true)
        }

        def id = logReport.id

        if (logReport.reportId==Codes.REPORT_IMPORT_FROM_ODK_CORES){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing transfer from odk to final table"
                    dynamicOdkSynchronizationService.executeTransfer(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_IMPORT_FROM_OPENHDS_FIELDWORKERS){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing transfer from openhds fieldworker to user final table"
                    importDataService.importFieldWorkersFromOpenHDS(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_IMPORT_FROM_OPENHDS_INDIVIDUALS){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing transfer from openhds individuals to final table"
                    importDataService.importLiveIndividualsFromOpenHDS(id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_IMPORT_FROM_OPENHDS_DEATHS){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing transfer from openhds deaths to final table"
                    importDataService.importDeathsFromOpenHDS(id)
                }
            }).start()
        }

        redirect (action: "index")
    }
}
