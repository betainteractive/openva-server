package net.openva.server.controllers

import net.openva.server.Codes
import net.openva.server.model.VerbalAutopsyStats
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus

class ReportsController {

    def verbalAutopsyControlService

    def index() {
        def logReports = LogReport.executeQuery("select lr from LogReport lr where lr.group.groupId in (:list) order by lr.reportId", [list: [Codes.GROUP_STATS]])

        def stats = VerbalAutopsyStats.last()

        stats = stats==null ? new VerbalAutopsyStats() : stats

        render view: "index", model: [logReports : logReports, stats: stats]
    }

    def showLogReport(){
        def logReportInstance = LogReport.get(params.id)

        def logFiles = LogReportFile.executeQuery("select f from LogReportFile f where f.logReport=? order by f.creationDate desc", [logReportInstance])

        render view:"showLogReport", model : [logReportInstance : logReportInstance, logFiles: logFiles]
    }

    def downloadLogFile = {

        def logReportFile = LogReportFile.get(params.id)

        File file = new File(logReportFile.fileName)

        if (file.exists()) {
            response.setContentType("text/plain") // or or image/JPEG or text/xml or whatever type the file is
            response.setHeader("Content-disposition", "attachment;filename=\"${file.name}\"")
            response.outputStream << file.bytes
        } else {
            render "${message(code: "default.file.not.found")} - ${logReportFile.fileName}"
        }

    }

    def executeUpdateStats = {

        LogReport logReport = null

        LogReport.withTransaction {
            logReport = LogReport.get(params.id)
            logReport.status = LogStatus.findByName(LogStatus.STARTED)
            logReport.start = new Date();
            logReport.save(flush: true)
        }

        if (logReport.reportId==Codes.REPORT_VERBAL_AUTOPSY_STATS){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing update of verbal autopsy stats"
                    verbalAutopsyControlService.calculateVaStats(logReport.id)
                }
            }).start()
        }

        redirect (action: "index")
    }
}
