package net.openva.server.controllers

import net.openva.server.Codes
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogStatus

class UpdatesController {

    def verbalAutopsyControlService

    def index = {
        def logReports = LogReport.executeQuery("select lr from LogReport lr where lr.group.groupId in (:list) order by lr.reportId", [list: [Codes.GROUP_UPDATE_VA, Codes.GROUP_STATS]])

        render view: "index", model: [logReports : logReports]
    }

    def addDeaths = {
        redirect controller: "verbalAutopsyControl", action: "create"
    }

    def addIndividuals = {
        redirect controller: "individual", action: "create"
    }

    def executeUpdate = {

        LogReport logReport = null

        LogReport.withTransaction {
            logReport = LogReport.get(params.id)
            logReport.status = LogStatus.findByName(LogStatus.STARTED)
            logReport.start = new Date();
            logReport.save(flush: true)
        }


        if (logReport.reportId==Codes.REPORT_UPDATE_VERBAL_AUTOPSY_CONTROL){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing update of verbal autopsy control"
                    verbalAutopsyControlService.updateVaControl(logReport.id)
                }
            }).start()
        }

        if (logReport.reportId==Codes.REPORT_VERBAL_AUTOPSY_STATS){
            new Thread(new Runnable() {
                @Override
                void run() {
                    println "executing update of verbal autopsy stats"
                    verbalAutopsyControlService.calculateVaStats(logReport.id)
                }
            }).start()
        }

        redirect (action: "index")
    }
}
