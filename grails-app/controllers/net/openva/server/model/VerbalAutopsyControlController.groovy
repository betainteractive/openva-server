package net.openva.server.model



import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional

@Transactional(readOnly = true)
class VerbalAutopsyControlController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond VerbalAutopsyControl.list(params), model:[verbalAutopsyControlInstanceCount: VerbalAutopsyControl.count()]
    }

    def show(VerbalAutopsyControl verbalAutopsyControlInstance) {
        respond verbalAutopsyControlInstance
    }

    def create() {
        respond new VerbalAutopsyControl(params)
    }

    @Transactional
    def save(VerbalAutopsyControl verbalAutopsyControlInstance) {
        if (verbalAutopsyControlInstance == null) {
            notFound()
            return
        }

        if (verbalAutopsyControlInstance.hasErrors()) {
            respond verbalAutopsyControlInstance.errors, view:'create'
            return
        }

        verbalAutopsyControlInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'verbalAutopsyControl.label', default: 'VerbalAutopsyControl'), verbalAutopsyControlInstance.id])
                redirect verbalAutopsyControlInstance
            }
            '*' { respond verbalAutopsyControlInstance, [status: CREATED] }
        }
    }

    def edit(VerbalAutopsyControl verbalAutopsyControlInstance) {
        respond verbalAutopsyControlInstance
    }

    @Transactional
    def update(VerbalAutopsyControl verbalAutopsyControlInstance) {
        if (verbalAutopsyControlInstance == null) {
            notFound()
            return
        }

        if (verbalAutopsyControlInstance.hasErrors()) {
            respond verbalAutopsyControlInstance.errors, view:'edit'
            return
        }

        verbalAutopsyControlInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'VerbalAutopsyControl.label', default: 'VerbalAutopsyControl'), verbalAutopsyControlInstance.id])
                redirect verbalAutopsyControlInstance
            }
            '*'{ respond verbalAutopsyControlInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(VerbalAutopsyControl verbalAutopsyControlInstance) {

        if (verbalAutopsyControlInstance == null) {
            notFound()
            return
        }

        verbalAutopsyControlInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'VerbalAutopsyControl.label', default: 'VerbalAutopsyControl'), verbalAutopsyControlInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'verbalAutopsyControl.label', default: 'VerbalAutopsyControl'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
