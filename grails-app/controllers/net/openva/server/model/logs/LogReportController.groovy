package net.openva.server.model.logs

class LogReportController {

    def show(LogReport logReportInstance) {
        redirect controller: "reports", action: "showLogReport", id: logReportInstance.id
    }
}
