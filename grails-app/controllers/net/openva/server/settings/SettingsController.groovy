package net.openva.server.settings

import net.betainteractive.utilities.GeneralUtil
import net.betainteractive.utilities.StringUtil

class SettingsController {

    def index() {

        if (ApplicationParam.count()==0){
            flash.message = message(code: 'settings.updated.label')
            redirect action: "index"
            return
        }

        def paramInstances = ApplicationParam.list();
        render view: "params", model: [paramInstances:paramInstances]
    }

    def update = {
        //println "params: ${params}"
        //println "params ids: ${params['id']}"

        if (ApplicationParam.count()==0){
            flash.message = message(code: 'settings.updated.label')
            redirect action: "index"
            return
        }

        //load values from params
        def param_ids = params['id']

        int date_index = 0;
        param_ids.eachWithIndex{ sid, int i ->

            long id = Long.parseLong(sid)
            def ap = ApplicationParam.get(id)

            String msg = message(code: ap.name)
            String value = params["value${i}"]

            if (ap.type.equals("date")){

                def year = Integer.parseInt( params["value${i}_year"])
                def month = Integer.parseInt( params["value${i}_month"])
                def day = Integer.parseInt( params["value${i}_day"])


                def date = GeneralUtil.getDate(year, month, day)
                value = StringUtil.format(date, "yyyy-MM-dd")
            }

            if (ap.type.equals("integer")){
                if (StringUtil.isInteger(value)){
                    flash.message = message(code: 'settings.format.integer.error', args: [value, msg])
                    redirect action: "index"
                    return
                }
            }

            if (value.isEmpty() || value==null){
                flash.message = message(code: 'settings.format.empty.error')
                redirect action: "index"
                return
            }

            ap.value = value
            ap.save(flush: true)

            //println "param: ${ap.name} : ${value}, ${ap.errors}"
        }



        flash.message = message(code: 'settings.updated.label')
        redirect action: "index"
    }
}
