package net.openva.odk.model

class Whova2016ConsentedNarratId10476aRef {
	//static mapWith = "none"

	String uri
	String creatorUriUser
	Date creationDate
	String lastUpdateUriUser
	Date lastUpdateDate
	String domAuri
	String subAuri
	String topLevelAuri
	Integer part

	static mapping = {
		datasource 'odk'
		table 'WHOVA2016_CONSENTED_NARRAT_ID10476A_REF'

		id name: "uri", generator: "assigned"
		version false
		uri column:'_URI'
		creatorUriUser column:'_CREATOR_URI_USER'
		creationDate column:'_CREATION_DATE'
		lastUpdateUriUser column:'_LAST_UPDATE_URI_USER'
		lastUpdateDate column:'_LAST_UPDATE_DATE'
		domAuri column:'_DOM_AURI'
		subAuri column:'_SUB_AURI'
		topLevelAuri column:'_TOP_LEVEL_AURI'
		part column:'PART'
	}

	static constraints = {
		uri maxSize: 80
		creatorUriUser maxSize: 80
		lastUpdateUriUser nullable: true, maxSize: 80
		domAuri maxSize: 80
		subAuri maxSize: 80
		topLevelAuri nullable: true, maxSize: 80
	}
}
