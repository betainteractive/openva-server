package net.openva.odk.model

class Whova2016ConsentedNarratId10479 {
	//static mapWith = "none"

	String uri
	String creatorUriUser
	Date creationDate
	String lastUpdateUriUser
	Date lastUpdateDate
	String parentAuri
	Integer ordinalNumber
	String topLevelAuri
	String value

	static mapping = {
		datasource 'odk'
		table 'WHOVA2016_CONSENTED_NARRAT_ID10479'

		id name: "uri", generator: "assigned"
		version false
		uri column:'_URI'
		creatorUriUser column:'_CREATOR_URI_USER'
		creationDate column:'_CREATION_DATE'
		lastUpdateUriUser column:'_LAST_UPDATE_URI_USER'
		lastUpdateDate column:'_LAST_UPDATE_DATE'
		parentAuri column:'_PARENT_AURI'
		ordinalNumber column:'_ORDINAL_NUMBER'
		topLevelAuri column:'_TOP_LEVEL_AURI'
		value column:'VALUE'
	}

	static constraints = {
		uri maxSize: 80
		creatorUriUser maxSize: 80
		lastUpdateUriUser nullable: true, maxSize: 80
		parentAuri nullable: true, maxSize: 80
		topLevelAuri nullable: true, maxSize: 80
	}
}
