package net.openva.odk.model

class Whova2016Core {
	//static mapWith = "none"

	String uri
	String creatorUriUser
	Date creationDate
	String lastUpdateUriUser
	Date lastUpdateDate
	Integer modelVersion
	Integer uiVersion
	Character isComplete
	Date submissionDate
	Date markedAsCompleteDate

	Integer processed

	static mapping = {
		datasource 'odk'
		table 'WHOVA2016_CORE'

		id name: "uri", generator: "assigned"
		version false

		uri column:'_URI'
		creatorUriUser column:'_CREATOR_URI_USER'
		creationDate column:'_CREATION_DATE'
		lastUpdateUriUser column:'_LAST_UPDATE_URI_USER'
		lastUpdateDate column:'_LAST_UPDATE_DATE'
		modelVersion column:'_MODEL_VERSION'
		uiVersion column:'_UI_VERSION'
		isComplete column:'_IS_COMPLETE'
		submissionDate column:'_SUBMISSION_DATE'
		markedAsCompleteDate column:'_MARKED_AS_COMPLETE_DATE'

		processed column: "PROCESSED"

	}

	static constraints = {
		uri maxSize: 80
		creatorUriUser maxSize: 80
		lastUpdateUriUser nullable: true, maxSize: 80
		modelVersion nullable: true
		uiVersion nullable: true
		isComplete nullable: true, maxSize: 1
		submissionDate nullable: true
		markedAsCompleteDate nullable: true

		processed nullable: true
	}
}
