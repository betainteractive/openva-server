package net.openva.openhds.model

class Death {
	//static mapWith = "none"

	String uuid
	Boolean deleted
	Date insertDate
	Date voidDate
	String voidReason
	String status
	String statusMessage
	Long ageAtDeath
	String deathCause
	Date deathDate
	String deathPlace
	String insertByUuid
	String voidByUuid
	String visitDeathUuid
	Fieldworker fieldworker
	String individualUuid


	static mapping = {
		datasource 'openhds'
		table 'death'

		id name: "uuid", generator: "assigned"
		version false
		uuid column:'uuid'
		deleted column:'deleted'
		insertDate column:'insertDate'
		voidDate column:'voidDate'
		voidReason column:'voidReason'
		status column:'status'
		statusMessage column:'statusMessage'
		ageAtDeath column:'ageAtDeath'
		deathCause column:'deathCause'
		deathDate column:'deathDate'
		deathPlace column:'deathPlace'
		insertByUuid column:'insertBy_uuid'
		voidByUuid column:'voidBy_uuid'
		visitDeathUuid column:'visitDeath_uuid'
		individualUuid column: "individual_uuid"
	}

	static constraints = {
		uuid maxSize: 32
		insertDate nullable: true
		voidDate nullable: true
		voidReason nullable: true
		status nullable: true
		statusMessage nullable: true
		ageAtDeath nullable: true
		deathCause nullable: true
		deathPlace nullable: true
		insertByUuid nullable: true, maxSize: 32
		voidByUuid nullable: true, maxSize: 32
		visitDeathUuid nullable: true, maxSize: 32
		individualUuid nullable: true, maxSize: 32
	}
}
