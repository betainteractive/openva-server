package net.openva.openhds.model

class Fieldworker {
	//static mapWith = "none"

	String uuid
	Boolean deleted
	Date insertDate
	Date voidDate
	String voidReason
	String extId
	String firstName
	String lastName
	String passwordHash
	String insertByUuid
	String voidByUuid

	static mapping = {
		datasource 'openhds'
		table 'fieldworker'

		id name: "uuid", generator: "assigned"
		version false
		uuid column:'uuid'
		deleted column:'deleted'
		insertDate column:'insertDate'
		voidDate column:'voidDate'
		voidReason column:'voidReason'
		extId column:'extId'
		firstName column:'firstName'
		lastName column:'lastName'
		passwordHash column:'passwordHash'

		insertByUuid column:'insertBy_uuid'
		voidByUuid column:'voidBy_uuid'
	}

	static constraints = {
		uuid maxSize: 32
		insertDate nullable: true
		voidDate nullable: true
		voidReason nullable: true
		firstName nullable: true
		lastName nullable: true
		insertByUuid nullable: true, maxSize: 32
		voidByUuid nullable: true, maxSize: 32
	}
}
