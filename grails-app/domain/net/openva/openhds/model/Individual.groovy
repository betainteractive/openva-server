package net.openva.openhds.model

class Individual {
	//static mapWith = "none"

	String uuid
	Boolean deleted
	Date insertDate
	Date voidDate
	String voidReason
	String status
	String statusMessage
	Date dob
	String dobAspect
	String extId
	String firstName
	String gender
	String lastName
	String middleName
	String insertByUuid
	String voidByUuid

	Individual mother
	Fieldworker fieldworker
	Individual father

	static hasMany = [individualsForFatherUuid: Individual,
	                  individualsForMotherUuid: Individual]

	static mappedBy = [individualsForFatherUuid: "father",
					   individualsForMotherUuid: "mother"]

	static mapping = {
		datasource 'openhds'
		table 'individual'

		id name: "uuid", generator: "assigned"
		version false
		uuid column:'uuid'
		deleted column:'deleted'
		insertDate column:'insertDate'
		voidDate column:'voidDate'
		voidReason column:'voidReason'
		status column:'status'
		statusMessage column:'statusMessage'
		dob column:'dob'
		dobAspect column:'dobAspect'
		extId column:'extId'
		firstName column:'firstName'
		gender column:'gender'
		lastName column:'lastName'
		middleName column:'middleName'


		fieldworker column: 'collectedBy_uuid'
		mother column: 'mother_uuid'
		father column: 'father_uuid'
	}

	static constraints = {
		uuid maxSize: 32
		insertDate nullable: true
		voidDate nullable: true
		voidReason nullable: true
		status nullable: true
		statusMessage nullable: true
		dob nullable: true
		dobAspect nullable: true
		extId nullable: true
		firstName nullable: true
		gender nullable: true
		lastName nullable: true
		middleName nullable: true
		insertByUuid nullable: true, maxSize: 32
		voidByUuid nullable: true, maxSize: 32
	}
}
