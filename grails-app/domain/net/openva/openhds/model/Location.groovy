package net.openva.openhds.model

class Location {
	//static mapWith = "none"

	String uuid
	Boolean deleted
	Date insertDate
	Date voidDate
	String voidReason
	String status
	String statusMessage
	String accuracy
	String altitude
	String extId
	String latitude
	String locationName
	String locationType
	String longitude
	String insertByUuid
	String voidByUuid
	String collectedByUuid
	String locationLevelUuid

	static hasMany = [residencies: Residency]

	static mapping = {
		datasource 'openhds'
		table 'location'

		id name: "uuid", generator: "assigned"
		version false
		uuid column:'uuid'
		deleted column:'deleted'
		insertDate column:'insertDate'
		voidDate column:'voidDate'
		voidReason column:'voidReason'
		status column:'status'
		statusMessage column:'statusMessage'
		accuracy column:'accuracy'
		altitude column:'altitude'
		extId column:'extId'
		latitude column:'latitude'
		locationName column:'locationName'
		locationType column:'locationType'
		longitude column:'longitude'
		insertByUuid column:'insertBy_uuid'
		voidByUuid column:'voidBy_uuid'
		collectedByUuid column:'collectedBy_uuid'
		locationLevelUuid column:'locationLevel_uuid'
	}

	static constraints = {
		uuid maxSize: 32
		insertDate nullable: true
		voidDate nullable: true
		voidReason nullable: true
		status nullable: true
		statusMessage nullable: true
		accuracy nullable: true
		altitude nullable: true
		latitude nullable: true
		locationName nullable: true
		locationType nullable: true
		longitude nullable: true
		insertByUuid nullable: true, maxSize: 32
		voidByUuid nullable: true, maxSize: 32
		collectedByUuid maxSize: 32
		locationLevelUuid nullable: true, maxSize: 32
	}
}
