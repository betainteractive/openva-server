package net.openva.openhds.model

class Residency {
	//static mapWith = "none"

	String uuid
	Boolean deleted
	Date insertDate
	Date voidDate
	String voidReason
	String status
	String statusMessage
	Date endDate
	String endType
	Date startDate
	String startType
	String insertByUuid
	String voidByUuid
	String collectedByUuid
	String individualUuid
	Location location

	static belongsTo = [Location]

	static mapping = {
		datasource 'openhds'
		table 'residency'

		id name: "uuid", generator: "assigned"
		version false
		uuid column:'uuid'
		deleted column:'deleted'
		insertDate column:'insertDate'
		voidDate column:'voidDate'
		voidReason column:'voidReason'
		status column:'status'
		statusMessage column:'statusMessage'
		endDate column:'endDate'
		endType column:'endType'
		startDate column:'startDate'
		startType column:'startType'
		insertByUuid column:'insertBy_uuid'
		voidByUuid column:'voidBy_uuid'
		collectedByUuid column:'collectedBy_uuid'
		individualUuid column:'individual_uuid'
		location column: 'location_uuid'
	}

	static constraints = {
		uuid maxSize: 32
		insertDate nullable: true
		voidDate nullable: true
		voidReason nullable: true
		status nullable: true
		statusMessage nullable: true
		endDate nullable: true
		endType nullable: true
		startType nullable: true
		insertByUuid nullable: true, maxSize: 32
		voidByUuid nullable: true, maxSize: 32
		collectedByUuid maxSize: 32
		individualUuid nullable: true, maxSize: 32
	}
}
