package net.openva.security.authentication

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

	private static final long serialVersionUID = 1

	transient generalUtilitiesService

	static String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR"
	static String ROLE_DATA_MANAGER = "ROLE_DATA_MANAGER"
	static String ROLE_FIELD_WORKER = "ROLE_FIELD_WORKER"

	String name
	String authority

	String toString(){
		generalUtilitiesService.getMessage(name, "")
	}

	Role(String authority) {
		this()
		this.authority = authority
	}

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
