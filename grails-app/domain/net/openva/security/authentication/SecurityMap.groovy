package net.openva.security.authentication

import org.springframework.http.HttpMethod

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes=['configAttribute', 'httpMethod', 'url'])
@ToString(includes=['configAttribute', 'httpMethod', 'url'], cache=true, includeNames=true, includePackage=false)
class SecurityMap implements Serializable {

	private static final long serialVersionUID = 1

	String url
	String configAttribute
	HttpMethod httpMethod


	SecurityMap(String url, String configAttribute, HttpMethod httpMethod = null) {
		this()
		this.url = url
		this.configAttribute = configAttribute
		this.httpMethod = httpMethod

	}

	static constraints = {
		configAttribute blank: false
		url blank: false, unique: 'httpMethod'
		httpMethod nullable: true
	}

	static mapping = {
		cache true
	}
}
