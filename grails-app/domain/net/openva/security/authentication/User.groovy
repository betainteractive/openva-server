package net.openva.security.authentication

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

	private static final long serialVersionUID = 1

	transient springSecurityService

	String firstName
	String lastName
	String username
	String password

	boolean isPasswordEncoded

	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	User createdBy
	Date creationDate
	User updatedBy
	Date updatedDate

	/*Collection Status*/
	int totalCollected
    int totalAttempts

	String toString(){
		return firstName + " " + lastName
	}

	User(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this)*.role
	}

	String getAuthoritiesText(){
		def text = ""
		getAuthorities().eachWithIndex { it, index ->
			if (index==0){
				text = it.name
			}else{
				text += ", ${it.name}"
			}
		}
	}

	def beforeInsert() {
		createdBy  = (User) springSecurityService.currentUser
		creationDate = new Date()
	}

	def beforeUpdate() {
		updatedBy = (User) springSecurityService.currentUser
		updatedDate = new Date()
	}

	static transients = ['springSecurityService','isPasswordEncoded']

	static constraints = {
		firstName blank: false
		lastName blank: false
		username blank: false, unique: true
		password blank: false

		createdBy nullable: true
		creationDate nullable: true
		updatedBy nullable: true
		updatedDate nullable:true

		totalAttempts nullable:false
		totalCollected nullable:false
	}

	static mapping = {
		password column: '`password`'
        totalCollected column: 'total_va_collected', defaultValue: "0"
        totalAttempts column: 'total_va_attempts', defaultValue: "0"
	}
}
