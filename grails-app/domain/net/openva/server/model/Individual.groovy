package net.openva.server.model

import net.betainteractive.utilities.GeneralUtil
import net.betainteractive.utilities.StringUtil

/**
 * This table represents individuals without death registration
 */
class Individual {

    String extId             /** Individual Id from OpenHDS **/
    String code              /** Individual identification code (HDSS site specific code) - can be the same as extId **/
    String name              /** FullName of the individual **/
    String gender            /** Gender of the dead individual **/
    Date dateOfBirth         /** Date of Birth**/
    Integer age                  /** Age of the individual **/

    String motherId          /** Individual Id of the mother from OpenHDS**/
    String motherName        /** mother's name **/
    String fatherId          /** Individual Id of the father from OpenHDS**/
    String fatherName        /** mother's name **/

    String householdId       /** Location Id from OpenHDS **/
    String householdNo       /** Household Number - it can be the Location.locationName from OpenHDS **/
    String gpsAccuracy       /** gps accuracy data can be blank **/
    String gpsAltitude       /** gps altitude data can be blank **/
    String gpsLatitude       /** gps latitude data can be blank **/
    String gpsLongitude      /** gps longitude data can be blank **/

    static mapping = {
        table 'individual'

        id column: "uuid"
        version false

        extId colum: "ext_id"
        code colum: "code"
        name colum: "name"
        gender colum: "gender"
        dateOfBirth colum: "date_of_birth"
        age column: "age"

        motherId colum: "mother_id"
        motherName colum: "mother_name"
        fatherId colum: "father_id"
        fatherName colum: "father_name"

        householdId column: "household_id"
        householdNo column: "household_no"
        gpsAccuracy column: "gps_accuracy"
        gpsAltitude column: "gps_altitude"
        gpsLatitude column: "gps_latitude"
        gpsLongitude column: "gps_longitude"
    }

    static constraints = {
        extId nullable: true
        code nullable: false, blank: false
        name nullable: false, blank: false
        gender blank: false
        dateOfBirth nullable: false
        age nullable: false

        motherId nullable: true
        motherName nullable: true
        fatherId nullable: true
        fatherName nullable: true

        householdId nullable: true
        householdNo nullable: true
        gpsAccuracy nullable: true, blank: true
        gpsAltitude nullable: true, blank: true
        gpsLatitude nullable: true, blank: true
        gpsLongitude nullable: true, blank: true
    }

    def beforeInsert() {
        age = GeneralUtil.getAge(dateOfBirth);
    }

    def beforeUpdate() {
        age = GeneralUtil.getAge(dateOfBirth);
    }

    String toXML(){
        return  ("<individual>") +
                ((extId==null) ?           "<extId />"        : "<extId>${extId}</extId>") +
                ((code==null) ?            "<code />"         : "<code>${code}</code>") +
                ((name==null) ?            "<name />"         : "<name>${name}</name>") +
                ((gender==null) ?          "<gender />"       : "<gender>${gender}</gender>") +
                ((dateOfBirth==null) ?     "<dateOfBirth />"  : "<dateOfBirth>${StringUtil.format(dateOfBirth, "yyyy-MM-dd")}</dateOfBirth>") +
                ((age==null) ?             "<age />"          : "<age>${age}</age>") +
                ((motherId==null) ?        "<motherId />"     : "<motherId>${motherId}</motherId>") +
                ((motherName==null) ?      "<motherName />"   : "<motherName>${motherName}</motherName>") +
                ((fatherId==null) ?        "<fatherId />"     : "<fatherId>${fatherId}</fatherId>") +
                ((fatherName==null) ?      "<fatherName />"   : "<fatherName>${fatherName}</fatherName>") +
                ((householdId==null) ?     "<householdId />"  : "<householdId>${householdId}</householdId>") +
                ((householdNo==null) ?     "<householdNo />"  : "<householdNo>${householdNo}</householdNo>") +
                ((gpsAccuracy==null) ?     "<gpsAccuracy />"  : "<gpsAccuracy>${gpsAccuracy}</gpsAccuracy>") +
                ((gpsAltitude==null) ?     "<gpsAltitude />"  : "<gpsAltitude>${gpsAltitude}</gpsAltitude>") +
                ((gpsLatitude==null) ?     "<gpsLatitude />"  : "<gpsLatitude>${gpsLatitude}</gpsLatitude>") +
                ((gpsLongitude==null) ?    "<gpsLongitude />" : "<gpsLongitude>${gpsLongitude}</gpsLongitude>") +
                ("</individual>")
    }
}
