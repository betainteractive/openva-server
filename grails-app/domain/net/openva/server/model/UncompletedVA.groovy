package net.openva.server.model

import net.openva.security.authentication.User

/**
 * Records the attempt to collect an VA specifying a reason
 */
class UncompletedVA {
    VerbalAutopsyControl control  /** A reference to the autopsy collected **/
    WhoVa2016 autopsy
    User fieldWorker
    String reason                 /** Reason to not collect or complete the VA **/

    String toString(){
        "${control}, user->${fieldWorker.username}"
    }

    static constraints = {
        control nullable: true
        autopsy nullable: true
        fieldWorker nullable: true
        reason nullable: true, blank: true
    }

    static mapping = {
        table 'uncompleted_va'

        id column: "id"
        version false

        control column: "va_control_id"
        autopsy colum: "autopsy_id"
        fieldWorker column: "field_worker_id"
        reason colum: "reason"
    }
}
