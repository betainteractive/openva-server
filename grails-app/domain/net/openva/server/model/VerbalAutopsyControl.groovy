package net.openva.server.model

import net.betainteractive.utilities.GeneralUtil
import net.betainteractive.utilities.StringUtil

/**
 * Verbal Autopsy Control Table - also it is a death registration like table
 */
class VerbalAutopsyControl {
    //static mapWith = "none"

    String extId             /** Individual Id from OpenHDS **/
    String code              /** Individual identification code (HDSS site specific code) - can be the same as extId **/
    String name              /** FullName of the individual **/
    String gender            /** Gender of the dead individual **/
    Date dateOfBirth         /** Date of Birth**/
    Date dateOfDeath         /** Date of Death **/
    Integer ageAtDeath           /** Age of the individual at death **/

    String motherId          /** Individual Id of the mother from OpenHDS **/
    String motherName        /** mother's name **/
    String fatherId          /** Individual Id of the father from OpenHDS **/
    String fatherName        /** mother's name **/

    String householdId       /** Location Id from OpenHDS **/
    String householdNo       /** Household Number - it can be the Loaction.locationName from OpenHDS **/

    String gpsAccuracy       /** gps accuracy data can be blank **/
    String gpsAltitude       /** gps altitude data can be blank **/
    String gpsLatitude       /** gps latitude data can be blank **/
    String gpsLongitude      /** gps longitude data can be blank **/

    String deathUuid         /** a reference to openhds death using the uuid - its blank if doesnt have reference **/

    Integer vaType           /** verbal autopsy type - neonatal, child or adult **/
    String  vaUuid           /** a reference to the collected verbal autopsy final table uuid/uri **/
    Integer vaCollected = 0  /** represent the va collection status - 0-not_collected, 1-collected, 2-collected-attempt (can be send to tablet for collection) **/
    String  vaProcessedWith  /** which tool was used to calculate the causes of death interVa/insilicoVa/smartVa **/
    Integer vaProcessed      /** represent the status of calculation of death causes using interVa/insilicoVa - 0: not calculated, 1: calculated **/
    String  vaResult         /** the causes of death calculated by va tools (insilico,interva,smartva) **/

    Integer vaAttempts        /** Number of attempts to collect VA's (not completed forms by some reason <eg. household destroyed>)  **/

    static hasMany = [collectionAttempts:UncompletedVA] /** Records all attempts to collect VA's **/

    String toString(){
        return "${code}: ${dateOfDeath}"
    }

    static mapping = {
        table 'verbal_autopsy_control'

        id column: "id"
        version false

        extId colum: "ext_id"
        code colum: "code"
        name colum: "name"
        gender colum: "gender"
        dateOfBirth colum: "date_of_birth"
        dateOfDeath colum: "date_of_death"
        ageAtDeath colum: "age_at_death"

        motherId colum: "mother_id"
        motherName colum: "mother_name"
        fatherId colum: "father_id"
        fatherName colum: "father_name"

        householdId column: "household_id"
        householdNo column: "household_no"
        gpsAccuracy column: "gps_accuracy"
        gpsAltitude column: "gps_altitude"
        gpsLatitude column: "gps_latitude"
        gpsLongitude column: "gps_longitude"

        deathUuid colum: "death_uuid"

        vaType colum: "va_type"
        vaUuid colum: "va_uuid"
        vaCollected colum: "va_collected", defaultValue:"0"
        vaProcessedWith colum: "va_processed_with"
        vaProcessed colum: "va_processed"
        vaResult colum: "va_result"

        vaAttempts column: "va_attempts"

    }

    static constraints = {
        extId nullable:true, unique: true
        code nullable:false, blank: false, unique: true
        name nullable:false, blank: true
        gender nullable:false
        dateOfBirth nullable:true
        dateOfDeath nullable:true
        ageAtDeath min: 0, nullable: true

        motherId nullable:true
        motherName nullable:true
        fatherId nullable:true
        fatherName nullable:true

        householdId nullable: true
        householdNo nullable: true
        gpsAccuracy nullable: true, blank: true
        gpsAltitude nullable: true, blank: true
        gpsLatitude nullable: true, blank: true
        gpsLongitude nullable: true, blank: true

        deathUuid nullable:true

        vaType nullable:true
        vaUuid nullable:true
        vaCollected nullable:false
        vaProcessedWith nullable:true
        vaProcessed nullable:true
        vaResult nullable:true

        vaAttempts nullable: true
    }

    def beforeInsert() {
        if (dateOfBirth != null && dateOfDeath != null){
            if (dateOfBirth <= dateOfDeath){
                ageAtDeath = GeneralUtil.getAge(dateOfBirth, dateOfDeath)
            }
        }

        if (extId==null || extId.isEmpty()){
            extId = code
        }
        if (code==null || code.isEmpty()){
            code = extId
        }
    }

    def beforeUpdate() {
        if (dateOfBirth != null && dateOfDeath != null){
            if (dateOfBirth <= dateOfDeath){
                ageAtDeath = GeneralUtil.getAge(dateOfBirth, dateOfDeath)
            }
        }
    }

    boolean isEmpty(String obj){
        return  (obj == null) || (obj.isEmpty())
    }

    String toXML(){

        def attempt = collectionAttempts.max{it.autopsy.sysOdkStart}
        def attemptDate = ""
        def attemptReason = ""

        if (attempt != null){
            def autopsy = attempt.autopsy
            def date = autopsy.sysOdkStart==null ? autopsy.sysOdkEnd : autopsy.sysOdkStart
            attemptDate = StringUtil.format(date, "yyyy-MM-dd")
            attemptReason = attempt.reason
        }

        return ("<vacontrol>")+
                (isEmpty(extId) ?              "<extId />"             : "<extId>${extId}</extId>") +
                (isEmpty(code) ?               "<code />"              : "<code>${code}</code>") +
                (isEmpty(name) ?               "<name />"              : "<name>${name}</name>") +
                (isEmpty(gender) ?             "<gender />"            : "<gender>${gender}</gender>") +
                ((dateOfBirth==null) ?        "<dateOfBirth />"       : "<dateOfBirth>${StringUtil.format(dateOfBirth, "yyyy-MM-dd")}</dateOfBirth>") +
                ((dateOfDeath==null) ?        "<dateOfDeath />"       : "<dateOfDeath>${StringUtil.format(dateOfDeath, "yyyy-MM-dd")}</dateOfDeath>") +
                ((ageAtDeath==null) ?         "<ageAtDeath />"        : "<ageAtDeath>${ageAtDeath}</ageAtDeath>") +

                (isEmpty(motherId) ?           "<motherId />"          : "<motherId>${motherId}</motherId>") +
                (isEmpty(motherName) ?         "<motherName />"        : "<motherName>${motherName}</motherName>") +
                (isEmpty(fatherId) ?           "<fatherId />"          : "<fatherId>${fatherId}</fatherId>") +
                (isEmpty(fatherName) ?         "<fatherName />"        : "<fatherName>${fatherName}</fatherName>") +

                (isEmpty(householdId) ?        "<householdId />"       : "<householdId>${householdId}</householdId>") +
                (isEmpty(householdNo) ?        "<householdNo />"       : "<householdNo>${householdNo}</householdNo>") +

                (isEmpty(gpsAccuracy) ?        "<gpsAccuracy />"       : "<gpsAccuracy>${gpsAccuracy}</gpsAccuracy>") +
                (isEmpty(gpsAltitude) ?        "<gpsAltitude />"       : "<gpsAltitude>${gpsAltitude}</gpsAltitude>") +
                (isEmpty(gpsLatitude) ?        "<gpsLatitude />"       : "<gpsLatitude>${gpsLatitude}</gpsLatitude>") +
                (isEmpty(gpsLongitude) ?       "<gpsLongitude />"      : "<gpsLongitude>${gpsLongitude}</gpsLongitude>") +
                /*((deathUuid==null) ?          "<deathUuid />"         : "<deathUuid>${deathUuid}</deathUuid>") +*/
                /*((vaUuid==null) ?             "<vaUuid />"            : "<vaUuid>${vaUuid}</vaUuid>") +*/
                ((vaType==null) ?              "<vaType />"            : "<vaType>${vaType}</vaType>") +
                ((vaCollected==null) ?         "<vaCollected />"       : "<vaCollected>${vaCollected}</vaCollected>") +
                (isEmpty(vaProcessedWith) ?    "<vaProcessedWith />"   : "<vaProcessedWith>${vaProcessedWith}</vaProcessedWith>") +
                ((vaProcessed==null) ?         "<vaProcessed />"       : "<vaProcessed>${vaProcessed}</vaProcessed>") +
                (isEmpty(vaResult) ?           "<vaResult />"          : "<vaResult>${vaResult}</vaResult>") +
                ((vaAttempts==null) ?          "<vaAttempts />"        : "<vaAttempts>${vaAttempts}</vaAttempts>") +
                (isEmpty(attemptDate) ?        "<lastAttemptDate />"   : "<lastAttemptDate>${attemptDate}</lastAttemptDate>") +
                (isEmpty(attemptReason) ?      "<lastAttemptReason />" : "<lastAttemptReason>${attemptReason}</lastAttemptReason>") +
               ("</vacontrol>")
    }
}
