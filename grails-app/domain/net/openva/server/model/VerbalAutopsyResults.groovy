package net.openva.server.model

/**
 * This class represents a Verbal Autopsy Causes of Deaths Calculation Results
 */
class VerbalAutopsyResults {
    VerbalAutopsyControl autopsy  /** a reference to the autopsy collected **/
    String usedTool               /** can be used InterVA, InsilicoVA, Tariff/SmartVA **/
    String result                 /** a text that represents the result    **/

    static constraints = {
        autopsy nullable: false
        usedTool nullable: false, blank: false
        result nullable: false, blank: false
    }
}
