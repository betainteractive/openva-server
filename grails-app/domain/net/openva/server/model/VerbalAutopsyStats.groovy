package net.openva.server.model

import net.openva.security.authentication.User
import net.openva.server.model.logs.LogReport

class VerbalAutopsyStats {
    LogReport logReport
    User createdBy
    Date creationDate

    int totalDeaths    /* Total of Death Registered - VerbalAutopsyControl */
    int totalToCollect /* Total of Verbal Autopsy that need to be collected */
    int totalCollected /* Total of Verbal Autopsy collected */
    int totalAttempts  /* Total of attempts to collect VA - represents the number of rejections (not consented + impossible to visit)*/
    int totalAttemptNi /* Total of attempts with reason equals to "No Informant" */
    int totalAttemptUh /* Total of attempts with reason equals to "Uninhabited House/House destroyed" */
    int totalAttemptWdr /* Total of attempts with reason equals to "Wrong Death Record" */
    int totalAttemptOt /* Total of attempts with reason equals to "Other Reason" */
    int totalAttemptNc /* Total of attempts with reason equals to "Not Consented" */


    static constraints = {
        logReport nullable: true
        createdBy nullable: false
        creationDate nullable: false
    }

    static mapping = {
        logReport column: "log_report_id"
        createdBy column: "log_created_by"
        creationDate column: "log_creation_date"

        totalDeaths column: "total_deaths"
        totalToCollect column: "total_to_collect"
        totalCollected column: "total_collected"
        totalAttempts column: "total_attempts"
        totalAttemptNi column: "total_attempt_ni"
        totalAttemptUh column: "total_attempt_uh"
        totalAttemptWdr column: "total_attempt_wdr"
        totalAttemptOt column: "total_attempt_ot"
        totalAttemptNc column: "total_attempt_nc"
    }
}
