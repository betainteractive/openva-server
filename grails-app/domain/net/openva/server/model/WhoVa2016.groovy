package net.openva.server.model

/**
 * WHO VA 2016 Final table for ODK VA Tables - This class and WhoVa2016Ext are the resume of all ODK Tables for VA's
 */
class WhoVa2016 extends WhoVa2016Ext {
	//static mapWith = "none"

	String sysOdkUri
	String sysOdkCreatorUriUser
	Date sysOdkCreationDate
	String sysOdkLastUpdateUriUser
	Date sysOdkLastUpdateDate
	Integer sysOdkModelVersion
	Integer sysOdkUiVersion
	Character sysOdkIsComplete
	Date sysOdkSubmissionDate
	Date sysOdkMarkedAsCompleteDate
	String sysOdkMetaInstanceId
	String sysOdkMetaInstanceName
	String sysOdkDeviceid
	Date sysOdkStart
	Date sysOdkEnd
	String sysOdkPhonenumber
	String sysOdkSimserial
	String fieldWorkerId
	String isPreRegistered
	String isVisitPossible
	String reasonToNotCollect
	String reasonOther
	String householdId
	String individualId
	BigDecimal gpsAcc
	BigDecimal gpsAlt
	BigDecimal gpsLat
	BigDecimal gpsLng
	String id10002
	String id10003
	String id10004
	String id10007
	String id10008
	String id10009
	String id10010
	Date id10011
	Date id10012
	String id10013
	String id10017
	String id10018
	String id10019
	String id10020
	Date id10021
	String id10022
	Date id10023
	Date id10024
	String ageInDays
	String dateEdit
	String ageInYears
	String ageInYearsRemain
	String ageInMonths
	String ageInMonthsRemain
	String ageInWeeks
	String isNeonatal1
	String isChild1
	String isAdult1
	String displayAgeNeonate /* this column is a odk note - we dont need it on final table */
	String displayAgeChild   /* this column is a odk note - we dont need it on final table */
	String displayAgeAdult   /* this column is a odk note - we dont need it on final table */
	String ageGroup
	String ageNeonateUnit
	Integer ageNeonateDays
	Integer ageNeonateHours
	Integer ageNeonateMinutes
	String ageChildUnit
	Integer ageChildDays
	String ageInMonthsByYear
	String ageInYears2
	String isNeonatal2
	Integer ageChildMonths
	Integer ageChildYears
	Integer ageAdult
	String isChild2
	String isAdult2
	String isNeonatal
	String isChild
	String isAdult
	String ageNeonateInWeeks
	String ageChildInWeeks
	String illAgeChildInMonths
	String isChildAgeLessThanOne
	String illAgeInMonth1
	String illAgeInMonth2
	String illAgeInMonth3
	String illAgeInMonth
	String illAgeNeonateInMonths
	String ageInDaysNeonate
	String id10051
	String id10052
	String id10053
	String id10054
	String id10055
	String id10056
	String id10057
	String id10058
	String id10059
	Date id10060
	String id10061
	String id10062
	String id10063
	String id10064
	String id10065
	String id10066
	String id10069
	String id10070
	Date id10071
	String id10072
	String id10073
	String id10077
	String id10079
	String id10080
	String id10081
	String id10082
	String id10083
	String id10084
	String id10085
	String id10086
	String id10087
	String id10088
	String id10089
	String id10090
	String id10091
	String id10092
	String id10093
	String id10094
	String id10095
	String id10096
	String id10097
	String id10098
	String id10099
	String id10100
	String id10104
	String id10105
	Integer id10106
	String id10107
	Integer id10108
	String id10109
	String id10110
	String id10111
	String id10112
	String id10113
	String id10114
	String id10115
	String id10116
	String illnessLength
	Integer id10120
	Integer id10121
	Integer id10122
	String id10123
	String checkDays
	String checkMonth
	String checkWeeks
	String id10125
	String id10126
	String id10127
	String id10128
	String id10129
	String id10130
	String id10131
	String id10132
	String id10133
	String id10134
	String id10135
	String id10136
	String id10137
	String id10138
	String id10139
	String id10140
	String id10141
	String id10142
	String id10143
	String id10144
	String id10147
	Integer id10148
	String id10149
	String id10150
	String id10151
	String id10152
	String id10153
	Integer id10154
	String id10155
	String id10156
	String id10157
	String id10158
	String id10159
	Integer id10161_0
	String id10161Unit
	Integer id10161_1
	String id10161
	Integer id10162
	Integer id10163
	String id10165
	String id10166
	Integer id10167
	String id10168
	Integer id10169
	String id10170
	String id10171
	String id10172
	String id10173_1_St /* select_501 option = stridor */
	String id10173_2_Gr /* select_501 option = grunting */
	String id10173_3_Wz /* select_501 option = wheezing */
	String id10173_4_No /* select_501 option = no */
	String id10173_5_Dk /* select_501 option = dk */
	String id10173_6_Rf /* select_501 option = ref */
	String id10174
	String id10175
	Integer id10176
	String id10176Unit
	Integer id10178
	Integer id10179
	Integer id10179_1
	String id10181
	Integer id10182
	Integer id10183
	Integer id10184
	String id10185
	String id10186
	String id10187
	String id10188
	String id10189
	Integer id10190
	String id10191
	String id10192
	String id10193
	String id10194
	String id10195
	String painDuration
	Integer id10196
	Integer id10197
	Integer id10197b
	Integer id10198
	String id10199
	String id10200
	Integer id10201
	Integer id10202
	String id10203
	String id10204
	Integer id10205
	Integer id10206
	String id10207
	String id10208
	Integer id10209
	String id10210
	Integer id10211
	String id10212
	Integer id10213
	String id10214
	String id10215
	Integer id10216
	String id10217
	String id10218
	String id10219
	String id10220
	Integer id10221
	String id10222
	String id10223
	String id10224
	String id10225
	String id10226
	String id10227
	String id10228
	String id10229
	String id10230
	String id10231
	Integer id10232
	String id10233
	Integer id10234
	String id10235_1_Fc /* select_135 option = face */
	String id10235_2_Tk /* select_135 option = trunk */
	String id10235_3_Et /* select_135 option = extremities */
	String id10235_4_Ev /* select_135 option = everywhere */
	Integer processed

	static mapping = {
		//datasource 'openva'
		table 'who_va_2016'

		//id name: "sysOdkUri", generator: "assigned"

		version false

		sysOdkUri column:'sys_odk_uri'
		sysOdkCreatorUriUser column:'sys_odk_creator_uri_user'
		sysOdkCreationDate column:'sys_odk_creation_date'
		sysOdkLastUpdateUriUser column:'sys_odk_last_update_uri_user'
		sysOdkLastUpdateDate column:'sys_odk_last_update_date'
		sysOdkModelVersion column:'sys_odk_model_version'
		sysOdkUiVersion column:'sys_odk_ui_version'
		sysOdkIsComplete column:'sys_odk_is_complete'
		sysOdkSubmissionDate column:'sys_odk_submission_date'
		sysOdkMarkedAsCompleteDate column:'sys_odk_marked_as_complete_date'
		sysOdkMetaInstanceId column:'sys_odk_meta_instance_id'
		sysOdkMetaInstanceName column:'sys_odk_meta_instance_name'
		sysOdkDeviceid column:'sys_odk_deviceid'
		sysOdkStart column:'sys_odk_start'
		sysOdkEnd column:'sys_odk_end'
		sysOdkPhonenumber column:'sys_odk_phonenumber'
		sysOdkSimserial column:'sys_odk_simserial'
		fieldWorkerId column:'field_worker_id'
		isPreRegistered column:'is_pre_registered'
		isVisitPossible column:'is_visit_possible'
		reasonToNotCollect column:'reason_to_not_collect'
		reasonOther column:'reason_other'
		householdId column:'household_id'
		individualId column:'individual_id'
		gpsAcc column:'gps_acc'
		gpsAlt column:'gps_alt'
		gpsLat column:'gps_lat'
		gpsLng column:'gps_lng'
		id10002 column:'id10002'
		id10003 column:'id10003'
		id10004 column:'id10004'
		id10007 column:'id10007'
		id10008 column:'id10008'
		id10009 column:'id10009'
		id10010 column:'id10010'
		id10011 column:'id10011'
		id10012 column:'id10012'
		id10013 column:'id10013'
		id10017 column:'id10017'
		id10018 column:'id10018'
		id10019 column:'id10019'
		id10020 column:'id10020'
		id10021 column:'id10021'
		id10022 column:'id10022'
		id10023 column:'id10023'
		id10024 column:'id10024'
		ageInDays column:'age_in_days'
		dateEdit column:'date_edit'
		ageInYears column:'age_in_years'
		ageInYearsRemain column:'age_in_years_remain'
		ageInMonths column:'age_in_months'
		ageInMonthsRemain column:'age_in_months_remain'
		ageInWeeks column:'age_in_weeks'
		isNeonatal1 column:'is_neonatal1'
		isChild1 column:'is_child1'
		isAdult1 column:'is_adult1'
		displayAgeAdult column:'display_age_adult'
		displayAgeChild column:'display_age_child'
		displayAgeNeonate column:'display_age_neonate'
		ageGroup column:'age_group'
		ageNeonateUnit column:'age_neonate_unit'
		ageNeonateDays column:'age_neonate_days'
		ageNeonateHours column:'age_neonate_hours'
		ageNeonateMinutes column:'age_neonate_minutes'
		ageChildUnit column:'age_child_unit'
		ageChildDays column:'age_child_days'
		ageInMonthsByYear column:'age_in_months_by_year'
		ageInYears2 column:'age_in_years2'
		isNeonatal2 column:'is_neonatal2'
		ageChildMonths column:'age_child_months'
		ageChildYears column:'age_child_years'
		ageAdult column:'age_adult'
		isChild2 column:'is_child2'
		isAdult2 column:'is_adult2'
		isNeonatal column:'is_neonatal'
		isChild column:'is_child'
		isAdult column:'is_adult'
		ageNeonateInWeeks column:'age_neonate_in_weeks'
		ageChildInWeeks column:'age_child_in_weeks'
		illAgeChildInMonths column:'ill_age_child_in_months'
		isChildAgeLessThanOne column:'is_child_age_less_than_one'
		illAgeInMonth1 column:'ill_age_in_month1'
		illAgeInMonth2 column:'ill_age_in_month2'
		illAgeInMonth3 column:'ill_age_in_month3'
		illAgeInMonth column:'ill_age_in_month'
		illAgeNeonateInMonths column:'ill_age_neonate_in_months'
		ageInDaysNeonate column:'age_in_days_neonate'
		id10051 column:'id10051'
		id10052 column:'id10052'
		id10053 column:'id10053'
		id10054 column:'id10054'
		id10055 column:'id10055'
		id10056 column:'id10056'
		id10057 column:'id10057'
		id10058 column:'id10058'
		id10059 column:'id10059'
		id10060 column:'id10060'
		id10061 column:'id10061'
		id10062 column:'id10062'
		id10063 column:'id10063'
		id10064 column:'id10064'
		id10065 column:'id10065'
		id10066 column:'id10066'
		id10069 column:'id10069'
		id10070 column:'id10070'
		id10071 column:'id10071'
		id10072 column:'id10072'
		id10073 column:'id10073'
		id10077 column:'id10077'
		id10079 column:'id10079'
		id10080 column:'id10080'
		id10081 column:'id10081'
		id10082 column:'id10082'
		id10083 column:'id10083'
		id10084 column:'id10084'
		id10085 column:'id10085'
		id10086 column:'id10086'
		id10087 column:'id10087'
		id10088 column:'id10088'
		id10089 column:'id10089'
		id10090 column:'id10090'
		id10091 column:'id10091'
		id10092 column:'id10092'
		id10093 column:'id10093'
		id10094 column:'id10094'
		id10095 column:'id10095'
		id10096 column:'id10096'
		id10097 column:'id10097'
		id10098 column:'id10098'
		id10099 column:'id10099'
		id10100 column:'id10100'
		id10104 column:'id10104'
		id10105 column:'id10105'
		id10106 column:'id10106'
		id10107 column:'id10107'
		id10108 column:'id10108'
		id10109 column:'id10109'
		id10110 column:'id10110'
		id10111 column:'id10111'
		id10112 column:'id10112'
		id10113 column:'id10113'
		id10114 column:'id10114'
		id10115 column:'id10115'
		id10116 column:'id10116'
		illnessLength column:'illness_length'
		id10120 column:'id10120'
		id10121 column:'id10121'
		id10122 column:'id10122'
		id10123 column:'id10123'
		checkDays column:'check_days'
		checkMonth column:'check_month'
		checkWeeks column:'check_weeks'
		id10125 column:'id10125'
		id10126 column:'id10126'
		id10127 column:'id10127'
		id10128 column:'id10128'
		id10129 column:'id10129'
		id10130 column:'id10130'
		id10131 column:'id10131'
		id10132 column:'id10132'
		id10133 column:'id10133'
		id10134 column:'id10134'
		id10135 column:'id10135'
		id10136 column:'id10136'
		id10137 column:'id10137'
		id10138 column:'id10138'
		id10139 column:'id10139'
		id10140 column:'id10140'
		id10141 column:'id10141'
		id10142 column:'id10142'
		id10143 column:'id10143'
		id10144 column:'id10144'
		id10147 column:'id10147'
		id10148 column:'id10148'
		id10149 column:'id10149'
		id10150 column:'id10150'
		id10151 column:'id10151'
		id10152 column:'id10152'
		id10153 column:'id10153'
		id10154 column:'id10154'
		id10155 column:'id10155'
		id10156 column:'id10156'
		id10157 column:'id10157'
		id10158 column:'id10158'
		id10159 column:'id10159'
		id10161_0 column:'id10161_0'
		id10161Unit column:'id10161_unit'
		id10161_1 column:'id10161_1'
		id10161 column:'id10161'
		id10162 column:'id10162'
		id10163 column:'id10163'
		id10165 column:'id10165'
		id10166 column:'id10166'
		id10167 column:'id10167'
		id10168 column:'id10168'
		id10169 column:'id10169'
		id10170 column:'id10170'
		id10171 column:'id10171'
		id10172 column:'id10172'
		id10173_1_St column:'id10173_1_st'
		id10173_2_Gr column:'id10173_2_gr'
		id10173_3_Wz column:'id10173_3_wz'
		id10173_4_No column:'id10173_4_no'
		id10173_5_Dk column:'id10173_5_dk'
		id10173_6_Rf column:'id10173_6_rf'
		id10174 column:'id10174'
		id10175 column:'id10175'
		id10176 column:'id10176'
		id10176Unit column:'id10176_unit'
		id10178 column:'id10178'
		id10179 column:'id10179'
		id10179_1 column:'id10179_1'
		id10181 column:'id10181'
		id10182 column:'id10182'
		id10183 column:'id10183'
		id10184 column:'id10184'
		id10185 column:'id10185'
		id10186 column:'id10186'
		id10187 column:'id10187'
		id10188 column:'id10188'
		id10189 column:'id10189'
		id10190 column:'id10190'
		id10191 column:'id10191'
		id10192 column:'id10192'
		id10193 column:'id10193'
		id10194 column:'id10194'
		id10195 column:'id10195'
		painDuration column:'pain_duration'
		id10196 column:'id10196'
		id10197 column:'id10197'
		id10197b column:'id10197b'
		id10198 column:'id10198'
		id10199 column:'id10199'
		id10200 column:'id10200'
		id10201 column:'id10201'
		id10202 column:'id10202'
		id10203 column:'id10203'
		id10204 column:'id10204'
		id10205 column:'id10205'
		id10206 column:'id10206'
		id10207 column:'id10207'
		id10208 column:'id10208'
		id10209 column:'id10209'
		id10210 column:'id10210'
		id10211 column:'id10211'
		id10212 column:'id10212'
		id10213 column:'id10213'
		id10214 column:'id10214'
		id10215 column:'id10215'
		id10216 column:'id10216'
		id10217 column:'id10217'
		id10218 column:'id10218'
		id10219 column:'id10219'
		id10220 column:'id10220'
		id10221 column:'id10221'
		id10222 column:'id10222'
		id10223 column:'id10223'
		id10224 column:'id10224'
		id10225 column:'id10225'
		id10226 column:'id10226'
		id10227 column:'id10227'
		id10228 column:'id10228'
		id10229 column:'id10229'
		id10230 column:'id10230'
		id10231 column:'id10231'
		id10232 column:'id10232'
		id10233 column:'id10233'
		id10234 column:'id10234'
		id10235_1_Fc column:'id10235_1_fc'
		id10235_2_Tk column:'id10235_2_tk'
		id10235_3_Et column:'id10235_3_et'
		id10235_4_Ev column:'id10235_4_ev'

		processed column:'processed'
	}

	static constraints = {
		sysOdkUri maxSize: 80, unique: true
		sysOdkCreatorUriUser maxSize: 80
		sysOdkLastUpdateUriUser nullable: true, maxSize: 80
		sysOdkModelVersion nullable: true
		sysOdkUiVersion nullable: true
		sysOdkIsComplete nullable: true, maxSize: 1
		sysOdkSubmissionDate nullable: true
		sysOdkMarkedAsCompleteDate nullable: true
		sysOdkMetaInstanceId nullable: true, maxSize: 50
		sysOdkMetaInstanceName nullable: true, maxSize: 50
		sysOdkDeviceid nullable: true, maxSize: 50
		sysOdkStart nullable: true
		sysOdkEnd nullable: true
		sysOdkPhonenumber nullable: true, maxSize: 55
		sysOdkSimserial nullable: true, maxSize: 100
		fieldWorkerId nullable: true, maxSize: 40
		isPreRegistered nullable: true, maxSize: 4
		isVisitPossible nullable: true, maxSize: 4
		reasonToNotCollect nullable: true, maxSize: 40
		reasonOther nullable: true
		householdId nullable: true, maxSize: 55
		individualId nullable: true, maxSize: 55
		gpsAcc nullable: true, scale: 10
		gpsAlt nullable: true, scale: 10
		gpsLat nullable: true, scale: 10
		gpsLng nullable: true, scale: 10
		id10002 nullable: true, maxSize: 10
		id10003 nullable: true, maxSize: 10
		id10004 nullable: true, maxSize: 10
		id10007 nullable: true, maxSize: 150
		id10008 nullable: true, maxSize: 30
		id10009 nullable: true, maxSize: 4
		id10010 nullable: true, maxSize: 150
		id10011 nullable: true
		id10012 nullable: true
		id10013 nullable: true, maxSize: 4
		id10017 nullable: true, maxSize: 100
		id10018 nullable: true, maxSize: 100
		id10019 nullable: true, maxSize: 10
		id10020 nullable: true, maxSize: 4
		id10021 nullable: true
		id10022 nullable: true, maxSize: 4
		id10023 nullable: true
		id10024 nullable: true
		ageInDays nullable: true, maxSize: 20
		dateEdit nullable: true, maxSize: 2
		ageInYears nullable: true, maxSize: 20
		ageInYearsRemain nullable: true, maxSize: 20
		ageInMonths nullable: true, maxSize: 20
		ageInMonthsRemain nullable: true, maxSize: 20
		ageInWeeks nullable: true, maxSize: 20
		isNeonatal1 nullable: true, maxSize: 20
		isChild1 nullable: true, maxSize: 20
		isAdult1 nullable: true, maxSize: 20
		displayAgeAdult nullable: true, maxSize: 2
		displayAgeChild nullable: true, maxSize: 2
		displayAgeNeonate nullable: true, maxSize: 2
		ageGroup nullable: true, maxSize: 20
		ageNeonateUnit nullable: true, maxSize: 20
		ageNeonateDays nullable: true
		ageNeonateHours nullable: true
		ageNeonateMinutes nullable: true
		ageChildUnit nullable: true, maxSize: 20
		ageChildDays nullable: true
		ageInMonthsByYear nullable: true, maxSize: 20
		ageInYears2 nullable: true, maxSize: 20
		isNeonatal2 nullable: true, maxSize: 20
		ageChildMonths nullable: true
		ageChildYears nullable: true
		ageAdult nullable: true
		isChild2 nullable: true, maxSize: 20
		isAdult2 nullable: true, maxSize: 20
		isNeonatal nullable: true, maxSize: 20
		isChild nullable: true, maxSize: 20
		isAdult nullable: true, maxSize: 20
		ageNeonateInWeeks nullable: true, maxSize: 20
		ageChildInWeeks nullable: true, maxSize: 20
		illAgeChildInMonths nullable: true, maxSize: 20
		isChildAgeLessThanOne nullable: true, maxSize: 20
		illAgeInMonth1 nullable: true, maxSize: 20
		illAgeInMonth2 nullable: true, maxSize: 20
		illAgeInMonth3 nullable: true, maxSize: 20
		illAgeInMonth nullable: true, maxSize: 20
		illAgeNeonateInMonths nullable: true, maxSize: 20
		ageInDaysNeonate nullable: true, maxSize: 20
		id10051 nullable: true, maxSize: 4
		id10052 nullable: true, maxSize: 40
		id10053 nullable: true, maxSize: 100
		id10054 nullable: true, maxSize: 100
		id10055 nullable: true, maxSize: 155
		id10056 nullable: true, maxSize: 155
		id10057 nullable: true, maxSize: 155
		id10058 nullable: true, maxSize: 40
		id10059 nullable: true, maxSize: 40
		id10060 nullable: true
		id10061 nullable: true, maxSize: 155
		id10062 nullable: true, maxSize: 155
		id10063 nullable: true, maxSize: 40
		id10064 nullable: true, maxSize: 40
		id10065 nullable: true, maxSize: 40
		id10066 nullable: true, maxSize: 100
		id10069 nullable: true, maxSize: 4
		id10070 nullable: true, maxSize: 100
		id10071 nullable: true
		id10072 nullable: true, maxSize: 100
		id10073 nullable: true, maxSize: 100
		id10077 nullable: true, maxSize: 4
		id10079 nullable: true, maxSize: 4
		id10080 nullable: true, maxSize: 40
		id10081 nullable: true, maxSize: 40
		id10082 nullable: true, maxSize: 4
		id10083 nullable: true, maxSize: 4
		id10084 nullable: true, maxSize: 4
		id10085 nullable: true, maxSize: 4
		id10086 nullable: true, maxSize: 4
		id10087 nullable: true, maxSize: 4
		id10088 nullable: true, maxSize: 40
		id10089 nullable: true, maxSize: 4
		id10090 nullable: true, maxSize: 4
		id10091 nullable: true, maxSize: 4
		id10092 nullable: true, maxSize: 4
		id10093 nullable: true, maxSize: 4
		id10094 nullable: true, maxSize: 4
		id10095 nullable: true, maxSize: 4
		id10096 nullable: true, maxSize: 4
		id10097 nullable: true, maxSize: 4
		id10098 nullable: true, maxSize: 4
		id10099 nullable: true, maxSize: 4
		id10100 nullable: true, maxSize: 4
		id10104 nullable: true, maxSize: 4
		id10105 nullable: true, maxSize: 4
		id10106 nullable: true
		id10107 nullable: true, maxSize: 4
		id10108 nullable: true
		id10109 nullable: true, maxSize: 4
		id10110 nullable: true, maxSize: 4
		id10111 nullable: true, maxSize: 4
		id10112 nullable: true, maxSize: 4
		id10113 nullable: true, maxSize: 4
		id10114 nullable: true, maxSize: 4
		id10115 nullable: true, maxSize: 4
		id10116 nullable: true, maxSize: 4
		illnessLength nullable: true, maxSize: 10
		id10120 nullable: true
		id10121 nullable: true
		id10122 nullable: true
		id10123 nullable: true, maxSize: 4
		checkDays nullable: true, maxSize: 2
		checkMonth nullable: true, maxSize: 2
		checkWeeks nullable: true, maxSize: 2
		id10125 nullable: true, maxSize: 4
		id10126 nullable: true, maxSize: 4
		id10127 nullable: true, maxSize: 4
		id10128 nullable: true, maxSize: 4
		id10129 nullable: true, maxSize: 4
		id10130 nullable: true, maxSize: 4
		id10131 nullable: true, maxSize: 4
		id10132 nullable: true, maxSize: 4
		id10133 nullable: true, maxSize: 4
		id10134 nullable: true, maxSize: 4
		id10135 nullable: true, maxSize: 4
		id10136 nullable: true, maxSize: 4
		id10137 nullable: true, maxSize: 4
		id10138 nullable: true, maxSize: 4
		id10139 nullable: true, maxSize: 4
		id10140 nullable: true, maxSize: 4
		id10141 nullable: true, maxSize: 4
		id10142 nullable: true, maxSize: 4
		id10143 nullable: true, maxSize: 4
		id10144 nullable: true, maxSize: 4
		id10147 nullable: true, maxSize: 4
		id10148 nullable: true
		id10149 nullable: true, maxSize: 4
		id10150 nullable: true, maxSize: 10
		id10151 nullable: true, maxSize: 10
		id10152 nullable: true, maxSize: 4
		id10153 nullable: true, maxSize: 4
		id10154 nullable: true
		id10155 nullable: true, maxSize: 4
		id10156 nullable: true, maxSize: 4
		id10157 nullable: true, maxSize: 4
		id10158 nullable: true, maxSize: 4
		id10159 nullable: true, maxSize: 4
		id10161_0 nullable: true
		id10161Unit nullable: true, maxSize: 10
		id10161_1 nullable: true
		id10161 nullable: true, maxSize: 10
		id10162 nullable: true
		id10163 nullable: true
		id10165 nullable: true, maxSize: 10
		id10166 nullable: true, maxSize: 4
		id10167 nullable: true
		id10168 nullable: true, maxSize: 4
		id10169 nullable: true
		id10170 nullable: true, maxSize: 4
		id10171 nullable: true, maxSize: 4
		id10172 nullable: true, maxSize: 4
		id10173_1_St nullable: true, maxSize: 4
		id10173_2_Gr nullable: true, maxSize: 4
		id10173_3_Wz nullable: true, maxSize: 4
		id10173_4_No nullable: true, maxSize: 4
		id10173_5_Dk nullable: true, maxSize: 4
		id10173_6_Rf nullable: true, maxSize: 4
		id10174 nullable: true, maxSize: 4
		id10175 nullable: true, maxSize: 4
		id10176 nullable: true
		id10176Unit nullable: true, maxSize: 10
		id10178 nullable: true
		id10179 nullable: true
		id10179_1 nullable: true
		id10181 nullable: true, maxSize: 4
		id10182 nullable: true
		id10183 nullable: true
		id10184 nullable: true
		id10185 nullable: true, maxSize: 4
		id10186 nullable: true, maxSize: 4
		id10187 nullable: true, maxSize: 4
		id10188 nullable: true, maxSize: 4
		id10189 nullable: true, maxSize: 4
		id10190 nullable: true
		id10191 nullable: true, maxSize: 4
		id10192 nullable: true, maxSize: 4
		id10193 nullable: true, maxSize: 4
		id10194 nullable: true, maxSize: 4
		id10195 nullable: true, maxSize: 4
		painDuration nullable: true, maxSize: 10
		id10196 nullable: true
		id10197 nullable: true
		id10197b nullable: true
		id10198 nullable: true
		id10199 nullable: true, maxSize: 40
		id10200 nullable: true, maxSize: 4
		id10201 nullable: true
		id10202 nullable: true
		id10203 nullable: true, maxSize: 10
		id10204 nullable: true, maxSize: 4
		id10205 nullable: true
		id10206 nullable: true
		id10207 nullable: true, maxSize: 4
		id10208 nullable: true, maxSize: 4
		id10209 nullable: true
		id10210 nullable: true, maxSize: 4
		id10211 nullable: true
		id10212 nullable: true, maxSize: 4
		id10213 nullable: true
		id10214 nullable: true, maxSize: 4
		id10215 nullable: true, maxSize: 4
		id10216 nullable: true
		id10217 nullable: true, maxSize: 4
		id10218 nullable: true, maxSize: 4
		id10219 nullable: true, maxSize: 4
		id10220 nullable: true, maxSize: 4
		id10221 nullable: true
		id10222 nullable: true, maxSize: 4
		id10223 nullable: true, maxSize: 4
		id10224 nullable: true, maxSize: 4
		id10225 nullable: true, maxSize: 4
		id10226 nullable: true, maxSize: 4
		id10227 nullable: true, maxSize: 4
		id10228 nullable: true, maxSize: 4
		id10229 nullable: true, maxSize: 4
		id10230 nullable: true, maxSize: 4
		id10231 nullable: true, maxSize: 4
		id10232 nullable: true
		id10233 nullable: true, maxSize: 4
		id10234 nullable: true
		id10235_1_Fc nullable: true, maxSize: 4
		id10235_2_Tk nullable: true, maxSize: 4
		id10235_3_Et nullable: true, maxSize: 4
		id10235_4_Ev nullable: true, maxSize: 4

		processed nullable: true
	}
}
