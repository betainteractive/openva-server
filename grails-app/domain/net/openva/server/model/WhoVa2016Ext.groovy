package net.openva.server.model

/**
 * WHO VA 2016 Final table Extension for ODK VA Tables - This class and WhoVa2016 are the resume of all ODK Tables for VA's
 */
class WhoVa2016Ext {
	//static mapWith = "none"

	//String sysOdkParentUri
	//WhoVa2016 whoVa2016
	String id10236
	String id10237
	String id10238
	String id10239
	String id10240
	String id10241
	String id10242
	String id10243
	String id10244
	String id10245
	String id10246
	String id10247
	Integer id10248
	String id10249
	Integer id10250
	String id10251
	String id10252
	String id10253
	String id10254
	String id10255
	String id10256
	String id10257
	String id10258
	String id10259
	String id10260_1_Rs /* select_161 option = right_side */
	String id10260_2_Ls /* select_161 option = left_side */
	String id10260_3_Lp /* select_161 option = lower_part_of_body */
	String id10260_4_Up /* select_161 option = upper_part_of_body */
	String id10260_5_Ol /* select_161 option = one_leg_only */
	String id10260_6_Oa /* select_161 option = one_arm_only */
	String id10260_7_Wb /* select_161 option = whole_body */
	String id10260_8_Ot /* select_161 option = other */
	String id10261
	Integer id10262
	String id10263
	String id10264
	String id10265
	Integer id10266
	String id10267
	String id10268
	String id10269
	String id10270
	String id10271
	String id10272
	String id10273
	Integer id10274
	String id10275
	String id10276
	String id10277
	String id10278
	String id10279
	String id10281
	String id10282
	String id10283
	String id10284
	Integer id10285
	String id10286
	String id10287
	String id10288
	String id10289
	String id10290
	String id10294
	String id10295
	String id10296
	String id10297
	String id10298
	String id10299
	String id10300
	String id10301
	String id10302
	Integer id10303
	String id10304
	String id10305
	String id10306
	String id10307
	String id10308
	Integer id10309
	String id10310
	String id10312
	String id10313
	String id10314
	String id10315
	String id10316
	String id10317
	String id10318
	Integer id10319
	String id10320
	String id10321
	String id10322
	String id10323
	String id10324
	String id10325
	String id10326
	String id10327
	String id10328
	String id10329
	String id10330
	String id10331
	Integer id10332
	String id10333
	String id10334
	String id10335
	String id10336
	String id10337
	String id10338
	String id10339
	String id10340
	String id10342
	String id10343
	String id10344
	String id10347
	Integer id10351
	Integer id10352
	String id10354
	String id10355
	String id10356
	String id10357
	String motherDeath
	Integer id103581
	Integer id10358
	Integer id10359
	String id10360
	String id10361
	String id10362
	String id10363
	String id10364
	String id10365
	Integer id10366
	Integer id10367
	String id10368
	String id10369
	String id10370
	String id10371
	String id10372
	String id10373
	String id10376
	String id10377
	String lastTimeMoved
	Integer id10379
	Integer id10380
	Integer id10382
	String id10383
	String id10384
	String id10385
	String id10387
	String id10388
	String id10389
	String id10391
	Integer id10392
	String id10393
	Integer id10394
	String id10395
	String id10396
	String id10397
	String id10398
	String id10399
	String id10400
	String id10401
	String id10402
	String id10403
	String id10404
	String id10405
	String id10406
	String id10408
	String id104081
	String id10411
	String id10412
	String id10413
	String id10414
	Integer id10415
	String id10418
	String id10419
	String id10420
	String id10421
	String id10422
	String id10423
	String id10424
	String id10425
	String id10426
	String id10427
	String id10428
	String id10429
	String id10430
	String id10431
	String id10432
	String id10433_1_Th /* select_322 option = traditional_healer */
	String id10433_2_Hp /* select_322 option = homeopath */
	String id10433_3_Rl /* select_322 option = religious_leader */
	String id10433_4_Gh /* select_322 option = government_hospital */
	String id10433_5_Gc /* select_322 option = government_health_center_or_clinic */
	String id10433_6_Ph /* select_322 option = private_hospital */
	String id10433_7_Cp /* select_322 option = community_based_practitionerinsystem */
	String id10433_8_Ta /* select_322 option = trained_birth_attendant */
	String id10433_9_Pp /* select_322 option = private_physician */
	String id10433_10_Rf /* select_322 option = relative_friend */
	String id10433_11_Ph /* select_322 option = pharmacy */
	String id10433_12_Dk /* select_322 option = dk */
	String id10433_13_Rf /* select_322 option = ref */
	String id10434
	String id10435
	String id10436
	String id10437
	String id10438
	Date id10439
	Date id10440
	Date id10441
	BigDecimal id10442
	BigDecimal id10443
	String id10444
	String id10445
	String id10446
	String id10450
	String id10451
	String id10452
	String id10453
	String id10454
	String id10455
	String id10456
	String id10457
	String id10458
	String id10459
	String id10462
	String id10463
	String id10464
	String id10465
	String id10466
	String id10467
	String id10468
	String id10469
	String id10470
	String id10471
	String id10472
	String id10473
	String id10476

	String id10477_1_Ck	 /* select_510 option = Chronic_kidney_disease */
	String id10477_2_Dl	 /* select_510 option = Dialysis */
	String id10477_3_Fv	 /* select_510 option = Fever */
	String id10477_4_Ha	 /* select_510 option = Heart_attack */
	String id10477_5_Hp	 /* select_510 option = Heart_problem */
	String id10477_6_Jd	 /* select_510 option = Jaundice */
	String id10477_7_Lf	 /* select_510 option = Liver_failure */
	String id10477_8_Ml	 /* select_510 option = Malaria */
	String id10477_9_Pn	 /* select_510 option = Pneumonia */
	String id10477_10_Rk /* select_510 option = Renal_kidney_failure */
	String id10477_11_Sc /* select_510 option = Suicide */
	String id10477_12_No /* select_510 option = None */
	String id10477_13_Dk /* select_510 option = dk */

	String id10478_1_Ab  /* select_511 option = abdomen */
	String id10478_2_Cc  /* select_511 option = cancer */
	String id10478_3_Dh  /* select_511 option = dehydration */
	String id10478_4_Dg  /* select_511 option = dengue */
	String id10478_5_Dr  /* select_511 option = diarrhea */
	String id10478_6_Fv  /* select_511 option = fever */
	String id10478_7_Hp  /* select_511 option = heart_problem */
	String id10478_8_Jd  /* select_511 option = jaundice */
	String id10478_9_Pn	 /* select_511 option = pneumonia */
	String id10478_10_Rs /* select_511 option = rash */
	String id10478_11_No /* select_511 option = None */
	String id10478_12_Dk /* select_511 option = dk */

	String id10479_1_Ax  /* select_512 option = asphyxia */
	String id10479_2_Ib  /* select_512 option = incubator */
	String id10479_3_Lp  /* select_512 option = lung_problem */
	String id10479_4_Pn  /* select_512 option = pneumonia */
	String id10479_5_Pd  /* select_512 option = preterm_delivery */
	String id10479_6_Rd  /* select_512 option = respiratory_distress */
	String id10479_7_No  /* select_512 option = None */
	String id10479_8_Dk  /* select_512 option = dk */
	Date id10481

	static mapping = {
		tablePerHierarchy false

		//datasource 'openva'
		table 'who_va_2016_ext'

		//id name: "sysOdkParentUri"

		version false

		//sysOdkParentUri column:'sys_odk_parent_uri'
		//whoVa2016 column:'sys_odk_parent_uri'
		id10236 column:'id10236'
		id10237 column:'id10237'
		id10238 column:'id10238'
		id10239 column:'id10239'
		id10240 column:'id10240'
		id10241 column:'id10241'
		id10242 column:'id10242'
		id10243 column:'id10243'
		id10244 column:'id10244'
		id10245 column:'id10245'
		id10246 column:'id10246'
		id10247 column:'id10247'
		id10248 column:'id10248'
		id10249 column:'id10249'
		id10250 column:'id10250'
		id10251 column:'id10251'
		id10252 column:'id10252'
		id10253 column:'id10253'
		id10254 column:'id10254'
		id10255 column:'id10255'
		id10256 column:'id10256'
		id10257 column:'id10257'
		id10258 column:'id10258'
		id10259 column:'id10259'

		id10260_1_Rs column:'id10260_1_rs'
		id10260_2_Ls column:'id10260_2_ls'
		id10260_3_Lp column:'id10260_3_lp'
		id10260_4_Up column:'id10260_4_up'
		id10260_5_Ol column:'id10260_5_ol'
		id10260_6_Oa column:'id10260_6_oa'
		id10260_7_Wb column:'id10260_7_wb'
		id10260_8_Ot column:'id10260_8_ot'

		id10261 column:'id10261'
		id10262 column:'id10262'
		id10263 column:'id10263'
		id10264 column:'id10264'
		id10265 column:'id10265'
		id10266 column:'id10266'
		id10267 column:'id10267'
		id10268 column:'id10268'
		id10269 column:'id10269'
		id10270 column:'id10270'
		id10271 column:'id10271'
		id10272 column:'id10272'
		id10273 column:'id10273'
		id10274 column:'id10274'
		id10275 column:'id10275'
		id10276 column:'id10276'
		id10277 column:'id10277'
		id10278 column:'id10278'
		id10279 column:'id10279'
		id10281 column:'id10281'
		id10282 column:'id10282'
		id10283 column:'id10283'
		id10284 column:'id10284'
		id10285 column:'id10285'
		id10286 column:'id10286'
		id10287 column:'id10287'
		id10288 column:'id10288'
		id10289 column:'id10289'
		id10290 column:'id10290'
		id10294 column:'id10294'
		id10295 column:'id10295'
		id10296 column:'id10296'
		id10297 column:'id10297'
		id10298 column:'id10298'
		id10299 column:'id10299'
		id10300 column:'id10300'
		id10301 column:'id10301'
		id10302 column:'id10302'
		id10303 column:'id10303'
		id10304 column:'id10304'
		id10305 column:'id10305'
		id10306 column:'id10306'
		id10307 column:'id10307'
		id10308 column:'id10308'
		id10309 column:'id10309'
		id10310 column:'id10310'
		id10312 column:'id10312'
		id10313 column:'id10313'
		id10314 column:'id10314'
		id10315 column:'id10315'
		id10316 column:'id10316'
		id10317 column:'id10317'
		id10318 column:'id10318'
		id10319 column:'id10319'
		id10320 column:'id10320'
		id10321 column:'id10321'
		id10322 column:'id10322'
		id10323 column:'id10323'
		id10324 column:'id10324'
		id10325 column:'id10325'
		id10326 column:'id10326'
		id10327 column:'id10327'
		id10328 column:'id10328'
		id10329 column:'id10329'
		id10330 column:'id10330'
		id10331 column:'id10331'
		id10332 column:'id10332'
		id10333 column:'id10333'
		id10334 column:'id10334'
		id10335 column:'id10335'
		id10336 column:'id10336'
		id10337 column:'id10337'
		id10338 column:'id10338'
		id10339 column:'id10339'
		id10340 column:'id10340'
		id10342 column:'id10342'
		id10343 column:'id10343'
		id10344 column:'id10344'
		id10347 column:'id10347'
		id10351 column:'id10351'
		id10352 column:'id10352'
		id10354 column:'id10354'
		id10355 column:'id10355'
		id10356 column:'id10356'
		id10357 column:'id10357'
		motherDeath column:'mother_death'
		id103581 column:'id10358_1'
		id10358 column:'id10358'
		id10359 column:'id10359'
		id10360 column:'id10360'
		id10361 column:'id10361'
		id10362 column:'id10362'
		id10363 column:'id10363'
		id10364 column:'id10364'
		id10365 column:'id10365'
		id10366 column:'id10366'
		id10367 column:'id10367'
		id10368 column:'id10368'
		id10369 column:'id10369'
		id10370 column:'id10370'
		id10371 column:'id10371'
		id10372 column:'id10372'
		id10373 column:'id10373'
		id10376 column:'id10376'
		id10377 column:'id10377'
		lastTimeMoved column:'last_time_moved'
		id10379 column:'id10379'
		id10380 column:'id10380'
		id10382 column:'id10382'
		id10383 column:'id10383'
		id10384 column:'id10384'
		id10385 column:'id10385'
		id10387 column:'id10387'
		id10388 column:'id10388'
		id10389 column:'id10389'
		id10391 column:'id10391'
		id10392 column:'id10392'
		id10393 column:'id10393'
		id10394 column:'id10394'
		id10395 column:'id10395'
		id10396 column:'id10396'
		id10397 column:'id10397'
		id10398 column:'id10398'
		id10399 column:'id10399'
		id10400 column:'id10400'
		id10401 column:'id10401'
		id10402 column:'id10402'
		id10403 column:'id10403'
		id10404 column:'id10404'
		id10405 column:'id10405'
		id10406 column:'id10406'
		id10408 column:'id10408'
		id104081 column:'id10408_1'
		id10411 column:'id10411'
		id10412 column:'id10412'
		id10413 column:'id10413'
		id10414 column:'id10414'
		id10415 column:'id10415'
		id10418 column:'id10418'
		id10419 column:'id10419'
		id10420 column:'id10420'
		id10421 column:'id10421'
		id10422 column:'id10422'
		id10423 column:'id10423'
		id10424 column:'id10424'
		id10425 column:'id10425'
		id10426 column:'id10426'
		id10427 column:'id10427'
		id10428 column:'id10428'
		id10429 column:'id10429'
		id10430 column:'id10430'
		id10431 column:'id10431'
		id10432 column:'id10432'

		id10433_1_Th column:'id10433_1_th'
		id10433_2_Hp column:'id10433_2_hp'
		id10433_3_Rl column:'id10433_3_rl'
		id10433_4_Gh column:'id10433_4_gh'
		id10433_5_Gc column:'id10433_5_gc'
		id10433_6_Ph column:'id10433_6_ph'
		id10433_7_Cp column:'id10433_7_cp'
		id10433_8_Ta column:'id10433_8_ta'
		id10433_9_Pp column:'id10433_9_pp'
		id10433_10_Rf column:'id10433_10_rf'
		id10433_11_Ph column:'id10433_11_ph'
		id10433_12_Dk column:'id10433_12_dk'
		id10433_13_Rf column:'id10433_13_rf'

		id10434 column:'id10434'
		id10435 column:'id10435'
		id10436 column:'id10436'
		id10437 column:'id10437'
		id10438 column:'id10438'
		id10439 column:'id10439'
		id10440 column:'id10440'
		id10441 column:'id10441'
		id10442 column:'id10442'
		id10443 column:'id10443'
		id10444 column:'id10444'
		id10445 column:'id10445'
		id10446 column:'id10446'
		id10450 column:'id10450'
		id10451 column:'id10451'
		id10452 column:'id10452'
		id10453 column:'id10453'
		id10454 column:'id10454'
		id10455 column:'id10455'
		id10456 column:'id10456'
		id10457 column:'id10457'
		id10458 column:'id10458'
		id10459 column:'id10459'
		id10462 column:'id10462'
		id10463 column:'id10463'
		id10464 column:'id10464'
		id10465 column:'id10465'
		id10466 column:'id10466'
		id10467 column:'id10467'
		id10468 column:'id10468'
		id10469 column:'id10469'
		id10470 column:'id10470'
		id10471 column:'id10471'
		id10472 column:'id10472'
		id10473 column:'id10473'
		id10476 column:'id10476'

		id10477_1_Ck column:'id10477_1_ck'
		id10477_2_Dl column:'id10477_2_dl'
		id10477_3_Fv column:'id10477_3_fv'
		id10477_4_Ha column:'id10477_4_ha'
		id10477_5_Hp column:'id10477_5_hp'
		id10477_6_Jd column:'id10477_6_jd'
		id10477_7_Lf column:'id10477_7_lf'
		id10477_8_Ml column:'id10477_8_ml'
		id10477_9_Pn column:'id10477_9_pn'
		id10477_10_Rk column:'id10477_10_rk'
		id10477_11_Sc column:'id10477_11_sc'
		id10477_12_No column:'id10477_12_no'
		id10477_13_Dk column:'id10477_13_dk'

		id10478_1_Ab column:'id10478_1_ab'
		id10478_2_Cc column:'id10478_2_cc'
		id10478_3_Dh column:'id10478_3_dh'
		id10478_4_Dg column:'id10478_4_dg'
		id10478_5_Dr column:'id10478_5_dr'
		id10478_6_Fv column:'id10478_6_fv'
		id10478_7_Hp column:'id10478_7_hp'
		id10478_8_Jd column:'id10478_8_jd'
		id10478_9_Pn column:'id10478_9_pn'
		id10478_10_Rs column:'id10478_10_rs'
		id10478_11_No column:'id10478_11_no'
		id10478_12_Dk column:'id10478_12_dk'

		id10479_1_Ax column:'id10479_1_ax'
		id10479_2_Ib column:'id10479_2_ib'
		id10479_3_Lp column:'id10479_3_lp'
		id10479_4_Pn column:'id10479_4_pn'
		id10479_5_Pd column:'id10479_5_pd'
		id10479_6_Rd column:'id10479_6_rd'
		id10479_7_No column:'id10479_7_no'
		id10479_8_Dk column:'id10479_8_dk'
		id10481 column:'id10481'
	}

	static constraints = {

		id10236 nullable: true, maxSize: 4
		id10237 nullable: true, maxSize: 4
		id10238 nullable: true, maxSize: 4
		id10239 nullable: true, maxSize: 4
		id10240 nullable: true, maxSize: 4
		id10241 nullable: true, maxSize: 4
		id10242 nullable: true, maxSize: 4
		id10243 nullable: true, maxSize: 4
		id10244 nullable: true, maxSize: 4
		id10245 nullable: true, maxSize: 4
		id10246 nullable: true, maxSize: 4
		id10247 nullable: true, maxSize: 4
		id10248 nullable: true
		id10249 nullable: true, maxSize: 4
		id10250 nullable: true
		id10251 nullable: true, maxSize: 4
		id10252 nullable: true, maxSize: 4
		id10253 nullable: true, maxSize: 4
		id10254 nullable: true, maxSize: 4
		id10255 nullable: true, maxSize: 4
		id10256 nullable: true, maxSize: 4
		id10257 nullable: true, maxSize: 4
		id10258 nullable: true, maxSize: 4
		id10259 nullable: true, maxSize: 4

		id10260_1_Rs nullable: true, maxSize: 4
		id10260_2_Ls nullable: true, maxSize: 4
		id10260_3_Lp nullable: true, maxSize: 4
		id10260_4_Up nullable: true, maxSize: 4
		id10260_5_Ol nullable: true, maxSize: 4
		id10260_6_Oa nullable: true, maxSize: 4
		id10260_7_Wb nullable: true, maxSize: 4
		id10260_8_Ot nullable: true, maxSize: 4

		id10261 nullable: true, maxSize: 4
		id10262 nullable: true
		id10263 nullable: true, maxSize: 10
		id10264 nullable: true, maxSize: 4
		id10265 nullable: true, maxSize: 4
		id10266 nullable: true
		id10267 nullable: true, maxSize: 4
		id10268 nullable: true, maxSize: 4
		id10269 nullable: true, maxSize: 4
		id10270 nullable: true, maxSize: 4
		id10271 nullable: true, maxSize: 4
		id10272 nullable: true, maxSize: 4
		id10273 nullable: true, maxSize: 4
		id10274 nullable: true
		id10275 nullable: true, maxSize: 4
		id10276 nullable: true, maxSize: 4
		id10277 nullable: true, maxSize: 4
		id10278 nullable: true, maxSize: 4
		id10279 nullable: true, maxSize: 4
		id10281 nullable: true, maxSize: 4
		id10282 nullable: true, maxSize: 4
		id10283 nullable: true, maxSize: 4
		id10284 nullable: true, maxSize: 4
		id10285 nullable: true
		id10286 nullable: true, maxSize: 4
		id10287 nullable: true, maxSize: 4
		id10288 nullable: true, maxSize: 4
		id10289 nullable: true, maxSize: 4
		id10290 nullable: true, maxSize: 4
		id10294 nullable: true, maxSize: 4
		id10295 nullable: true, maxSize: 4
		id10296 nullable: true, maxSize: 4
		id10297 nullable: true, maxSize: 4
		id10298 nullable: true, maxSize: 4
		id10299 nullable: true, maxSize: 4
		id10300 nullable: true, maxSize: 4
		id10301 nullable: true, maxSize: 4
		id10302 nullable: true, maxSize: 4
		id10303 nullable: true
		id10304 nullable: true, maxSize: 4
		id10305 nullable: true, maxSize: 4
		id10306 nullable: true, maxSize: 4
		id10307 nullable: true, maxSize: 4
		id10308 nullable: true, maxSize: 4
		id10309 nullable: true
		id10310 nullable: true, maxSize: 4
		id10312 nullable: true, maxSize: 4
		id10313 nullable: true, maxSize: 4
		id10314 nullable: true, maxSize: 4
		id10315 nullable: true, maxSize: 4
		id10316 nullable: true, maxSize: 4
		id10317 nullable: true, maxSize: 4
		id10318 nullable: true, maxSize: 4
		id10319 nullable: true
		id10320 nullable: true, maxSize: 4
		id10321 nullable: true, maxSize: 4
		id10322 nullable: true, maxSize: 4
		id10323 nullable: true, maxSize: 4
		id10324 nullable: true, maxSize: 4
		id10325 nullable: true, maxSize: 4
		id10326 nullable: true, maxSize: 4
		id10327 nullable: true, maxSize: 4
		id10328 nullable: true, maxSize: 4
		id10329 nullable: true, maxSize: 4
		id10330 nullable: true, maxSize: 4
		id10331 nullable: true, maxSize: 4
		id10332 nullable: true
		id10333 nullable: true, maxSize: 4
		id10334 nullable: true, maxSize: 4
		id10335 nullable: true, maxSize: 4
		id10336 nullable: true, maxSize: 4
		id10337 nullable: true, maxSize: 40
		id10338 nullable: true, maxSize: 4
		id10339 nullable: true, maxSize: 40
		id10340 nullable: true, maxSize: 4
		id10342 nullable: true, maxSize: 4
		id10343 nullable: true, maxSize: 4
		id10344 nullable: true, maxSize: 4
		id10347 nullable: true, maxSize: 4
		id10351 nullable: true
		id10352 nullable: true
		id10354 nullable: true, maxSize: 4
		id10355 nullable: true, maxSize: 40
		id10356 nullable: true, maxSize: 4
		id10357 nullable: true, maxSize: 40
		motherDeath nullable: true, maxSize: 10
		id103581 nullable: true
		id10358 nullable: true
		id10359 nullable: true
		id10360 nullable: true, maxSize: 40
		id10361 nullable: true, maxSize: 4
		id10362 nullable: true, maxSize: 4
		id10363 nullable: true, maxSize: 4
		id10364 nullable: true, maxSize: 4
		id10365 nullable: true, maxSize: 4
		id10366 nullable: true
		id10367 nullable: true
		id10368 nullable: true, maxSize: 4
		id10369 nullable: true, maxSize: 4
		id10370 nullable: true, maxSize: 4
		id10371 nullable: true, maxSize: 4
		id10372 nullable: true, maxSize: 4
		id10373 nullable: true, maxSize: 4
		id10376 nullable: true
		id10377 nullable: true
		lastTimeMoved nullable: true
		id10379 nullable: true
		id10380 nullable: true
		id10382 nullable: true
		id10383 nullable: true, maxSize: 4
		id10384 nullable: true, maxSize: 4
		id10385 nullable: true, maxSize: 20
		id10387 nullable: true, maxSize: 4
		id10388 nullable: true, maxSize: 4
		id10389 nullable: true, maxSize: 4
		id10391 nullable: true, maxSize: 4
		id10392 nullable: true
		id10393 nullable: true, maxSize: 4
		id10394 nullable: true
		id10395 nullable: true, maxSize: 4
		id10396 nullable: true, maxSize: 4
		id10397 nullable: true, maxSize: 4
		id10398 nullable: true, maxSize: 4
		id10399 nullable: true, maxSize: 4
		id10400 nullable: true, maxSize: 4
		id10401 nullable: true, maxSize: 4
		id10402 nullable: true, maxSize: 4
		id10403 nullable: true, maxSize: 4
		id10404 nullable: true, maxSize: 4
		id10405 nullable: true, maxSize: 4
		id10406 nullable: true, maxSize: 4
		id10408 nullable: true, maxSize: 4
		id104081 nullable: true, maxSize: 4
		id10411 nullable: true, maxSize: 4
		id10412 nullable: true, maxSize: 4
		id10413 nullable: true, maxSize: 4
		id10414 nullable: true, maxSize: 40
		id10415 nullable: true
		id10418 nullable: true, maxSize: 4
		id10419 nullable: true, maxSize: 4
		id10420 nullable: true, maxSize: 4
		id10421 nullable: true, maxSize: 4
		id10422 nullable: true, maxSize: 4
		id10423 nullable: true, maxSize: 4
		id10424 nullable: true, maxSize: 4
		id10425 nullable: true, maxSize: 4
		id10426 nullable: true, maxSize: 4
		id10427 nullable: true, maxSize: 4
		id10428 nullable: true, maxSize: 4
		id10429 nullable: true, maxSize: 4
		id10430 nullable: true, maxSize: 4
		id10431 nullable: true, maxSize: 255
		id10432 nullable: true, maxSize: 4

		id10433_1_Th nullable: true, maxSize: 4
		id10433_2_Hp nullable: true, maxSize: 4
		id10433_3_Rl nullable: true, maxSize: 4
		id10433_4_Gh nullable: true, maxSize: 4
		id10433_5_Gc nullable: true, maxSize: 4
		id10433_6_Ph nullable: true, maxSize: 4
		id10433_7_Cp nullable: true, maxSize: 4
		id10433_8_Ta nullable: true, maxSize: 4
		id10433_9_Pp nullable: true, maxSize: 4
		id10433_10_Rf nullable: true, maxSize: 4
		id10433_11_Ph nullable: true, maxSize: 4
		id10433_12_Dk nullable: true, maxSize: 4
		id10433_13_Rf nullable: true, maxSize: 4

		id10434 nullable: true, maxSize: 200
		id10435 nullable: true, maxSize: 4
		id10436 nullable: true, maxSize: 200
		id10437 nullable: true, maxSize: 4
		id10438 nullable: true, maxSize: 4
		id10439 nullable: true
		id10440 nullable: true
		id10441 nullable: true
		id10442 nullable: true, scale: 10
		id10443 nullable: true, scale: 10
		id10444 nullable: true, maxSize: 200
		id10445 nullable: true, maxSize: 4
		id10446 nullable: true, maxSize: 4
		id10450 nullable: true, maxSize: 4
		id10451 nullable: true, maxSize: 4
		id10452 nullable: true, maxSize: 4
		id10453 nullable: true, maxSize: 4
		id10454 nullable: true, maxSize: 4
		id10455 nullable: true, maxSize: 4
		id10456 nullable: true, maxSize: 4
		id10457 nullable: true, maxSize: 4
		id10458 nullable: true, maxSize: 4
		id10459 nullable: true, maxSize: 4
		id10462 nullable: true, maxSize: 4
		id10463 nullable: true, maxSize: 4
		id10464 nullable: true
		id10465 nullable: true
		id10466 nullable: true
		id10467 nullable: true
		id10468 nullable: true
		id10469 nullable: true
		id10470 nullable: true
		id10471 nullable: true
		id10472 nullable: true
		id10473 nullable: true
		id10476 nullable: true, maxSize: 1000

		id10477_1_Ck nullable: true, maxSize: 4
		id10477_2_Dl nullable: true, maxSize: 4
		id10477_3_Fv nullable: true, maxSize: 4
		id10477_4_Ha nullable: true, maxSize: 4
		id10477_5_Hp nullable: true, maxSize: 4
		id10477_6_Jd nullable: true, maxSize: 4
		id10477_7_Lf nullable: true, maxSize: 4
		id10477_8_Ml nullable: true, maxSize: 4
		id10477_9_Pn nullable: true, maxSize: 4
		id10477_10_Rk nullable: true, maxSize: 4
		id10477_11_Sc nullable: true, maxSize: 4
		id10477_12_No nullable: true, maxSize: 4
		id10477_13_Dk nullable: true, maxSize: 4

		id10478_1_Ab nullable: true, maxSize: 4
		id10478_2_Cc nullable: true, maxSize: 4
		id10478_3_Dh nullable: true, maxSize: 4
		id10478_4_Dg nullable: true, maxSize: 4
		id10478_5_Dr nullable: true, maxSize: 4
		id10478_6_Fv nullable: true, maxSize: 4
		id10478_7_Hp nullable: true, maxSize: 4
		id10478_8_Jd nullable: true, maxSize: 4
		id10478_9_Pn nullable: true, maxSize: 4
		id10478_10_Rs nullable: true, maxSize: 4
		id10478_11_No nullable: true, maxSize: 4
		id10478_12_Dk nullable: true, maxSize: 4

		id10479_1_Ax nullable: true, maxSize: 4
		id10479_2_Ib nullable: true, maxSize: 4
		id10479_3_Lp nullable: true, maxSize: 4
		id10479_4_Pn nullable: true, maxSize: 4
		id10479_5_Pd nullable: true, maxSize: 4
		id10479_6_Rd nullable: true, maxSize: 4
		id10479_7_No nullable: true, maxSize: 4
		id10479_8_Dk nullable: true, maxSize: 4
		id10481 nullable: true
	}
}
