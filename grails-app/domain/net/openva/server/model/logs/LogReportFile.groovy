package net.openva.server.model.logs

/**
 *
 */
class LogReportFile {
    Date creationDate
    String fileName
    int processedCount
    int errorsCount

    String getOnlyFileName(){

        if (fileName != null)
            return fileName.substring(fileName.lastIndexOf("/")+1)

        return ""
    }

    String toString(){
        getOnlyFileName()
    }

    static belongsTo = [logReport : LogReport]

    static constraints = {

    }

    def beforeInsert = {
        if (processedCount == null)
            processedCount = 0
        if (errorsCount == null){
            errorsCount = 0
        }
    }
}
