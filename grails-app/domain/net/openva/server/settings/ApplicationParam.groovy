package net.openva.server.settings

import net.openva.security.authentication.User

class ApplicationParam {
    String name
    String type
    String value

    User createdBy
    Date creationDate
    User updatedBy
    Date updatedDate

    static constraints = {
        name blank:false, unique: true
        type blank:true
        value blank:true

        createdBy nullable: true
        creationDate nullable: true
        updatedBy nullable: true
        updatedDate nullable:true
    }

    transient springSecurityService

    static transients = ['springSecurityService']

    def beforeInsert() {
        createdBy  =  springSecurityService.currentUser
        this.creationDate = new Date()
    }

    def beforeUpdate() {
        this.updatedBy = springSecurityService.currentUser
        this.updatedDate = new Date()
    }
}
