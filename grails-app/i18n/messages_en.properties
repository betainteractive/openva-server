default.application.name=OpenVA: Verbal Autopsy Collector Server Application

default.doesnt.match.message=Property [{0}] of class [{1}] with value [{2}] does not match the required pattern [{3}]
default.invalid.url.message=Property [{0}] of class [{1}] with value [{2}] is not a valid URL
default.invalid.creditCard.message=Property [{0}] of class [{1}] with value [{2}] is not a valid credit card number
default.invalid.email.message=Property [{0}] of class [{1}] with value [{2}] is not a valid e-mail address
default.invalid.range.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid range from [{3}] to [{4}]
default.invalid.size.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid size range from [{3}] to [{4}]
default.invalid.max.message=Property [{0}] of class [{1}] with value [{2}] exceeds maximum value [{3}]
default.invalid.min.message=Property [{0}] of class [{1}] with value [{2}] is less than minimum value [{3}]
default.invalid.max.size.message=Property [{0}] of class [{1}] with value [{2}] exceeds the maximum size of [{3}]
default.invalid.min.size.message=Property [{0}] of class [{1}] with value [{2}] is less than the minimum size of [{3}]
default.invalid.validator.message=Property [{0}] of class [{1}] with value [{2}] does not pass custom validation
default.not.inlist.message=Property [{0}] of class [{1}] with value [{2}] is not contained within the list [{3}]
default.blank.message=Property [{0}] of class [{1}] cannot be blank
default.not.equal.message=Property [{0}] of class [{1}] with value [{2}] cannot equal [{3}]
default.null.message=Property [{0}] of class [{1}] cannot be null
default.not.unique.message=Property [{0}] of class [{1}] with value [{2}] must be unique

default.paginate.prev=Previous
default.paginate.next=Next
default.boolean.true=True
default.boolean.false=False
default.date.format=yyyy-MM-dd HH:mm:ss z
default.number.format=0

default.created.message={0} {1} created
default.updated.message={0} {1} updated
default.deleted.message={0} {1} deleted
default.not.deleted.message={0} {1} could not be deleted
default.not.found.message={0} not found with id {1}
default.optimistic.locking.failure=Another user has updated this {0} while you were editing

default.home.label=Home
default.list.label={0} List
default.add.label=Add {0}
default.new.label=New {0}
default.create.label=Create {0}
default.show.label=Show {0}
default.edit.label=Edit {0}
default.import.label=Import {0}
default.export.label=Export {0}

default.execute.label=Execute

default.button.create.label=Create
default.button.edit.label=Edit
default.button.update.label=Update
default.button.delete.label=Delete
default.button.delete.confirm.message=Are you sure?

default.file.not.found=Error! File not Found

# Data binding errors. Use "typeMismatch.$className.$propertyName to customize (eg typeMismatch.Book.author)
typeMismatch.java.net.URL=Property {0} must be a valid URL
typeMismatch.java.net.URI=Property {0} must be a valid URI
typeMismatch.java.util.Date=Property {0} must be a valid Date
typeMismatch.java.lang.Double=Property {0} must be a valid number
typeMismatch.java.lang.Integer=Property {0} must be a valid number
typeMismatch.java.lang.Long=Property {0} must be a valid number
typeMismatch.java.lang.Short=Property {0} must be a valid number
typeMismatch.java.math.BigDecimal=Property {0} must be a valid number
typeMismatch.java.math.BigInteger=Property {0} must be a valid number
typeMismatch=Property {0} is type-mismatched

default.main.welcome.title=Welcome to OpenVA
default.main.welcome.msg=This software its a server application that manages Verbal Autopsy Data collected using a mobile app <OpenVA Collect>
default.main.welcome.links=Useful links
default.main.welcome.developer=Developed by:    Paulo Filimone
default.main.welcome.developer_email=Email: paulphilimone@gmail.com

# Main Menu
default.menu.home.label=HOME
default.menu.users.label=USERS
default.menu.add.label=UPDATES
default.menu.import.label=IMPORT
default.menu.add.import.label=Import Data
default.menu.add.death.label=Add Death Registration
default.menu.add.individual.label=Add Live Individuals
default.menu.sync.label=EXPORT
default.menu.sync.export.label=Export XML/ZIPs
default.menu.tools.label=TOOLS
default.menu.settings.label=Settings
default.menu.vatools.label=VA Interpretator Tools
default.menu.reports.label=REPORTS
default.menu.stats.label=STATS

default.menu.add.import.title=Synchronization Tasks - Import Data to OpenVA
default.menu.sync.execute.title=Synchronization Tasks - Export Files to XML/ZIP
default.menu.updates.tasks.title=Update Tasks

default.gender.M=Male
default.gender.F=Female

default.language.select=Set Language

# Setting and parameters
openhds.individual.custom_code=Custom OpenHDS Individual Code 
openhds.death.min_date=Minimum Death Date (from OpenHDS)
openva.odk.form_id=WHO ODK Form Id

role.administrator.label=Administrator
role.datamanager.label=Data Manager
role.fieldworker.label=Field Worker

logstatus.started.label=Started
logstatus.finished.label=Finished
logstatus.error.label=Error occurred
logstatus.not.started.label=Never Started

logreport.odk.sync.label=Synchronize WHO VA ODK to OpenVA Tables
logreport.import.fieldworkers.label=Synchronize OpenHDS FieldWorkers to OpenVA tables
logreport.import.individuals.label=Synchronize OpenHDS Live Individuals to OpenVA tables
logreport.import.deaths.label=Synchronize OpenHDS Deaths to OpenVA tables
logreport.vacontrol.update.label=Update Verbal Autopsy Control List
logreport.vacontrol.interpret.label=Interpret Verbal Autopsy Data using VA Tools
logreport.vacontrol.calc.stats.label=Calculate Verbal Autopsy Stats
logreport.restexport.zip_xml_files.label=Generate ZIP/XML Files to be synchronized to the Mobile App
logreport.export.users.zip_xml_files.label=Generate ZIP/XML Files for Users
logreport.export.individuals.zip_xml_files.label=Generate ZIP/XML Files for Live Individuals
logreport.export.vacontrol.zip_xml_files.label=Generate ZIP/XML Files for VA Control List
logreport.export.stats.zip_xml_files.label=Generate ZIP/XML Files for VA Stats

logreport.show.label=Show Report:  {0}
logreport.reportId.label=Report Id
logreport.group.label=Group
logreport.status.label=Status
logreport.description.label=Report Description
logreport.start.label=Last Sync Start
logreport.end.label=Last Sync End

# LogReportFile messages, Customized by X-47
logReportFile.id.label=Id
logReportFile.creationDate.label=Creation Date
logReportFile.errorsCount.label=Errors Count
logReportFile.fileName.label=File Name
logReportFile.logReport.label=Log Report
logReportFile.processedCount.label=Processed Count


login.label=Login to system
login.username.label=Username
login.password.label=Password
login.remember.me.label=Remember me
login.tips.label=Tips:
login.forgot.password.label=I forgot my password
login.system.version.label=System Version:
login.button.label=Login
login.as.label=Logged as
login.not.logged.label=Not Logged
login.logout.label=Logout

notification.status.label={0} UnReaded Notifications

settings.params.title=Codes and Parameters
settings.params.empty=There is no list of parameters avaiable
settings.updated.label=Settings were updated!
settings.format.integer.error=The value [{0}] of the code [{1}] is not an integer
settings.format.empty.error=The value cannot be empty

# User messages, Customized by X-47
user.label=User
user.roles.label=Permission Roles
user.add.label=Add User
user.import.label=Import User
user.search.label=Search User
user.create.label=Create User
user.edit.label=Edit User
user.list.label=Users List
user.new.label=Add User
user.show.label=Show User
user.create.button.label=Save User
user.update.button.label=Update User
user.delete.button.label=Delete User
user.edit.button.label=Edit User
user.created=User {0} created
user.updated=User {0} updated
user.deleted=User {0} deleted
user.not.found=User not found with id {0}
user.not.deleted=User not deleted with id {0}
user.optimistic.locking.failure=Another user has updated this User while you were editing
user.id.label=Id
user.firstName.label=First Name
user.firstName.blank.error=Property [First Name] of class [User] cannot be blank
user.firstName.nullable.error=Property [First Name] of class [User] cannot be null
user.lastName.label=Last Name
user.lastName.blank.error=Property [Last Name] of class [User] cannot be blank
user.lastName.nullable.error=Property [Last Name] of class [User] cannot be null
user.fullName.label=Full Name
user.username.label=Username
user.username.blank.error=Property [Username] of class [User] cannot be blank
user.username.nullable.error=Property [Username] of class [User] cannot be null
user.password.label=Password
user.password.blank.error=Property [Password] of class [User] cannot be blank
user.password.nullable.error=Property [Password] of class [User] cannot be null
user.createdBy.label=Created By
user.creationDate.label=Creation Date
user.updatedBy.label=Updated By
user.updatedDate.label=Updated Date
user.totalAttempts.label=Total Collection Attempts
user.totalAttempts.nullable.error=Property [Total Collection Attempts] of class [User] cannot be null
user.totalCollected.label=Total VA's Collected
user.totalCollected.nullable.error=Property [Total VA's Collected] of class [User] cannot be null
user.accountExpired.label=Account Expired
user.accountExpired.nullable.error=Property [Account Expired] of class [User] cannot be null
user.accountLocked.label=Account Locked
user.accountLocked.nullable.error=Property [Account Locked] of class [User] cannot be null
user.enabled.label=Enabled
user.enabled.nullable.error=Property [Enabled] of class [User] cannot be null
user.passwordExpired.label=Password Expired
user.passwordExpired.nullable.error=Property [Password Expired] of class [User] cannot be null
user.authorities.label=Authorities
user.authoritiesText.label=Authorities Text
user.isPasswordEncoded.label=Is Password Encoded
user.springSecurityService.label=Spring Security Service


# VerbalAutopsyControl messages, Customized by X-47
verbalAutopsyControl.label=Death Registration
verbalAutopsyControl.add.label=Add Death Registration
verbalAutopsyControl.import.label=Import Death Registration
verbalAutopsyControl.search.label=Search Death Registration
verbalAutopsyControl.create.label=Create Death Registration
verbalAutopsyControl.edit.label=Edit Death Registration
verbalAutopsyControl.list.label=Death Registration List
verbalAutopsyControl.new.label=New Death Registration
verbalAutopsyControl.show.label=Show Death Registration
verbalAutopsyControl.create.button.label=Save Death Registration
verbalAutopsyControl.update.button.label=Update Death Registration
verbalAutopsyControl.delete.button.label=Delete Death Registration
verbalAutopsyControl.edit.button.label=Edit Death Registration
verbalAutopsyControl.created=Death Registration {0} created
verbalAutopsyControl.updated=Death Registration {0} updated
verbalAutopsyControl.deleted=Death Registration {0} deleted
verbalAutopsyControl.not.found=Death Registration not found with id {0}
verbalAutopsyControl.not.deleted=Death Registration not deleted with id {0}
verbalAutopsyControl.optimistic.locking.failure=Another user has updated this Death Registration while you were editing
verbalAutopsyControl.id.label=Id
verbalAutopsyControl.extId.label=Ext Id
verbalAutopsyControl.code.label=Code
verbalAutopsyControl.code.nullable.error=Property [Code] of class [Death Registration] cannot be null
verbalAutopsyControl.code.blank.error=Property [Code] of class [Death Registration] cannot be blank
verbalAutopsyControl.name.label=Name
verbalAutopsyControl.name.nullable.error=Property [Name] of class [Death Registration] cannot be null
verbalAutopsyControl.name.blank.error=Property [Name] of class [Death Registration] cannot be blank
verbalAutopsyControl.gender.label=Gender
verbalAutopsyControl.gender.nullable.error=Property [Gender] of class [Death Registration] cannot be null
verbalAutopsyControl.dateOfBirth.label=Date Of Birth
verbalAutopsyControl.dateOfDeath.label=Date Of Death
verbalAutopsyControl.ageAtDeath.label=Age At Death
verbalAutopsyControl.ageAtDeath.min.error=Property [Age At Death] of class [Death Registration] with value [{2}] is less than minimum value [{3}]
verbalAutopsyControl.ageAtDeath.nullable.error=Property [Age At Death] of class [Death Registration] cannot be null
verbalAutopsyControl.motherId.label=Mother Id
verbalAutopsyControl.motherName.label=Mother Name
verbalAutopsyControl.fatherId.label=Father Id
verbalAutopsyControl.fatherName.label=Father Name
verbalAutopsyControl.householdId.label=Household Id
verbalAutopsyControl.householdNo.label=Household No
verbalAutopsyControl.gpsAccuracy.label=Gps Accuracy
verbalAutopsyControl.gpsAltitude.label=Gps Altitude
verbalAutopsyControl.gpsLatitude.label=Gps Latitude
verbalAutopsyControl.gpsLongitude.label=Gps Longitude
verbalAutopsyControl.deathUuid.label=Death Uuid
verbalAutopsyControl.vaType.label=Verbal Autopsy Type
verbalAutopsyControl.vaUuid.label=Verbal Autopsy Uuid
verbalAutopsyControl.vaCollected.label=Collected
verbalAutopsyControl.vaCollected.nullable.error=Property [Collected] of class [Death Registration] cannot be null
verbalAutopsyControl.vaProcessedWith.label=Verbal Autopsy Processed With
verbalAutopsyControl.vaProcessed.label=Processed
verbalAutopsyControl.vaResult.label=Verbal Autopsy  Result
verbalAutopsyControl.vaAttempts.label=Total Collection Attempts
verbalAutopsyControl.collectionAttempts.label=Collection Attempts


# Individual messages, Customized by X-47
individual.label=Individual
individual.add.label=Add Individual
individual.import.label=Import Individual
individual.search.label=Search Individual
individual.create.label=Create Individual
individual.edit.label=Edit Individual
individual.list.label=Individual List
individual.new.label=New Individual
individual.show.label=Show Individual
individual.create.button.label=Save Individual
individual.update.button.label=Update Individual
individual.delete.button.label=Delete Individual
individual.edit.button.label=Edit Individual
individual.created=Individual {0} created
individual.updated=Individual {0} updated
individual.deleted=Individual {0} deleted
individual.not.found=Individual not found with id {0}
individual.not.deleted=Individual not deleted with id {0}
individual.optimistic.locking.failure=Another user has updated this Individual while you were editing
individual.id.label=Id
individual.extId.label=Ext Id
individual.code.label=Code
individual.code.nullable.error=Property [Code] of class [Individual] cannot be null
individual.code.blank.error=Property [Code] of class [Individual] cannot be blank
individual.name.label=Name
individual.name.nullable.error=Property [Name] of class [Individual] cannot be null
individual.name.blank.error=Property [Name] of class [Individual] cannot be blank
individual.gender.label=Gender
individual.gender.blank.error=Property [Gender] of class [Individual] cannot be blank
individual.gender.nullable.error=Property [Gender] of class [Individual] cannot be null
individual.dateOfBirth.label=Date Of Birth
individual.dateOfBirth.nullable.error=Property [Date Of Birth] of class [Individual] cannot be null
individual.age.label=Age
individual.age.nullable.error=Property [Age] of class [Individual] cannot be null
individual.motherId.label=Mother Id
individual.motherName.label=Mother Name
individual.fatherId.label=Father Id
individual.fatherName.label=Father Name
individual.householdId.label=Household Id
individual.householdNo.label=Household No
individual.gpsAccuracy.label=Gps Accuracy
individual.gpsAltitude.label=Gps Altitude
individual.gpsLatitude.label=Gps Latitude
individual.gpsLongitude.label=Gps Longitude

#stats
stats.id.label=Id
stats.export.label=Export Stats
stats.logReport.label=Log Report
stats.createdBy.label=Created By
stats.creationDate.label=Creation Date

stats.totalDeaths.label=Total of Deaths Registered
stats.totalCollected.label=Total of Verbal Autopsies Collected
stats.totalToCollect.label=Total of Verbal Autopsies to Collect

stats.user.attempts.title=User collection stats
stats.attempts.title=Collection Attempts Stats

stats.totalAttempts.label=Total of Collection Attempts
stats.totalAttemptNi.label=Total of attempts with reason "No Informant"
stats.totalAttemptUh.label=Total of attempts with reason "Uninhabited House"
stats.totalAttemptWdr.label=Total of attempts with reason "Wrong Death Record"
stats.totalAttemptOt.label=Total of attempts with reason "Other Reason"
stats.totalAttemptNc.label=Total of attempts with reason "Not consented"





