package net.openva.server

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import grails.core.GrailsApplication
import grails.core.support.GrailsApplicationAware
import grails.util.Holders
import net.openva.io.SystemPath
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.context.ApplicationContextAware
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.core.env.PropertiesPropertySource
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource

class Application extends GrailsAutoConfiguration implements EnvironmentAware {

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    @Override
    void setEnvironment(Environment environment) {

        String conf = "openva.config.location"

        def configFile = getExternalConfigurationFile()

        //println "path: ${configFile}, ${conf}"

        if (configFile != null){
            Resource resourceConfig = new FileSystemResource(configFile)

            YamlPropertiesFactoryBean ypfb = new YamlPropertiesFactoryBean()

            ypfb.setResources([resourceConfig] as Resource[])
            ypfb.afterPropertiesSet()

            Properties properties = ypfb.getObject()

            environment.propertySources.addFirst(new PropertiesPropertySource(conf, properties))
        }
    }

    File getExternalConfigurationFile(){
        def url = getClass().classLoader.getResource("openva-config.yml")

        return new File(url.toURI())
    }

}