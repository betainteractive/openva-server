package net.openva.server

import net.openva.io.SystemPath
import net.openva.security.authentication.*
import net.openva.server.Codes
import net.openva.server.model.VerbalAutopsyControl
import net.openva.server.model.logs.LogGroup
import net.openva.server.model.logs.LogStatus
import net.openva.server.model.logs.LogReport
import net.openva.server.settings.ApplicationParam
import java.io.File


class BootStrap {

    def servletContext
    def generalUtilitiesService
    def grailsApplication
    def importDataService
    def verbalAutopsyControlService
    def exportFilesService
    def dynamicOdkSynchronizationService

    def init = { servletContext ->

        createDirectories(servletContext)
        configSecurityMap()
        defaultAppUser()
        insertDefaults()
        //testapp()
    }

    def destroy = {
    }

    def testapp(){
        //println grailsApplication.config.dataSource_openhds.driverClassName
        //println importDataService.getIndividualColumnNames() //getIndividualCustomCodeMap("status")
        //println importDataService.getIndividualCustomCodeMap("code")
        //importDataService.importLiveIndividualsFromOpenHDS(2)
        //importDataService.importDeathsFromOpenHDS(3)
        //importDataService.importFieldWorkersFromOpenHDS(4)

        //println "va ${VerbalAutopsyControl.list().max{it.dateOfDeath}}"
        //exportFilesService.generateUsersXML(8)
        //exportFilesService.generateLiveIndividualXML(8)
        //exportFilesService.generateVaControlXML(8)
        //exportFilesService.generateVaStatsXML(8)

        dynamicOdkSynchronizationService.executeTransfer(Codes.REPORT_IMPORT_FROM_ODK_CORES)

        assert 1==0
    }

    boolean isUnix(){
        return File.listRoots().first().getPath().equalsIgnoreCase("/")
    }

    def createDirectories(def servletContext) {

        def homeKey = isUnix() ? "HOME" : "HOMEPATH"

        def homeDir = servletContext.getRealPath("/") // System.getenv().get(homeKey);
        def appDir = "${homeDir}${SystemPath.APP_PATH}"
        def binDir = "${homeDir}${SystemPath.BIN_PATH}"
        def logDir = "${homeDir}${SystemPath.LOG_PATH}"
        def resDir = "${homeDir}${SystemPath.RESOURCES_PATH}"

        SystemPath.HOME_PATH = homeDir

        println "Homepath: ${homeDir}"

        //create app dir
        println "Creating dir "+appDir
        println "created: " + new File(appDir).mkdirs()

        //create bin dir
        println "Creating dir "+binDir
        println "created: " + new File(binDir).mkdirs()

        //create log dir
        println "Creating dir "+logDir
        println "created: " + new File(logDir).mkdirs()

        //create resources dir
        println "Creating dir "+resDir
        println "created: " + new File(resDir).mkdirs()
    }

    def configSecurityMap(){
        def svc = generalUtilitiesService

        if (Role.count() == 0){
            new Role(name: "role.administrator.label", authority: Role.ROLE_ADMINISTRATOR).save(flush: true)
            new Role(name: "role.datamanager.label", authority: Role.ROLE_DATA_MANAGER).save(flush: true)
            new Role(name: "role.fieldworker.label", authority: Role.ROLE_FIELD_WORKER).save(flush: true)
        }

        if (true){

            SecurityMap.list().each{
                it.delete(flush: true)
            }

            for (String url in [
                    '/', '/**/favicon.ico',
                    '/**/js/**', '/**/css/**', '/**/images/**',
                    '/login', '/login.*', '/login/*', '/index', '/index.gsp',
                    '/logout', '/logout.*', '/logout/*', '/error**', '/shutdown',

                    '/**/assets/**',
                    '/**/images/**',
                    '/**/javascripts/**',
                    '/**/stylesheets/**'
            ]) {
                new SecurityMap(url: url, configAttribute: 'permitAll').save()
            }


            //new SecurityMap(url: "/index", configAttribute: "IS_AUTHENTICATED_FULLY").save(flush: true)

            new SecurityMap(url: "/user/*/**", configAttribute: "${Role.ROLE_ADMINISTRATOR}").save(flush: true)


            new SecurityMap(url: "/exportFiles/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/generalUtilities/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/importData/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/export/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/updates/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)

            new SecurityMap(url: "/verbalAutopsyControl/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/individual/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)

            new SecurityMap(url: "/settings/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/reports/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/logReport/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)

            //rest api access
            new SecurityMap(url: "/api/login", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/api/export/individuals/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/api/export/users/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/api/export/vacontrols/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)
            new SecurityMap(url: "/api/export/stats/**", configAttribute: "${Role.ROLE_ADMINISTRATOR},${Role.ROLE_DATA_MANAGER}").save(flush: true)


        }
    }

    def defaultAppUser (){
        //Default ApplicationUser
        if (User.count()==0){
            User user = new User()
            user.firstName = "System"
            user.lastName = "Administrator"
            user.username = "admin";
            user.password = "admin";

            println ("hasErrors: "+user.hasErrors())
            println ("saving: "+user.save(flush: true)+" user: "+user)

            Role admin = Role.findByAuthority(Role.ROLE_ADMINISTRATOR)

            println ("admin: "+admin)

            UserRole.create(user, admin)
        }
    }

    def insertDefaults(){
        if (LogStatus.count() == 0){
            new LogStatus(name: LogStatus.STARTED).save(flush: true)
            new LogStatus(name: LogStatus.FINISHED).save(flush: true)
            new LogStatus(name: LogStatus.ERROR).save(flush: true)
            new LogStatus(name: LogStatus.NOT_STARTED).save(flush: true)
        }

        //Inserting Log Groups
        new LogGroup(groupId: Codes.GROUP_ODK,            name: "ODK",      description: "").save(flush: true)
        new LogGroup(groupId: Codes.GROUP_IMPORT_DATA, name: "OPENHDS",  description: "").save(flush: true)
        new LogGroup(groupId: Codes.GROUP_UPDATE_VA,      name: "UPDATES",  description: "").save(flush: true)
        new LogGroup(groupId: Codes.GROUP_STATS,          name: "STATS",    description: "").save(flush: true)
        new LogGroup(groupId: Codes.GROUP_GENERATE_FILES, name: "GENFILES", description: "").save(flush: true)


        //Inserting Log Reports

        /* Group Import from OpenHDS */
        new LogReport(
                reportId: Codes.REPORT_IMPORT_FROM_ODK_CORES,
                group: LogGroup.findByGroupId(Codes.GROUP_IMPORT_DATA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.odk.sync.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_IMPORT_FROM_OPENHDS_FIELDWORKERS,
                group: LogGroup.findByGroupId(Codes.GROUP_IMPORT_DATA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.import.fieldworkers.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_IMPORT_FROM_OPENHDS_INDIVIDUALS,
                group: LogGroup.findByGroupId(Codes.GROUP_IMPORT_DATA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.import.individuals.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_IMPORT_FROM_OPENHDS_DEATHS,
                group: LogGroup.findByGroupId(Codes.GROUP_IMPORT_DATA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.import.deaths.label'
        ).save(flush: true)

        /* Group Update VA */
        new LogReport(
                reportId: Codes.REPORT_UPDATE_VERBAL_AUTOPSY_CONTROL,
                group: LogGroup.findByGroupId(Codes.GROUP_UPDATE_VA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.vacontrol.update.label'
        ).save(flush: true)

        /* Group Interpret VA Data */
        new LogReport(
                reportId: Codes.REPORT_INTERPRET_VERBAL_AUTOPSY_DATA,
                group: LogGroup.findByGroupId(Codes.GROUP_INTERPRET_VA),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.vacontrol.interpret.label'
        ).save(flush: true)

        /* Group Stats */
        new LogReport(
                reportId: Codes.REPORT_VERBAL_AUTOPSY_STATS,
                group: LogGroup.findByGroupId(Codes.GROUP_STATS),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.vacontrol.calc.stats.label'
        ).save(flush: true)

        /* Group Generate Files */
        new LogReport(
                reportId: Codes.REPORT_GENERATE_USERS_ZIP_XML_FILES,
                group: LogGroup.findByGroupId(Codes.GROUP_GENERATE_FILES),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.export.users.zip_xml_files.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_GENERATE_INDIVIDUALS_ZIP_XML_FILES,
                group: LogGroup.findByGroupId(Codes.GROUP_GENERATE_FILES),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.export.individuals.zip_xml_files.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_GENERATE_VACONTROL_ZIP_XML_FILES,
                group: LogGroup.findByGroupId(Codes.GROUP_GENERATE_FILES),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.export.vacontrol.zip_xml_files.label'
        ).save(flush: true)

        new LogReport(
                reportId: Codes.REPORT_GENERATE_STATS_ZIP_XML_FILES,
                group: LogGroup.findByGroupId(Codes.GROUP_GENERATE_FILES),
                status: LogStatus.findByName(LogStatus.NOT_STARTED),
                description: 'logreport.export.stats.zip_xml_files.label'
        ).save(flush: true)

        //Inserting defaults system params

        def customCode = grailsApplication.config.openva.openhds.individual.custom_code
        println "code: ${customCode}"

        new ApplicationParam(name: Codes.PARAMS_OPENHDS_INDIVIDUAL_CUSTOM_CODE, type: "string", value: customCode).save(flush: true) /* HDSS Site specific individual code variable*/
        new ApplicationParam(name: Codes.PARAMS_OPENHDS_MINIMUM_DEATH_DATE, type: "date", value: "1970-01-01").save(flush: true) /* Minimum Date of Death to retrieve records from OpenHDS */
        new ApplicationParam(name: Codes.PARAMS_OPENVA_ODK_FORM_ID, type: "string", value: "who_va_2016").save(flush: true) /* WHO VA 2016 Form Id in ODK  */

    }
}
