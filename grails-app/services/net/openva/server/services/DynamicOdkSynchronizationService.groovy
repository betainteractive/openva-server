package net.openva.server.services

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import net.betainteractive.io.LogOutput
import net.openva.io.SystemPath
import net.openva.odk.model.*
import net.openva.openhds.model.Individual
import net.openva.server.io.Copy
import net.openva.server.model.WhoVa2016
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus
import org.grails.orm.hibernate.cfg.GrailsDomainBinder
import org.hibernate.jdbc.Work

import java.sql.Connection
import java.sql.SQLException

class DynamicOdkSynchronizationService {
    static datasource = ['odk']

    static transactional = false

    def generalUtilitiesService

    def executeTransfer(long logReportId) {

        transferToWhoVa2016(logReportId)

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.save(flush: true)
        }
    }

    def transferToWhoVa2016(long reportId) {
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "db-synch-odk-va-2016");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0

        def records = []

        //update null to 0
        println "updating processed null to processed zero"
        Whova2016Core.withTransaction {
            Whova2016Core.executeUpdate("update Whova2016Core w set w.processed=0 where w.processed is null")
        }

        println "reading who va records from ODK"
        Whova2016Core.withTransaction {
            records = Whova2016Core.executeQuery("select w.uri from Whova2016Core w where w.processed=0")
        }

        println "WhoVa2016 ${records.size()} records to copy from odk to final table"

        int from = 0;
        int to = 0;
        int max = 500

        /*
        while (processed < records.size()) {
            from = to
            to = (records.size() > to + max) ? (to + max) : records.size()

        }*/

        records.each { uri ->
            processed++
            Whova2016Core.withTransaction {
                WhoVa2016.withTransaction {

                    Whova2016Core core = Whova2016Core.findByUri(uri)


                    def core1 = getRecordsFromVaCore(uri)
                    def core2 = getRecordsFromVaCore2(uri)
                    def core3 = getRecordsFromVaCore3(uri)
                    def core4 = getRecordsFromVaCore4(uri)
                    def core5 = getRecordsFromVaCore5(uri)
                    def core6 = getRecordsFromVaCore6(uri)
                    def core7 = getRecordsFromVaCore7(uri)
                    def core8 = getRecordsFromVaCore8(uri)

                    /* multiple selection questions */
                    def id10173_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10173.findAllByParentAuri(uri)
                    def id10235_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10235.findAllByParentAuri(uri)
                    def id10260_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10260.findAllByParentAuri(uri)

                    def id10433_opts = Whova2016ConsntdLlhstoryHealthServiceUtlztonId10433.findAllByParentAuri(uri)

                    def id10477_opts = Whova2016ConsentedNarratId10477.findAllByParentAuri(uri)
                    def id10478_opts = Whova2016ConsentedNarratId10478.findAllByParentAuri(uri)
                    def id10479_opts = Whova2016ConsentedNarratId10479.findAllByParentAuri(uri)

                    WhoVa2016 whova = new WhoVa2016()

                    //odk identification columns
                    whova.sysOdkUri = core.uri
                    whova.sysOdkCreatorUriUser = core.creatorUriUser
                    whova.sysOdkCreationDate = core.creationDate
                    whova.sysOdkLastUpdateUriUser = core.lastUpdateUriUser
                    whova.sysOdkLastUpdateDate = core.lastUpdateDate
                    whova.sysOdkModelVersion = core.modelVersion
                    whova.sysOdkUiVersion = core.uiVersion
                    whova.sysOdkIsComplete = core.isComplete
                    whova.sysOdkSubmissionDate = core.submissionDate
                    whova.sysOdkMarkedAsCompleteDate = core.markedAsCompleteDate
                    whova.processed = 0

                    Copy.fromTo(core1, whova)
                    Copy.fromTo(core2, whova)
                    Copy.fromTo(core3, whova)
                    Copy.fromTo(core4, whova)
                    Copy.fromTo(core5, whova)
                    Copy.fromTo(core6, whova)
                    Copy.fromTo(core7, whova)
                    Copy.fromTo(core8, whova)

                    id10173_opts.each { opt ->
                        final value = opt.value
                        switch (value){
                            case 'stridor'  : whova.id10173_1_St = "yes"; break
                            case 'grunting' : whova.id10173_2_Gr = "yes"; break
                            case 'wheezing' : whova.id10173_3_Wz = "yes"; break
                            case 'no'       : whova.id10173_4_No = "yes"; break
                            case 'dk'       : whova.id10173_5_Dk = "yes"; break
                            case 'ref'      : whova.id10173_6_Rf = "yes"; break
                        }
                    }

                    id10235_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'face'        : whova.id10235_1_Fc = "yes"; break
                            case 'trunk'       : whova.id10235_2_Tk = "yes"; break
                            case 'extremities' : whova.id10235_3_Et = "yes"; break
                            case 'everywhere'  : whova.id10235_4_Ev = "yes"; break
                        }
                    }

                    id10260_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'right_side'         : whova.id10260_1_Rs = "yes"; break
                            case 'left_side'          : whova.id10260_2_Ls = "yes"; break
                            case 'lower_part_of_body' : whova.id10260_3_Lp = "yes"; break
                            case 'upper_part_of_body' : whova.id10260_4_Up = "yes"; break
                            case 'one_leg_only'       : whova.id10260_5_Ol = "yes"; break
                            case 'one_arm_only'       : whova.id10260_6_Oa = "yes"; break
                            case 'whole_body'         : whova.id10260_7_Wb = "yes"; break
                            case 'other'              : whova.id10260_8_Ot = "yes"; break
                        }
                    }

                    id10433_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'traditional_healer'                   : whova.id10433_1_Th = "yes"; break
                            case 'homeopath'                            : whova.id10433_2_Hp = "yes"; break
                            case 'religious_leader'                     : whova.id10433_3_Rl = "yes"; break
                            case 'government_hospital'                  : whova.id10433_4_Gh = "yes"; break
                            case 'government_health_center_or_clinic'   : whova.id10433_5_Gc = "yes"; break
                            case 'private_hospital'                     : whova.id10433_6_Ph = "yes"; break
                            case 'community_based_practitionerinsystem' : whova.id10433_7_Cp = "yes"; break
                            case 'trained_birth_attendant'              : whova.id10433_8_Ta = "yes"; break
                            case 'private_physician'                    : whova.id10433_9_Pp = "yes"; break
                            case 'relative_friend'                      : whova.id10433_10_Rf = "yes"; break
                            case 'pharmacy'                             : whova.id10433_11_Ph = "yes"; break
                            case 'dk'                                   : whova.id10433_12_Dk = "yes"; break
                            case 'ref'                                  : whova.id10433_13_Rf = "yes"; break
                        }
                    }

                    id10477_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'Chronic_kidney_disease' : whova.id10477_1_Ck = "yes"; break
                            case 'Dialysis'               : whova.id10477_2_Dl = "yes"; break
                            case 'Fever'                  : whova.id10477_3_Fv = "yes"; break
                            case 'Heart_attack'           : whova.id10477_4_Ha = "yes"; break
                            case 'Heart_problem'          : whova.id10477_5_Hp = "yes"; break
                            case 'Jaundice'               : whova.id10477_6_Jd = "yes"; break
                            case 'Liver_failure'          : whova.id10477_7_Lf = "yes"; break
                            case 'Malaria'                : whova.id10477_8_Ml = "yes"; break
                            case 'Pneumonia'              : whova.id10477_9_Pn = "yes"; break
                            case 'Renal_kidney_failure'   : whova.id10477_10_Rk = "yes"; break
                            case 'Suicide'                : whova.id10477_11_Sc = "yes"; break
                            case 'None'                   : whova.id10477_12_No = "yes"; break
                            case 'dk'                     : whova.id10477_13_Dk = "yes"; break
                        }
                    }

                    id10478_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'abdomen'       : whova.id10478_1_Ab = "yes"; break
                            case 'cancer'        : whova.id10478_2_Cc = "yes"; break
                            case 'dehydration'   : whova.id10478_3_Dh = "yes"; break
                            case 'dengue'        : whova.id10478_4_Dg = "yes"; break
                            case 'diarrhea'      : whova.id10478_5_Dr = "yes"; break
                            case 'fever'         : whova.id10478_6_Fv = "yes"; break
                            case 'heart_problem' : whova.id10478_7_Hp = "yes"; break
                            case 'jaundice'      : whova.id10478_8_Jd = "yes"; break
                            case 'pneumonia'     : whova.id10478_9_Pn = "yes"; break
                            case 'rash'          : whova.id10478_10_Rs = "yes"; break
                            case 'None'          : whova.id10478_11_No = "yes"; break
                            case 'dk'            : whova.id10478_12_Dk = "yes"; break
                        }
                    }

                    id10479_opts.each { opt ->
                        final value = opt.value
                        switch (value) {
                            case 'asphyxia'             : whova.id10479_1_Ax = "yes"; break
                            case 'incubator'            : whova.id10479_2_Ib = "yes"; break
                            case 'lung_problem'         : whova.id10479_3_Lp = "yes"; break
                            case 'pneumonia'            : whova.id10479_4_Pn = "yes"; break
                            case 'preterm_delivery'     : whova.id10479_5_Pd = "yes"; break
                            case 'respiratory_distress' : whova.id10479_6_Rd = "yes"; break
                            case 'None'                 : whova.id10479_7_No = "yes"; break
                            case 'dk'                   : whova.id10479_8_Dk = "yes"; break
                        }
                    }

                    if (whova.save(flush: true)){
                        core.processed = 1
                        println "saving whova ${processed}"
                    }else{
                        core.processed = 2 /* error occurred */
                        errors++

                        def msg = "Couldnt save WhoVa2016 from ODK Core with uri=${whova.sysOdkUri}"
                        def msgErr = "Errors: ${whova.errors}"

                        output.println(msg)
                        output.println(msgErr)
                        println(msg)
                        println(msgErr)
                    }

                    core.save(flush: true)
                }
            }

        }

        println "finished importing from odk!!!"

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(reportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.creationDate = new Date()
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()
    }

    /* Because ODK is generating the columns randomly through the tables in each Aggregate form deployemnt
     * With the methods below we will read and map all the data from core1 to core8 with this we'll bypass the randomly columns creation */

    Map<String,Object> getRecordsFromTableCore(String tableName, String byKeyColumn, String withKeyValue){
        def map = [:]
        //println tableName
        Whova2016Core.withSession { s ->
            s.doWork new Work(){
                void execute(Connection connection) throws SQLException {
                    def sql = new Sql(connection)
                    def query = "select * from " + tableName + " where ${byKeyColumn}='${withKeyValue}'"
                    def result = sql.rows(query)

                    //println result.toString()

                    result.each { GroovyRowResult it ->
                        map = it
                        //println "set: ${it}"
                    }
                }
            }
        }

        return map
    }

    Map<String,Object> getRecordsFromVaCore(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core.class).table.name
        return getRecordsFromTableCore(tableName, "_URI", uri)
    }

    Map<String,Object> getRecordsFromVaCore2(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core2.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore3(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core3.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore4(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core4.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore5(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core5.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore6(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core6.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore7(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core7.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

    Map<String,Object> getRecordsFromVaCore8(String uri){
        def tableName = GrailsDomainBinder.getMapping(Whova2016Core8.class).table.name
        return getRecordsFromTableCore(tableName, "_TOP_LEVEL_AURI", uri)
    }

}

