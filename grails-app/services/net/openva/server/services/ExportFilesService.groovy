package net.openva.server.services

import grails.gorm.transactions.Transactional
import net.betainteractive.io.LogOutput
import net.betainteractive.io.writers.ZipMaker
import net.betainteractive.utilities.StringUtil
import net.openva.io.SystemPath
import net.openva.security.authentication.User
import net.openva.server.model.Individual
import net.openva.server.model.VerbalAutopsyControl
import net.openva.server.model.VerbalAutopsyStats
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus
import org.w3c.dom.Document
import org.w3c.dom.Element

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

@Transactional
class ExportFilesService {

    /*
     * Generate files xml/zip - users, live_individuals, deaths/va_controls, stats
     */

    def generalUtilitiesService

    def generateUsersXML(long logReportId) {

        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "generate-users-xml-zip");
        PrintStream output = log.output
        if (output == null) return;

        def start = new Date();

        int processed = 0
        int errors = 0

        try {
            //Ler todos Individuos
            def resultUsers = []

            User.withTransaction {
                resultUsers = User.list()
            }

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("users");
            doc.appendChild(rootElement);


            int count = 0;

            resultUsers.each { user ->
                count++;
                Element elementUser = createUser(doc, user);
                rootElement.appendChild(elementUser);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(SystemPath.getAbsoluteResourcesPath() + File.separator + "users.xml"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved! - users.xml");
            output.println("File saved! - users.xml");

            //zip file
            ZipMaker zipMaker = new ZipMaker(SystemPath.getAbsoluteResourcesPath() + File.separator + "users.zip")
            zipMaker.addFile(SystemPath.getAbsoluteResourcesPath() + File.separator + "users.xml")
            def b = zipMaker.makeZip()

            println "creating zip - users.zip - success="+b

            processed = 1

        } catch (Exception ex) {
            ex.printStackTrace()
            processed = 0
            errors = 1
            output.println(ex.toString())
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            logReport.start = start
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.save()

            println "error 1: ${logReport.errors}, ${logReport.start}"

            LogReportFile reportFile = new LogReportFile(creationDate: logReport.start, fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.addToLogFiles(reportFile)
            logReport.save()

            println "error 2: ${logReport.errors}"
        }

        output.close();

    }

    def generateLiveIndividualXML(long logReportId) {

        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "generate-individual-xml-zip");
        PrintStream output = log.output
        if (output == null) return;

        def start = new Date();

        int processed = 0
        int errors = 0

        try {
            //Ler todos Individuos
            def individuals = []

            Individual.withTransaction {
                individuals = Individual.list()
            }

            println "creating xml file ${individuals.size()}"
            PrintStream outputFile = new PrintStream(new FileOutputStream(SystemPath.getAbsoluteResourcesPath() + File.separator + "individuals.xml"), true)

            outputFile.print("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><individuals>")

            individuals.each { individual ->
                outputFile.print(individual.toXML())
                individual = null
            }

            outputFile.print("</individuals>")
            outputFile.close()

            System.out.println("File saved! - individuals.xml");
            output.println("File saved! - individuals.xml");

            //zip file
            ZipMaker zipMaker = new ZipMaker(SystemPath.getAbsoluteResourcesPath() + File.separator + "individuals.zip")
            zipMaker.addFile(SystemPath.getAbsoluteResourcesPath() + File.separator + "individuals.xml")
            def b = zipMaker.makeZip()

            println "creating zip - individuals.zip - success="+b

            processed = 1

        } catch (Exception ex) {
            ex.printStackTrace()
            processed = 0
            errors = 1
            output.println(ex.toString())
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.start = start
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close();

    }

    def generateVaControlXML(long logReportId) {

        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "generate-vacontrol-xml-zip");
        PrintStream output = log.output
        if (output == null) return;

        def start = new Date();

        int processed = 0
        int errors = 0

        try {
            def results = []

            VerbalAutopsyControl.withTransaction {
                results = VerbalAutopsyControl.executeQuery("select v.id from VerbalAutopsyControl v where v.vaCollected=0 or v.vaCollected=2")
            }

            println "creating xml file ${results.size()}"
            PrintStream outputFile = new PrintStream(new FileOutputStream(SystemPath.getAbsoluteResourcesPath() + File.separator + "vacontrols.xml"), true)

            outputFile.print("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><vacontrols>")

            results.each { id ->
                def vacontrol = VerbalAutopsyControl.get(id)
                outputFile.print(vacontrol.toXML())
                vacontrol = null
            }

            outputFile.print("</vacontrols>")
            outputFile.close()

            System.out.println("File saved! - vacontrols.xml");
            output.println("File saved! - vacontrols.xml");

            //zip file
            ZipMaker zipMaker = new ZipMaker(SystemPath.getAbsoluteResourcesPath() + File.separator + "vacontrols.zip")
            zipMaker.addFile(SystemPath.getAbsoluteResourcesPath() + File.separator + "vacontrols.xml")
            def b = zipMaker.makeZip()

            println "creating zip - vacontrols.zip - success="+b

            processed = 1

        } catch (Exception ex) {
            ex.printStackTrace()
            processed = 0
            errors = 1
            output.println(ex.toString())
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.start = start
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close();

    }

    def generateVaStatsXML(long logReportId) {

        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "generate-va-stats-xml-zip");
        PrintStream output = log.output
        if (output == null) return;

        def start = new Date();

        int processed = 0
        int errors = 0

        try {
            def vaStats = null

            VerbalAutopsyStats.withTransaction {
                vaStats = VerbalAutopsyStats.last()
            }

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("vastats");
            doc.appendChild(rootElement);

            if (vaStats != null) {
                Element element = createVaStats(doc, vaStats);
                rootElement.appendChild(element);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(SystemPath.getAbsoluteResourcesPath() + File.separator + "vastats.xml"));

            // Output to console for testing
            transformer.transform(source, result);

            System.out.println("File saved! - vastats.xml");
            output.println("File saved! - vastats.xml");

            //zip file
            ZipMaker zipMaker = new ZipMaker(SystemPath.getAbsoluteResourcesPath() + File.separator + "vastats.zip")
            zipMaker.addFile(SystemPath.getAbsoluteResourcesPath() + File.separator + "vastats.xml")
            def b = zipMaker.makeZip()

            println "creating zip - vastats.zip - success="+b

            processed = 1

        } catch (Exception ex) {
            ex.printStackTrace()
            processed = 0
            errors = 1
            output.println(ex.toString())
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.start = start
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close();
    }

    private Element createUser(Document doc, User user) {
        Element individual = doc.createElement("user");

        individual.appendChild(createAttribute(doc, "username", user.getUsername()));
        individual.appendChild(createAttribute(doc, "password", user.getPassword()));
        individual.appendChild(createAttribute(doc, "firstName", user.getFirstName()));
        individual.appendChild(createAttribute(doc, "lastName", user.getLastName()));
        individual.appendChild(createAttribute(doc, "roles", user.getAuthoritiesText()));
        individual.appendChild(createAttribute(doc, "totalCollected", user.totalCollected))
        individual.appendChild(createAttribute(doc, "totalAttempts", user.totalAttempts))

        return individual;
    }

    private Element createLiveIndividual(Document doc, Individual ind) {
        Element element = doc.createElement("individual");

        element.appendChild(createAttribute(doc, "extId", ind.extId))
        element.appendChild(createAttribute(doc, "code", ind.code))
        element.appendChild(createAttribute(doc, "name", ind.name))
        element.appendChild(createAttribute(doc, "gender", ind.gender))
        element.appendChild(createAttribute(doc, "dateOfBirth", StringUtil.format(ind.dateOfBirth, "yyyy-MM-dd") ))
        element.appendChild(createAttribute(doc, "age", ind.age))

        element.appendChild(createAttribute(doc, "motherId", ind.motherId))
        element.appendChild(createAttribute(doc, "motherName", ind.motherName))
        element.appendChild(createAttribute(doc, "fatherId", ind.fatherId))
        element.appendChild(createAttribute(doc, "fatherName", ind.fatherName))

        element.appendChild(createAttribute(doc, "householdId", ind.householdId))
        element.appendChild(createAttribute(doc, "householdNo", ind.householdNo))
        element.appendChild(createAttribute(doc, "gpsAccuracy", ind.gpsAccuracy))
        element.appendChild(createAttribute(doc, "gpsAltitude", ind.gpsAltitude))
        element.appendChild(createAttribute(doc, "gpsLatitude", ind.gpsLatitude))
        element.appendChild(createAttribute(doc, "gpsLongitude", ind.gpsLongitude))

        return element
    }

    private Element createVaControl(Document doc, VerbalAutopsyControl vaControl) {

        def attempt = vaControl.collectionAttempts.max{it.autopsy.sysOdkStart}
        //println "testing attempt on export: ${attempt}"

        Element element = doc.createElement("vacontrol");

        element.appendChild(createAttribute(doc, "extId", vaControl.extId))
        element.appendChild(createAttribute(doc, "code", vaControl.code))
        element.appendChild(createAttribute(doc, "name", vaControl.name))
        element.appendChild(createAttribute(doc, "gender", vaControl.gender))
        element.appendChild(createAttribute(doc, "dateOfBirth", StringUtil.format(vaControl.dateOfBirth, "yyyy-MM-dd")))
        element.appendChild(createAttribute(doc, "dateOfDeath", StringUtil.format(vaControl.dateOfDeath, "yyyy-MM-dd")))
        element.appendChild(createAttribute(doc, "ageAtDeath", vaControl.ageAtDeath))

        element.appendChild(createAttribute(doc, "householdId", vaControl.householdId))
        element.appendChild(createAttribute(doc, "householdNo", vaControl.householdNo))

        element.appendChild(createAttribute(doc, "motherId", vaControl.motherId))
        element.appendChild(createAttribute(doc, "motherName", vaControl.motherName))
        element.appendChild(createAttribute(doc, "fatherId", vaControl.fatherId))
        element.appendChild(createAttribute(doc, "fatherName", vaControl.fatherName))

        element.appendChild(createAttribute(doc, "gpsAccuracy", vaControl.gpsAccuracy))
        element.appendChild(createAttribute(doc, "gpsAltitude", vaControl.gpsAltitude))
        element.appendChild(createAttribute(doc, "gpsLatitude", vaControl.gpsLatitude))
        element.appendChild(createAttribute(doc, "gpsLongitude", vaControl.gpsLongitude))

        element.appendChild(createAttribute(doc, "deathUuid", vaControl.deathUuid))

        element.appendChild(createAttribute(doc, "vaType", vaControl.vaType))
        element.appendChild(createAttribute(doc, "vaUuid", vaControl.vaUuid))
        element.appendChild(createAttribute(doc, "vaCollected", vaControl.vaCollected))
        element.appendChild(createAttribute(doc, "vaProcessedWith", vaControl.vaProcessedWith))
        element.appendChild(createAttribute(doc, "vaProcessed", vaControl.vaProcessed))
        element.appendChild(createAttribute(doc, "vaResult", vaControl.vaResult))

        element.appendChild(createAttribute(doc, "vaAttempts", vaControl.vaAttempts))
        element.appendChild(createAttribute(doc, "lastAttemptDate", attempt==null ? "" : attempt.autopsy.sysOdkStart))
        element.appendChild(createAttribute(doc, "lastAttemptReason", attempt==null ? "" : attempt.reason))
        //last retry reason and date (lastAttemptReason, lastAttemptDate)


        return element;
    }

    private Element createVaStats(Document doc, VerbalAutopsyStats stats) {
        Element element = doc.createElement("stats");

        element.appendChild(createAttribute(doc, "creationDate", StringUtil.format(stats.creationDate, "yyyy-MM-dd")))
        element.appendChild(createAttribute(doc, "totalDeaths", stats.totalDeaths));
        element.appendChild(createAttribute(doc, "totalToCollect", stats.totalToCollect));
        element.appendChild(createAttribute(doc, "totalCollected", stats.totalCollected));
        element.appendChild(createAttribute(doc, "totalAttempts", stats.totalAttempts));
        element.appendChild(createAttribute(doc, "totalAttemptNi", stats.totalAttemptNi));
        element.appendChild(createAttribute(doc, "totalAttemptUh", stats.totalAttemptUh));
        element.appendChild(createAttribute(doc, "totalAttemptWdr", stats.totalAttemptWdr));
        element.appendChild(createAttribute(doc, "totalAttemptOt", stats.totalAttemptOt));
        element.appendChild(createAttribute(doc, "totalAttemptNc", stats.totalAttemptNc));

        return element;
    }

    private Element createAttribute(Document doc, String name, String value){
        Element element = doc.createElement(name);
        element.appendChild(doc.createTextNode(value));
        return element;
    }

    private Element createAttribute(Document doc, String name, int value){
        Element element = doc.createElement(name);
        element.appendChild(doc.createTextNode(""+value));
        return element;
    }
}
