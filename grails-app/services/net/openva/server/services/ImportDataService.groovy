package net.openva.server.services

import grails.gorm.transactions.Transactional
import groovy.time.TimeCategory
import net.betainteractive.io.LogOutput
import net.betainteractive.utilities.GeneralUtil
import net.betainteractive.utilities.StringUtil
import net.openva.io.SystemPath
import net.openva.openhds.model.Death
import net.openva.openhds.model.Fieldworker
import net.openva.openhds.model.Individual
import net.openva.security.authentication.Role
import net.openva.security.authentication.User
import net.openva.security.authentication.UserRole
import net.openva.server.Codes
import net.openva.server.model.VerbalAutopsyControl
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus
import net.openva.server.settings.ApplicationParam
//import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass
import groovy.sql.Sql
import org.hibernate.SessionFactory
import org.hibernate.jdbc.Work

import java.sql.Connection
import java.sql.ResultSetMetaData
import java.sql.SQLException

@Transactional
class ImportDataService {
    static datasource = ['openhds']

    //static transactional = false

    def generalUtilitiesService
    def sessionFactory

    def importLiveIndividualsFromOpenHDS(long logReportId) {
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "db-individual-from-openhds");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0

        def final customCode = getIndividualCustomCode()
        def codeMap = [:] //map of extId and customCode
        def members = []

        def customCodeMapped = isCustomCodeMapped()

        def start = new Date()

        println "reading openhds individual and current residency ${start}"
        Individual.withTransaction {
            members = Individual.executeQuery("select i.uuid, i.extId, i.firstName, i.middleName, i.lastName, i.gender, i.dob, i.father.extId, i.father.firstName, i.father.middleName, i.father.lastName, i.mother.extId, i.mother.firstName, i.mother.middleName, i.mother.lastName, r.location.extId, r.location.locationName, r.startType, r.startDate, r.endType, r.endDate, r.location.accuracy, r.location.altitude, r.location.latitude, r.location.longitude from Individual i, Residency r where i.uuid=r.individualUuid and r.endType in ('NA','EXT') and r.startDate=(select max(r2.startDate) from Residency r2 where r2.individualUuid=r.individualUuid )")
            //members = Individual.executeQuery("select i.uuid, r.location.extId, i.extId, i.lastName, i.firstName, i.gender, i.dob, i.father.extId, i.father.lastName, i.father.firstName, i.mother.extId, i.mother.lastName, i.mother.firstName, r.startType, r.startDate, r.endType, r.endDate from Individual i, Residency r where i.uuid=r.individual.uuid and r.startDate=(select max(r2.startDate) from Residency r2 where r2.individual.uuid=r.individual.uuid )")
        }

        println "reading non-mapped individual codes - ${TimeCategory.minus(new Date(), start)}"
        if (!customCodeMapped) {
            codeMap = getIndividualCustomCodeMap(customCode)
        }

        println "clearing individual table - ${TimeCategory.minus(new Date(), start)}"
        net.openva.server.model.Individual.withNewTransaction{
            net.openva.server.model.Individual.executeUpdate("delete from net.openva.server.model.Individual")
        }

        println "executing copy to individual table - ${members.size()}, map ${codeMap}"

        int from = 0;
        int to = 0;
        int max = 2000

        while (processed < members.size()){
            from = to
            to = (members.size() > to+max) ? (to+max) : members.size(); //reading only some records to speed up the process

            net.openva.server.model.Individual.withNewTransaction {
                members.subList(from, to).each { mem ->
                    //perform saves
                    def uuid = mem[0]
                    def extid = mem[1]
                    def firstName = mem[2]
                    def middleName = mem[3]
                    def lastName = mem[4]
                    def gender = mem[5]
                    def dob = mem[6]
                    def fatherExtId = mem[7]
                    def fatherFname = mem[8]
                    def fatherMname = mem[9]
                    def fatherLname = mem[10]
                    def motherExtId = mem[11]
                    def motherFname = mem[12]
                    def motherMname = mem[13]
                    def motherLname = mem[14]
                    def locExtId = mem[15]
                    def locName = mem[16]
                    def startType = mem[17]
                    def startDate = mem[18]
                    def endType = mem[19]
                    def endDate = mem[20]
                    def accuracy = mem[21]
                    def altitude = mem[22]
                    def latitude = mem[23]
                    def longitude = mem[24]

                    net.openva.server.model.Individual individual = new net.openva.server.model.Individual()
                    individual.extId = extid
                    individual.code = extid
                    individual.name = StringUtil.getFullname(firstName, middleName, lastName)
                    individual.gender = gender
                    individual.dateOfBirth = dob
                    individual.age = GeneralUtil.getAge(individual.dateOfBirth)
                    individual.motherId = motherExtId
                    individual.motherName = StringUtil.getFullname(motherFname, motherMname, motherLname)
                    individual.fatherId = fatherExtId
                    individual.fatherName = StringUtil.getFullname(motherFname, motherMname, motherLname)

                    individual.householdId = locExtId
                    individual.householdNo = locName
                    individual.gpsAccuracy = accuracy
                    individual.gpsAltitude = altitude
                    individual.gpsLatitude = latitude
                    individual.gpsLongitude = longitude

                    /* check if the customized code is one of the default variables: firstName, middleName, lastName - other variables on individual are system dedicated  ones*/
                    /* some hdss sites will only use one variable for name - firstName would be the 1st choice */
                    if (customCodeMapped) {
                        switch (customCode) {
                            case 'firstName': individual.name = StringUtil.getFullname("", middleName, lastName);
                                individual.code = firstName
                                individual.motherName = StringUtil.getFullname("", motherMname, motherLname)
                                individual.fatherName = StringUtil.getFullname("", fatherMname, fatherLname)
                                break;
                            case 'middleName': individual.name = StringUtil.getFullname(firstName, "", lastName);
                                individual.code = middleName
                                individual.motherName = StringUtil.getFullname(motherFname, "", motherLname)
                                individual.fatherName = StringUtil.getFullname(fatherFname, "", fatherLname)
                                break
                            case 'lastName': individual.name = StringUtil.getFullname(firstName, middleName, "");
                                individual.code = lastName
                                individual.motherName = StringUtil.getFullname(motherFname, motherMname, "")
                                individual.fatherName = StringUtil.getFullname(fatherFname, fatherMname, "")
                                break;
                        }
                    } else {
                        //if customCode is a variable that wasnt in the OpenHDS Individual default table (was inserted by costumization)
                        if (!codeMap.isEmpty()) {
                            individual.code = codeMap.get(extid)
                        }
                    }

                    processed++

                    if (!individual.save(flush: true)) {
                        errors++

                        def msg = "Couldnt save Live Individual copied from OpenHDS extId=${individual.extId}"
                        def msgErr = "Errors: ${individual.errors}"

                        output.println(msg)
                        output.println(msgErr)
                        println(msg)
                        println(msgErr)
                    }

                    //println("saving ${processed}, ${individual.errors}")
                }
            }

            println("${max} reached clearing session - ${TimeCategory.minus(new Date(), start)}")
            sessionFactory.currentSession.flush() //clearing cache save us a lot of time
            sessionFactory.currentSession.clear()
        }

        println("finished generating!!! - ${TimeCategory.minus(new Date(), start)}")

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: logReport.start, fileName: log.logFileName, logReport: logReport)
            reportFile.creationDate = new Date()
            reportFile.processedCount = processed
            reportFile.errorsCount = errors

            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()

    }

    def importDeathsFromOpenHDS(long logReportId){
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "db-deaths-from-openhds");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0
        def start = new Date()

        println "starting generating deaths - ${start}"


        def minDate = getMinimumDeathDate()
        def customCode = getIndividualCustomCode()
        def codeMap = [:] //map of extId and customCode
        def customCodeMapped = isCustomCodeMapped()
        def deathUuids = []
        def deaths = []

        println "reading non-mapped individual codes - ${TimeCategory.minus(new Date(), start)}"
        if (!customCodeMapped) {
            codeMap = getIndividualCustomCodeMap(customCode)
        }

        println "custom mapped code: ${customCode}"
        println "reading existing va control - ${TimeCategory.minus(new Date(), start)}"
        VerbalAutopsyControl.withTransaction {
            deathUuids = VerbalAutopsyControl.executeQuery("select v.deathUuid from VerbalAutopsyControl v where v.deathUuid<>''")
        }

        println "${deathUuids.size()} va-controls exists, reading all deaths - ${TimeCategory.minus(new Date(), start)}"
        Death.withTransaction {
            if (deathUuids.size()>0){
                deaths = Death.executeQuery("select i.uuid, i.extId, i.firstName, i.middleName, i.lastName, i.gender, i.dob, i.father.extId, i.father.firstName, i.father.middleName, i.father.lastName, i.mother.extId, i.mother.firstName, i.mother.middleName, i.mother.lastName, l.extId, l.locationName, r.startType, r.startDate, r.endType, r.endDate, l.accuracy, l.altitude, l.latitude, l.longitude, d.deathDate, d.uuid from Death d, Individual i, Residency r, Location l where d.individualUuid=i.uuid and d.individualUuid=r.individualUuid and r.location.uuid=l.uuid and r.endType='DTH' and d.deathDate > :dthDate and d.uuid not in (:list)", [ dthDate:minDate, list:deathUuids])
            }else{
                deaths = Death.executeQuery("select i.uuid, i.extId, i.firstName, i.middleName, i.lastName, i.gender, i.dob, i.father.extId, i.father.firstName, i.father.middleName, i.father.lastName, i.mother.extId, i.mother.firstName, i.mother.middleName, i.mother.lastName, l.extId, l.locationName, r.startType, r.startDate, r.endType, r.endDate, l.accuracy, l.altitude, l.latitude, l.longitude, d.deathDate, d.uuid from Death d, Individual i, Residency r, Location l where d.individualUuid=i.uuid and d.individualUuid=r.individualUuid and r.location.uuid=l.uuid and r.endType='DTH' and d.deathDate > :dthDate", [ dthDate:minDate])
            }

        }

        println "processing deaths ${deaths.size()} - ${TimeCategory.minus(new Date(), start)}"

        int from = 0;
        int to = 0;
        int max = 1000

        while (processed < deaths.size()){
            from = to
            to = (deaths.size() > to+max) ? (to+max) : deaths.size(); //reading only some records to speed up the process

            VerbalAutopsyControl.withNewTransaction {
                deaths.subList(from, to).each { mem ->
                    //perform saves
                    def uuid = mem[0]
                    def extid = mem[1]
                    def firstName = mem[2]
                    def middleName = mem[3]
                    def lastName = mem[4]
                    def gender = mem[5]
                    def dob = mem[6]
                    def fatherExtId = mem[7]
                    def fatherFname = mem[8]
                    def fatherMname = mem[9]
                    def fatherLname = mem[10]
                    def motherExtId = mem[11]
                    def motherFname = mem[12]
                    def motherMname = mem[13]
                    def motherLname = mem[14]
                    def locExtId = mem[15]
                    def locName = mem[16]
                    def startType = mem[17]
                    def startDate = mem[18]
                    def endType = mem[19]
                    def endDate = mem[20]
                    def accuracy = mem[21]
                    def altitude = mem[22]
                    def latitude = mem[23]
                    def longitude = mem[24]
                    def deathDate = mem[25]
                    def deathUuid = mem[26]

                    VerbalAutopsyControl vaControl = new VerbalAutopsyControl()
                    vaControl.extId = extid
                    vaControl.code = extid
                    vaControl.name = StringUtil.getFullname(firstName, middleName, lastName)
                    vaControl.gender = gender
                    vaControl.dateOfBirth = dob
                    vaControl.motherId = motherExtId
                    vaControl.motherName = StringUtil.getFullname(motherFname, motherMname, motherLname)
                    vaControl.fatherId = fatherExtId
                    vaControl.fatherName = StringUtil.getFullname(motherFname, motherMname, motherLname)

                    vaControl.dateOfDeath = deathDate
                    vaControl.ageAtDeath = GeneralUtil.getAge(vaControl.dateOfBirth, vaControl.dateOfDeath)

                    vaControl.householdId = locExtId
                    vaControl.householdNo = locName
                    vaControl.gpsAccuracy = accuracy
                    vaControl.gpsAltitude = altitude
                    vaControl.gpsLatitude = latitude
                    vaControl.gpsLongitude = longitude

                    vaControl.deathUuid = deathUuid

                    vaControl.vaType = getVaType(vaControl.dateOfBirth, vaControl.dateOfDeath)
                    vaControl.vaUuid = ""
                    vaControl.vaCollected = 0
                    vaControl.vaProcessedWith = ""
                    vaControl.vaProcessed = 0
                    vaControl.vaResult = ""
                    vaControl.vaAttempts = 0


                    /* check if the customized code is one of the default variables: firstName, middleName, lastName - other variables on individual are system dedicated  ones*/
                    /* some hdss sites will only use one variable for name - firstName would be the 1st choice */
                    if (customCodeMapped) {
                        switch (customCode) {
                            case 'firstName': vaControl.name = StringUtil.getFullname("", middleName, lastName);
                                vaControl.code = firstName
                                vaControl.motherName = StringUtil.getFullname("", motherMname, motherLname)
                                vaControl.fatherName = StringUtil.getFullname("", fatherMname, fatherLname)
                                break;
                            case 'middleName': vaControl.name = StringUtil.getFullname(firstName, "", lastName);
                                vaControl.code = middleName
                                vaControl.motherName = StringUtil.getFullname(motherFname, "", motherLname)
                                vaControl.fatherName = StringUtil.getFullname(fatherFname, "", fatherLname)
                                break
                            case 'lastName': vaControl.name = StringUtil.getFullname(firstName, middleName, "");
                                vaControl.code = lastName
                                vaControl.motherName = StringUtil.getFullname(motherFname, motherMname, "")
                                vaControl.fatherName = StringUtil.getFullname(fatherFname, fatherMname, "")
                                break;
                        }
                    } else {
                        //if customCode is a variable that wasnt in the OpenHDS Individual default table (was inserted by costumization)
                        if (!codeMap.isEmpty()) {
                            vaControl.code = codeMap.get(extid)
                        }
                    }

                    processed++

                    if (!vaControl.save(flush: true)) {
                        errors++

                        def msg = "Couldnt save VAControl copied from OpenHDS Death uuid=${vaControl.deathUuid}/${extid}/${vaControl.code}"
                        def msgErr = "Errors: ${vaControl.errors}"

                        output.println(msg)
                        output.println(msgErr)
                        println(msg)
                        println(msgErr)
                    }

                    //println("saving ${processed}, ${individual.errors}")
                }
            }

            println("${max} reached clearing session - ${TimeCategory.minus(new Date(), start)}")
            sessionFactory.currentSession.flush() //clearing cache save us a lot of time
            sessionFactory.currentSession.clear()
        }

        println("finished generating deaths!!! - ${TimeCategory.minus(new Date(), start)}")


        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.creationDate = new Date()
            reportFile.processedCount = processed
            reportFile.errorsCount = errors

            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()
    }

    def importFieldWorkersFromOpenHDS(long logReportId){
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "db-fieldworkers-from-openhds");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0
        def start = new Date()

        println "starting copy of fieldworkers - ${start}"


        println "reading existing va control - ${TimeCategory.minus(new Date(), start)}"

        def fieldworkers = []

        Fieldworker.withTransaction {
            fieldworkers = Fieldworker.findAll()
        }

        println "processing fieldworkers ${fieldworkers.size()} - ${TimeCategory.minus(new Date(), start)}"

        int from = 0;
        int to = 0;
        int max = 1000

        while (processed < fieldworkers.size()){
            from = to
            to = (fieldworkers.size() > to+max) ? (to+max) : fieldworkers.size(); //reading only some records to speed up the process

            User.withNewTransaction {
                def role = Role.findByAuthority(Role.ROLE_FIELD_WORKER)

                fieldworkers.subList(from, to).each { fw ->
                    //perform saves
                    def uuid = fw.uuid
                    def extid = fw.extId
                    def firstName = fw.firstName
                    def lastName = fw.lastName
                    def password = fw.passwordHash

                    User user = User.findOrCreateByUsername(extid)
                    //User user = new User()
                    user.firstName = firstName
                    user.lastName = lastName
                    user.username = extid
                    user.password = password
                    user.isPasswordEncoded = true

                    processed++

                    if (user.save(flush: true)) {
                        UserRole.create(user, role)
                    }else{
                        errors++
                        def msg = "Couldnt create/save User copied from OpenHDS Fielworker with uuid=${fw.uuid}"
                        def msgErr = "Errors: ${user.errors}"

                        output.println(msg)
                        output.println(msgErr)
                        println(msg)
                        println(msgErr)
                    }

                    //println("saving ${processed}, ${individual.errors}")
                }
            }

            println("${max} reached clearing session - ${TimeCategory.minus(new Date(), start)}")
            sessionFactory.currentSession.flush() //clearing cache save us a lot of time
            sessionFactory.currentSession.clear()
        }

        println("finished generating deaths!!! - ${TimeCategory.minus(new Date(), start)}")


        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.creationDate = new Date()
            reportFile.processedCount = processed
            reportFile.errorsCount = errors

            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()
    }

    int getVaType(Date dateOfBirth, Date dateOfDeath) {
        int ageInDays = GeneralUtil.getAgeInDays(dateOfBirth, dateOfDeath)
        if (ageInDays <= 27) return 1                         //neonate
        if (ageInDays > 27 && ageInDays < 11*365.25) return 2 //child
        if (ageInDays > 11*365.25) return 3                   //adult
        return 0
    }

    Date getMinimumDeathDate(){
        def minDate = GeneralUtil.getDate(1900, 1, 1) //default minimum date

        ApplicationParam.withTransaction {
            def appParam = ApplicationParam.findByName(Codes.PARAMS_OPENHDS_MINIMUM_DEATH_DATE)
            if (appParam != null){
                def value = appParam.value
                minDate = StringUtil.toDate(value, "yyyy-MM-dd")
            }

        }

        return minDate
    }

    String getIndividualCustomCode(){
        def customCode = ""

        ApplicationParam.withTransaction {
            customCode = ApplicationParam.findByName(Codes.PARAMS_OPENHDS_INDIVIDUAL_CUSTOM_CODE).value
        }

        return customCode
    }

    boolean isCustomCodeMapped(){
        def customCode = ""
        def list = []

        ApplicationParam.withTransaction {
            customCode = ApplicationParam.findByName(Codes.PARAMS_OPENHDS_INDIVIDUAL_CUSTOM_CODE).value
        }



        //def d = new DefaultGrailsDomainClass(Individual.class)
        //list = d.persistentProperties.name.unique()

        //println "columns ${list}"



        return getIndividualColumnNames().contains(customCode) //list.contains(customCode)
    }

    def Map<String, String> getIndividualCustomCodeMap(String customColumn){
        Map<String, String> map = [:]

        println "testing direct database connection xc"

        //Accessing directly the openhds.individual table jumping hibernate mapping - some hdss sites may have added extra columns into openhds tables
        Individual.withNewSession { s ->
            s.doWork new Work() {
                void execute(Connection connection) throws SQLException {
                    def sql = new Sql(connection)
                    sql.eachRow("select extId, " + customColumn + " from individual") {
                        //println "${it}"
                        map.put(it[0], it[1])
                    }
                }
            }
        }

        return map
    }

    /**
     * Accessing directly the openhds.individual table jumping hibernate mapping to retrieve column names - some hdss sites may have added extra columns into openhds tables
     * @return
     */
    List<String> getIndividualColumnNames(){
        def list = []

        Individual.withNewSession { s ->
            s.doWork new Work(){
                void execute(Connection connection) throws SQLException {
                    def sql = new Sql(connection)

                    sql.rows("select * from individual limit 1", { ResultSetMetaData meta ->
                        (1..meta.getColumnCount()).each {
                            //println "ind.column ${it} - ${meta.getColumnName(it)}"
                            list.add(meta.getColumnName(it))
                        }
                    }).each {
                        //println "set: ${it}"
                    }
                }
            }
        }

        return list
    }


}
