package net.openva.server.services

import grails.gorm.transactions.Transactional
import net.betainteractive.io.LogOutput
import net.openva.io.SystemPath
import net.openva.odk.model.Whova2016ConsentedNarratId10477
import net.openva.odk.model.Whova2016ConsentedNarratId10478
import net.openva.odk.model.Whova2016ConsentedNarratId10479
import net.openva.odk.model.Whova2016ConsntdLlhstoryHealthServiceUtlztonId10433
import net.openva.odk.model.Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10173
import net.openva.odk.model.Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10235
import net.openva.odk.model.Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10260
import net.openva.odk.model.Whova2016Core
import net.openva.odk.model.Whova2016Core2
import net.openva.odk.model.Whova2016Core3
import net.openva.odk.model.Whova2016Core4
import net.openva.odk.model.Whova2016Core5
import net.openva.odk.model.Whova2016Core6
import net.openva.odk.model.Whova2016Core7
import net.openva.odk.model.Whova2016Core8
import net.openva.server.io.Copy
import net.openva.server.model.WhoVa2016
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus

@Transactional
class OdkSynchronizationService {
    static datasource = ['odk']

    static transactional = false

    def generalUtilitiesService

    def executeTransfer(long logReportId) {

        transferToWhoVa2016(logReportId)

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.save(flush: true)
            println "asd - ${logReportId}, ${logReport.status} - " + logReport.errors
        }
    }

    def transferToWhoVa2016(long reportId) {
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "db-synch-odk-va-2016");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0

        def records = []

        //update null to 0
        println "updating processed null to processed zero"
        Whova2016Core.withTransaction {
            Whova2016Core.executeUpdate("update Whova2016Core w set w.processed=0 where w.processed is null")
        }

        println "reading who va records from ODK"
        Whova2016Core.withTransaction {
            records = Whova2016Core.executeQuery("select w.uri from Whova2016Core w where w.processed=0")
        }

        println "WhoVa2016 ${records.size()} records to copy from odk to final table"

        records.each { uri ->
            processed++

            WhoVa2016.withTransaction {

                Whova2016Core core1 = Whova2016Core.findByUri(uri)
                Whova2016Core2 core2 = Whova2016Core2.findByParentAuri(uri)
                Whova2016Core3 core3 = Whova2016Core3.findByParentAuri(uri)
                Whova2016Core4 core4 = Whova2016Core4.findByParentAuri(uri)
                Whova2016Core5 core5 = Whova2016Core5.findByParentAuri(uri)
                Whova2016Core6 core6 = Whova2016Core6.findByParentAuri(uri)
                Whova2016Core7 core7 = Whova2016Core7.findByParentAuri(uri)
                Whova2016Core8 core8 = Whova2016Core8.findByParentAuri(uri)

                def id10173_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10173.findAllByParentAuri(uri)
                def id10235_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10235.findAllByParentAuri(uri)
                def id10260_opts = Whova2016ConsntdLlhstrySignsSmptmsFnlLlnessId10260.findAllByParentAuri(uri)

                def id10433_opts = Whova2016ConsntdLlhstoryHealthServiceUtlztonId10433.findAllByParentAuri(uri)

                def id10477_opts = Whova2016ConsentedNarratId10477.findAllByParentAuri(uri)
                def id10478_opts = Whova2016ConsentedNarratId10478.findAllByParentAuri(uri)
                def id10479_opts = Whova2016ConsentedNarratId10479.findAllByParentAuri(uri)

                WhoVa2016 whova = new WhoVa2016()
                Copy.fromTo(core1, whova)
                Copy.fromTo(core2, whova)
                Copy.fromTo(core3, whova)
                Copy.fromTo(core4, whova)
                Copy.fromTo(core5, whova)
                Copy.fromTo(core6, whova)
                Copy.fromTo(core7, whova)
                Copy.fromTo(core8, whova)

                id10173_opts.each { opt ->
                    final value = opt.value
                    switch (value){
                        case 'stridor'  : whova.id10173_1_St = value; break
                        case 'grunting' : whova.id10173_2_Gr = value; break
                        case 'wheezing' : whova.id10173_3_Wz = value; break
                        case 'no'       : whova.id10173_4_No = value; break
                        case 'dk'       : whova.id10173_5_Dk = value; break
                        case 'ref'      : whova.id10173_6_Rf = value; break
                    }
                }

                id10235_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'face'        : whova.id10235_1_Fc = value; break
                        case 'trunk'       : whova.id10235_2_Tk = value; break
                        case 'extremities' : whova.id10235_3_Et = value; break
                        case 'everywhere'  : whova.id10235_4_Ev = value; break
                    }
                }

                id10260_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'right_side'         : whova.id10260_1_Rs = value; break
                        case 'left_side'          : whova.id10260_2_Ls= value; break
                        case 'lower_part_of_body' : whova.id10260_3_Lp = value; break
                        case 'upper_part_of_body' : whova.id10260_4_Up = value; break
                        case 'one_leg_only'       : whova.id10260_5_Ol = value; break
                        case 'one_arm_only'       : whova.id10260_6_Oa = value; break
                        case 'whole_body'         : whova.id10260_7_Wb = value; break
                        case 'other'              : whova.id10260_8_Ot = value; break
                    }
                }

                id10433_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'traditional_healer'                   : whova.id10433_1_Th = value; break
                        case 'homeopath'                            : whova.id10433_2_Hp = value; break
                        case 'religious_leader'                     : whova.id10433_3_Rl = value; break
                        case 'government_hospital'                  : whova.id10433_4_Gh = value; break
                        case 'government_health_center_or_clinic'   : whova.id10433_5_Gc = value; break
                        case 'private_hospital'                     : whova.id10433_6_Ph = value; break
                        case 'community_based_practitionerinsystem' : whova.id10433_7_Cp = value; break
                        case 'trained_birth_attendant'              : whova.id10433_8_Ta = value; break
                        case 'private_physician'                    : whova.id10433_9_Pp = value; break
                        case 'relative_friend'                      : whova.id10433_10_Rf = value; break
                        case 'pharmacy'                             : whova.id10433_11_Ph = value; break
                        case 'dk'                                   : whova.id10433_12_Dk = value; break
                        case 'ref'                                  : whova.id10433_13_Rf = value; break
                    }
                }

                id10477_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'Chronic_kidney_disease' : whova.id10477_1_Ck = value; break
                        case 'Dialysis'               : whova.id10477_2_Dl = value; break
                        case 'Fever'                  : whova.id10477_3_Fv = value; break
                        case 'Heart_attack'           : whova.id10477_4_Ha = value; break
                        case 'Heart_problem'          : whova.id10477_5_Hp = value; break
                        case 'Jaundice'               : whova.id10477_6_Jd = value; break
                        case 'Liver_failure'          : whova.id10477_7_Lf = value; break
                        case 'Malaria'                : whova.id10477_8_Ml = value; break
                        case 'Pneumonia'              : whova.id10477_9_Pn = value; break
                        case 'Renal_kidney_failure'   : whova.id10477_10_Rk = value; break
                        case 'Suicide'                : whova.id10477_11_Sc = value; break
                        case 'None'                   : whova.id10477_12_No = value; break
                        case 'dk'                     : whova.id10477_13_Dk = value; break
                    }
                }

                id10478_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'abdomen'       : whova.id10478_1_Ab = value; break
                        case 'cancer'        : whova.id10478_2_Cc = value; break
                        case 'dehydration'   : whova.id10478_3_Dh = value; break
                        case 'dengue'        : whova.id10478_4_Dg = value; break
                        case 'diarrhea'      : whova.id10478_5_Dr = value; break
                        case 'fever'         : whova.id10478_6_Fv = value; break
                        case 'heart_problem' : whova.id10478_7_Hp = value; break
                        case 'jaundice'      : whova.id10478_8_Jd = value; break
                        case 'pneumonia'     : whova.id10478_9_Pn = value; break
                        case 'rash'          : whova.id10478_10_Rs = value; break
                        case 'None'          : whova.id10478_11_No = value; break
                        case 'dk'            : whova.id10478_12_Dk = value; break
                    }
                }

                id10479_opts.each { opt ->
                    final value = opt.value
                    switch (value) {
                        case 'asphyxia'             : whova.id10479_1_Ax = value; break
                        case 'incubator'            : whova.id10479_2_Ib = value; break
                        case 'lung_problem'         : whova.id10479_3_Lp = value; break
                        case 'pneumonia'            : whova.id10479_4_Pn = value; break
                        case 'preterm_delivery'     : whova.id10479_5_Pd = value; break
                        case 'respiratory_distress' : whova.id10479_6_Rd = value; break
                        case 'None'                 : whova.id10479_7_No = value; break
                        case 'dk'                   : whova.id10479_8_Dk = value; break
                    }
                }

                if (whova.save(flush: true)){
                    core1.processed = 1
                }else{
                    core1.processed = 2 /* error occurred */
                    errors++

                    def msg = "Couldnt save WhoVa2016 from ODK Core with uri=${whova.sysOdkUri}"
                    def msgErr = "Errors: ${whova.errors}"

                    output.println(msg)
                    output.println(msgErr)
                    println(msg)
                    println(msgErr)
                }

                core1.save(flush: true)
            }
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(reportId)
            LogReportFile reportFile = new LogReportFile(creationDate: new Date(), fileName: log.logFileName, logReport: logReport)
            reportFile.creationDate = new Date()
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()
    }
}

