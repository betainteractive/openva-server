package net.openva.server.services

//import grails.gorm.transactions.Transactional
import groovy.time.TimeCategory
import net.betainteractive.io.LogOutput
import net.betainteractive.utilities.GeneralUtil
import net.betainteractive.utilities.StringUtil
import net.openva.io.SystemPath
import net.openva.security.authentication.User
import net.openva.server.model.Individual
import net.openva.server.model.VerbalAutopsyControl
import net.openva.server.model.UncompletedVA
import net.openva.server.model.VerbalAutopsyStats
import net.openva.server.model.WhoVa2016
import net.openva.server.model.logs.LogReport
import net.openva.server.model.logs.LogReportFile
import net.openva.server.model.logs.LogStatus


class VerbalAutopsyControlService {

    def generalUtilitiesService
    def sessionFactory

    def updateVaControl(long logReportId) {
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "update-va-control");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0

        def whoUris = []


        def start = new Date()

        //read all odk who_va
        WhoVa2016.withTransaction {
            whoUris = WhoVa2016.executeQuery("select w.sysOdkUri from WhoVa2016 w where w.processed=0")
        }


        println "uris to process: ${whoUris.size()}"

        int from = 0;
        int to = 0;
        int max = 50
        while (processed < whoUris.size()) {
            from = to
            to = (whoUris.size() > to + max) ? (to + max) : whoUris.size();
            //reading only some records to speed up the process

            VerbalAutopsyControl.withTransaction {
                whoUris.subList(from, to).each { uri ->

                    processed++

                    def WhoVa2016 whova = null
                    def VerbalAutopsyControl vaControl = null
                    def User fieldWorker = null
                    def Individual individual = null


                    whova = WhoVa2016.findBySysOdkUri(uri) //read the collected autopsy
                    vaControl = VerbalAutopsyControl.findByCode(whova.individualId)
                    //find autopsy control by the code
                    fieldWorker = User.findByUsername(whova.fieldWorkerId)
                    //get the fieldworker/user that collected the VA
                    individual = net.openva.server.model.Individual.findByCode(whova.individualId)

                    def collectionAttempts = new HashSet<UncompletedVA>()
                    def isNewVaControl = false

                    //println "id=${whova.individualId}"

                    def isRegistered = individual != null
                    //(whova.isPreRegistered != null && whova.isPreRegistered.equals("yes"))

                    if (vaControl == null && isRegistered == false && whova.isVisitPossible == "no") {
                        //This case cannot be saved to the system because the individual doesnt exist on the database and the fieldworkers were unable to visit the house

                        if (whova != null) {
                            whova.processed = 2
                            //whova.save(flush: true)
                            WhoVa2016.executeUpdate("update WhoVa2016 w set w.processed=? where w.sysOdkUri=?", [whova.processed, whova.sysOdkUri])

                            errors++

                            def msg = "Couldnt save/update VaControl with WhoVa2016 data using uri = ${whova.sysOdkUri}"
                            def msgErr = "Errors: This is an unregistered individual that wasnt visited at all"

                            output.println(msg)
                            output.println(msgErr)
                            println(msg)
                            println(msgErr)

                        }


                        return
                    }

                    if (vaControl == null && isRegistered == false) {
                        //for individuals that are note registered on the system
                        isNewVaControl = true

                        vaControl = new VerbalAutopsyControl()

                        vaControl.extId = whova.individualId
                        vaControl.code = whova.individualId
                        vaControl.name = StringUtil.getFullname(whova.id10017, "", whova.id10018)
                        vaControl.gender = getGenderFormat(whova.id10019)
                        vaControl.dateOfBirth = whova.id10021
                        vaControl.motherId = ""
                        vaControl.motherName = whova.id10062
                        vaControl.fatherId = ""
                        vaControl.fatherName = whova.id10061

                        vaControl.dateOfDeath = whova.id10023

                        if (vaControl.dateOfDeath != null && vaControl.dateOfBirth != null) {
                            if (vaControl.dateOfBirth <= vaControl.dateOfDeath) {
                                vaControl.ageAtDeath = GeneralUtil.getAge(vaControl.dateOfBirth, vaControl.dateOfDeath)
                            } else {
                                def msg = "WARNING: Date of birth is greater than the Date of Death"
                                output.println(msg)
                                println(msg)
                            }
                        }

                        vaControl.householdId = whova.householdId
                        vaControl.householdNo = whova.householdId
                        vaControl.gpsAccuracy = ""
                        vaControl.gpsAltitude = ""
                        vaControl.gpsLatitude = ""
                        vaControl.gpsLongitude = ""

                        vaControl.vaType = getVaType(whova)
                        //vaControl.vaUuid = whova.sysOdkUri
                        //vaControl.vaCollected = 1
                        //vaControl.vaProcessedWith = ""
                        //vaControl.vaProcessed = 0
                        //vaControl.vaResult = ""
                        vaControl.vaAttempts = 0

                    }

                    if (vaControl == null && isRegistered) { //is unregistered death but has individual on system

                        isNewVaControl = true

                        vaControl = new VerbalAutopsyControl()

                        vaControl.extId = individual.extId
                        vaControl.code = individual.code
                        vaControl.name = individual.name
                        vaControl.gender = individual.gender
                        vaControl.dateOfBirth = individual.dateOfBirth
                        vaControl.motherId = individual.motherId
                        vaControl.motherName = (individual.motherName == null || individual.motherName.isEmpty()) ? whova.id10062 : individual.motherName
                        vaControl.fatherId = individual.fatherId
                        vaControl.fatherName = (individual.fatherName == null || individual.fatherName.isEmpty()) ? whova.id10061 : individual.fatherName

                        vaControl.dateOfDeath = whova.id10023

                        if (vaControl.dateOfDeath != null && vaControl.dateOfBirth != null) {
                            if (vaControl.dateOfBirth <= vaControl.dateOfDeath) {
                                vaControl.ageAtDeath = GeneralUtil.getAge(vaControl.dateOfBirth, vaControl.dateOfDeath)
                            } else {
                                def msg = "WARNING: Date of birth is greater than the Date of Death"
                                output.println(msg)
                                println(msg)
                            }
                        }

                        vaControl.householdId = individual.householdId
                        vaControl.householdNo = individual.householdId
                        vaControl.gpsAccuracy = individual.gpsAccuracy
                        vaControl.gpsAltitude = individual.gpsAltitude
                        vaControl.gpsLatitude = individual.gpsLatitude
                        vaControl.gpsLongitude = individual.gpsLongitude

                        vaControl.vaType = getVaType(whova)
                        //vaControl.vaUuid = whova.sysOdkUri
                        //vaControl.vaCollected = 1
                        //vaControl.vaProcessedWith = ""
                        //vaControl.vaProcessed = 0
                        //vaControl.vaResult = ""
                        vaControl.vaAttempts = 0

                    }

                    if (vaControl != null) {
                        vaControl.name = vaControl.name == null ? "" : vaControl.name
                    }

                    //Update the VA
                    vaControl.vaUuid = whova.sysOdkUri
                    vaControl.vaCollected = 1


                    println "trying to save: ${vaControl.code}, ${vaControl.name}, ${vaControl.gender}, ${vaControl.vaCollected}, ${isNewVaControl}"

                    if (vaControl.save(flush:true)) {


                        //check Retries
                        UncompletedVA vaAttempt = null

                        if (whova.isVisitPossible == "no") {
                            vaControl.vaCollected = 2 //collected but is a rejection or impossiblity to proceed

                            vaAttempt = new UncompletedVA()
                            vaAttempt.autopsy = whova
                            vaAttempt.reason = whova.reasonOther == null ? whova.reasonToNotCollect : whova.reasonOther
                        }

                        if (whova.id10013 != null && whova.id10013 == "no") { //did not consented
                            vaControl.vaCollected = 2 //collected but is a rejection or impossiblity to proceed

                            vaAttempt = new UncompletedVA()
                            vaAttempt.autopsy = whova
                            vaAttempt.reason = "not_consented"
                        }

                        if (vaAttempt != null) {
                            vaAttempt.fieldWorker = fieldWorker

                            vaAttempt.save()
                            //println "vaAttempt errors: ${vaAttempt.errors}"

                            vaControl.vaAttempts = vaControl.vaAttempts + 1
                            //vaControl.addToCollectionAttempts(vaAttempt)
                            vaControl.save()
                            //println "vaControl-attempt errors: ${vaControl.errors}"
                        }

                        whova.processed = 1
                    } else {
                        whova.processed = 2
                        errors++

                        def msg = "Couldnt save VaControl with code = ${vaControl.code}"
                        def msgErr = "Errors: ${vaControl.errors}"

                        output.println(msg)
                        output.println(msgErr)
                        println(msg)
                        println(msgErr)
                    }

                    if (whova != null) {
                        WhoVa2016.executeUpdate("update WhoVa2016 w set w.processed=? where w.sysOdkUri=?", [whova.processed, whova.sysOdkUri])
                        //whova.save(flush: true)
                    }
                }

            }

            println("${max} reached clearing session - ${TimeCategory.minus(new Date(), start)}")
            //sessionFactory.currentSession.flush() //clearing cache save us a lot of time
            //sessionFactory.currentSession.clear()
        }

        println "finished update va !!!"

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: logReport.start, fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()
        }

        output.close()
    }

    def calculateVaStats(long logReportId) {
        LogOutput log = generalUtilitiesService.getOutput(SystemPath.getAbsoluteLogPath(), "va-stats");
        PrintStream output = log.output

        if (output == null) return;

        int processed = 0
        int errors = 0

        //Calculate Stats
        VerbalAutopsyStats.withNewTransaction {

            def stats = new VerbalAutopsyStats()

            stats.logReport = LogReport.get(logReportId)
            stats.createdBy = generalUtilitiesService.currentUser()
            stats.creationDate = new Date()

            stats.totalDeaths = VerbalAutopsyControl.count()
            stats.totalToCollect = VerbalAutopsyControl.countByVaCollectedAndVaCollected(0, 2)
            stats.totalCollected = VerbalAutopsyControl.countByVaCollected(1)
            stats.totalAttempts = UncompletedVA.count()
            stats.totalAttemptNi = UncompletedVA.countByReason("no_informant")
            stats.totalAttemptUh = UncompletedVA.countByReason("uninhabited_house")
            stats.totalAttemptWdr = UncompletedVA.countByReason("wrong_death_record")
            stats.totalAttemptOt = UncompletedVA.countByReason("other")
            stats.totalAttemptNc = UncompletedVA.countByReason("not_consented")


            User.list().each { user ->

                def resCollected = VerbalAutopsyControl.executeQuery("select count(who.id) from VerbalAutopsyControl v, WhoVa2016 who where v.vaCollected=1 and v.vaUuid=who.sysOdkUri and who.fieldWorkerId=?", [user.username])

                user.totalCollected = resCollected[0]
                user.totalAttempts = UncompletedVA.countByFieldWorker(user);

                user.save()
            }

            processed = 1

            if (!stats.save(flush: true)) {
                errors = 1

                def msg = "Couldnt save VaStats by user=${stats.createdBy}, ${stats.creationDate}"
                def msgErr = "Errors: ${stats.errors}"

                output.println(msg)
                output.println(msgErr)
                println(msg)
                println(msgErr)
            }else{
                println("stats calculated and saved!!!")
            }
        }

        LogReport.withTransaction {
            LogReport logReport = LogReport.get(logReportId)
            LogReportFile reportFile = new LogReportFile(creationDate: logReport.start, fileName: log.logFileName, logReport: logReport)
            reportFile.processedCount = processed
            reportFile.errorsCount = errors
            logReport.end = new Date()
            logReport.status = LogStatus.findByName(LogStatus.FINISHED)
            logReport.addToLogFiles(reportFile)
            logReport.save()

            //println(logReport.errors)
        }

        output.close()
    }

    int getVaType(WhoVa2016 whova) {
        if (whova.isNeonatal != null && whova.isNeonatal.equals("1")) return 1
        if (whova.isChild != null && whova.isChild.equals("1")) return 2
        if (whova.isAdult != null && whova.isAdult.equals("1")) return 3
        return 0
    }

    def getGenderFormat(String genderText){
        if (genderText==null || genderText.isEmpty()) return ""
        def g = ""+genderText.charAt(0)+""
        g = g.toUpperCase();
        return g
    }

}
