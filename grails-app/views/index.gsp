<!doctype html>
<html>
<head>
	<meta name="layout" content="main"/>
	<title><g:message code="default.application.name"/></title>

	<style type="text/css" media="screen">

		#page-body {
			margin: 2em 2em 1.25em 2em;
		}

		h1 {
			color: #48802c;
			font-weight: normal;
			font-size: 18px;
			margin: 0.8em 0 0.3em 0;
		}

		h2 {
			color: #48802c;
			margin-top: 1em;
			margin-bottom: 0.3em;
			font-size: 1em;
		}

		p {
			line-height: 1.5;
			margin: 0.25em 0;
		}

		#controller-list ul {
			list-style-position: inside;
		}

		#controller-list li {
			line-height: 1.3;
			list-style-position: inside;
			margin: 0.25em 0;
		}

		@media screen and (max-width: 480px) {

			#page-body {
				margin: 0 1em 1em;
			}

			#page-body h1 {
				margin-top: 0;
			}
		}

		#page-body h1 {
			margin-top: 0;
		}

	</style>
</head>
<body>


<div id="page-body">
	<h1><g:message code="default.main.welcome.title" /></h1>
	<br>
	<p><g:message code="default.main.welcome.msg" /></p>

	<br>
	<div id="controller-list" role="navigation">
		<h2><g:message code="default.main.welcome.links" /></h2>
		<ul>
			<li class="controller"><g:link url="http://www.who.int/healthinfo/statistics/verbalautopsystandards/en/">The 2016 WHO verbal autopsy instrument</g:link></li>
			<li class="controller"><g:link url="https://github.com/OpenVA/">OpenVA Github Repository</g:link></li>
		</ul>
	</div>
	<br>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<p><g:message code="default.main.welcome.developer" /></p>
	<g:message code="default.main.welcome.developer_email" />
	<br>
	<br>
</div>
</body>
</html>