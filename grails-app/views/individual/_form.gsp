<%@ page import="net.openva.server.model.Individual" %>


<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'code', 'error')} required">
	<label for="code">
		<g:message code="individual.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="code" required="" value="${individualInstance?.code}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="individual.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${individualInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'gender', 'error')} required">
	<label for="gender">
		<g:message code="individual.gender.label" default="Gender" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="gender" required="" from="${['M','F']}" value="${individualInstance?.gender}" valueMessagePrefix="default.gender"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'dateOfBirth', 'error')} required">
	<label for="dateOfBirth">
		<g:message code="individual.dateOfBirth.label" default="Date Of Birth" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfBirth" precision="day"  value="${individualInstance?.dateOfBirth}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'motherId', 'error')} ">
	<label for="motherId">
		<g:message code="individual.motherId.label" default="Mother Id" />
		
	</label>
	<g:textField name="motherId" value="${individualInstance?.motherId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'motherName', 'error')} ">
	<label for="motherName">
		<g:message code="individual.motherName.label" default="Mother Name" />
		
	</label>
	<g:textField name="motherName" value="${individualInstance?.motherName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'fatherId', 'error')} ">
	<label for="fatherId">
		<g:message code="individual.fatherId.label" default="Father Id" />
		
	</label>
	<g:textField name="fatherId" value="${individualInstance?.fatherId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'fatherName', 'error')} ">
	<label for="fatherName">
		<g:message code="individual.fatherName.label" default="Father Name" />
		
	</label>
	<g:textField name="fatherName" value="${individualInstance?.fatherName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'householdId', 'error')} ">
	<label for="householdId">
		<g:message code="individual.householdId.label" default="Household Id" />
		
	</label>
	<g:textField name="householdId" value="${individualInstance?.householdId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: individualInstance, field: 'householdNo', 'error')} ">
	<label for="householdNo">
		<g:message code="individual.householdNo.label" default="Household No" />
		
	</label>
	<g:textField name="householdNo" value="${individualInstance?.householdNo}"/>

</div>