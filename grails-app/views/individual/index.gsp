
<%@ page import="net.openva.server.model.Individual" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'individual.label', default: 'Individual')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#list-individual" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="individual.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" controller="updates" action="addDeaths"><g:message code="default.menu.add.death.label" /></g:link></li>
			</ul>
		</div>
		<div id="list-individual" class="content scaffold-list" role="main">
			<h1><g:message code="individual.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="extId" title="${message(code: 'individual.extId.label', default: 'Ext Id')}" />
					
						<g:sortableColumn property="code" title="${message(code: 'individual.code.label', default: 'Code')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'individual.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="gender" title="${message(code: 'individual.gender.label', default: 'Gender')}" />
					
						<g:sortableColumn property="dateOfBirth" title="${message(code: 'individual.dateOfBirth.label', default: 'Date Of Birth')}" />
					
						<g:sortableColumn property="age" title="${message(code: 'individual.age.label', default: 'Age')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${individualInstanceList}" status="i" var="individualInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${individualInstance.id}">${fieldValue(bean: individualInstance, field: "extId")}</g:link></td>
					
						<td>${fieldValue(bean: individualInstance, field: "code")}</td>
					
						<td>${fieldValue(bean: individualInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: individualInstance, field: "gender")}</td>
					
						<td><g:formatDate date="${individualInstance.dateOfBirth}" format="yyyy-MM-dd" /></td>
					
						<td>${fieldValue(bean: individualInstance, field: "age")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${individualInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
