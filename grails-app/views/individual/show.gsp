
<%@ page import="net.openva.server.model.Individual" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'individual.label', default: 'Individual')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#show-individual" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="individual.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="individual.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-individual" class="content scaffold-show" role="main">
			<h1><g:message code="individual.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list individual">
			
				<g:if test="${individualInstance?.extId}">
				<li class="fieldcontain">
					<span id="extId-label" class="property-label"><g:message code="individual.extId.label" default="Ext Id" /></span>
					
						<span class="property-value" aria-labelledby="extId-label"><g:fieldValue bean="${individualInstance}" field="extId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.code}">
				<li class="fieldcontain">
					<span id="code-label" class="property-label"><g:message code="individual.code.label" default="Code" /></span>
					
						<span class="property-value" aria-labelledby="code-label"><g:fieldValue bean="${individualInstance}" field="code"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="individual.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${individualInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.gender}">
				<li class="fieldcontain">
					<span id="gender-label" class="property-label"><g:message code="individual.gender.label" default="Gender" /></span>
					
						<span class="property-value" aria-labelledby="gender-label"><g:fieldValue bean="${individualInstance}" field="gender"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.dateOfBirth}">
				<li class="fieldcontain">
					<span id="dateOfBirth-label" class="property-label"><g:message code="individual.dateOfBirth.label" default="Date Of Birth" /></span>
					
						<span class="property-value" aria-labelledby="dateOfBirth-label"><g:formatDate date="${individualInstance?.dateOfBirth}" format="yyyy-MM-dd" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.age}">
				<li class="fieldcontain">
					<span id="age-label" class="property-label"><g:message code="individual.age.label" default="Age" /></span>
					
						<span class="property-value" aria-labelledby="age-label"><g:fieldValue bean="${individualInstance}" field="age"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.motherId}">
				<li class="fieldcontain">
					<span id="motherId-label" class="property-label"><g:message code="individual.motherId.label" default="Mother Id" /></span>
					
						<span class="property-value" aria-labelledby="motherId-label"><g:fieldValue bean="${individualInstance}" field="motherId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.motherName}">
				<li class="fieldcontain">
					<span id="motherName-label" class="property-label"><g:message code="individual.motherName.label" default="Mother Name" /></span>
					
						<span class="property-value" aria-labelledby="motherName-label"><g:fieldValue bean="${individualInstance}" field="motherName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.fatherId}">
				<li class="fieldcontain">
					<span id="fatherId-label" class="property-label"><g:message code="individual.fatherId.label" default="Father Id" /></span>
					
						<span class="property-value" aria-labelledby="fatherId-label"><g:fieldValue bean="${individualInstance}" field="fatherId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.fatherName}">
				<li class="fieldcontain">
					<span id="fatherName-label" class="property-label"><g:message code="individual.fatherName.label" default="Father Name" /></span>
					
						<span class="property-value" aria-labelledby="fatherName-label"><g:fieldValue bean="${individualInstance}" field="fatherName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.householdId}">
				<li class="fieldcontain">
					<span id="householdId-label" class="property-label"><g:message code="individual.householdId.label" default="Household Id" /></span>
					
						<span class="property-value" aria-labelledby="householdId-label"><g:fieldValue bean="${individualInstance}" field="householdId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.householdNo}">
				<li class="fieldcontain">
					<span id="householdNo-label" class="property-label"><g:message code="individual.householdNo.label" default="Household No" /></span>
					
						<span class="property-value" aria-labelledby="householdNo-label"><g:fieldValue bean="${individualInstance}" field="householdNo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.gpsAccuracy}">
				<li class="fieldcontain">
					<span id="gpsAccuracy-label" class="property-label"><g:message code="individual.gpsAccuracy.label" default="Gps Accuracy" /></span>
					
						<span class="property-value" aria-labelledby="gpsAccuracy-label"><g:fieldValue bean="${individualInstance}" field="gpsAccuracy"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.gpsAltitude}">
				<li class="fieldcontain">
					<span id="gpsAltitude-label" class="property-label"><g:message code="individual.gpsAltitude.label" default="Gps Altitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsAltitude-label"><g:fieldValue bean="${individualInstance}" field="gpsAltitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.gpsLatitude}">
				<li class="fieldcontain">
					<span id="gpsLatitude-label" class="property-label"><g:message code="individual.gpsLatitude.label" default="Gps Latitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsLatitude-label"><g:fieldValue bean="${individualInstance}" field="gpsLatitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${individualInstance?.gpsLongitude}">
				<li class="fieldcontain">
					<span id="gpsLongitude-label" class="property-label"><g:message code="individual.gpsLongitude.label" default="Gps Longitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsLongitude-label"><g:fieldValue bean="${individualInstance}" field="gpsLongitude"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:individualInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${individualInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
