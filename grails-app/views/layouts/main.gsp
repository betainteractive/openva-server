<!DOCTYPE html>
<html class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:message code="default.application.name"/></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<asset:javascript src="application.js"/>
	<asset:stylesheet src="application.css"/>
	<asset:stylesheet src="lang_selector.css"/>

	<style type="text/css" media="screen">

	.status_main {
		display: table;
		background-color: #ffffff;
		border: .2em solid #fff;
		margin: 0em 1em 1em 1em;
		margin-right: auto;
		margin-left: auto;
		padding: 1em;
		width: 97%;
		min-height: 80%;
		height: 80vh;
		-moz-box-shadow: 0px 0px 1.25em #ccc;
		-webkit-box-shadow: 0px 0px 1.25em #ccc;
		box-shadow: 0px 0px 1.25em #ccc;
		-moz-border-radius: 0.6em;
		-webkit-border-radius: 0.6em;
		border-radius: 0.6em;
		font-size: 0.85em;
	}

	.ie6 .status_main {
		display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
	}

	.status_main ul {
		font-size: 0.9em;
		list-style-type: none;
		margin-bottom: 0.6em;
		padding: 0;
	}

	.status_main li {
		line-height: 1.3;
	}

	.status_main h1 {
		text-transform: uppercase;
		font-size: 1.1em;
		margin: 0 0 0.3em;
	}

	#headerLeft{
		float: left;
		height: 50px;
		margin-left: -40px;
		margin-right: 20px;
	}

	/************** siteInfo styles ****************/
	/* Footer
    -----------------------------------------------------------------------------*/
	#footer {
		position: relative;
		margin: -14px auto 0;
		width: auto;
		height: 14px;
	}

	#footerMenu {
		/*background-color: #fff;*/
		/*border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;  */
		width: 98%;
		margin-left: -1px;
	}

	.siteInfo{
		vertical-align: bottom;
		clear: both;
		font-size: 11px;
		/*font-weight: bold;*/
		color: #999;
		padding: 10px 10px 10px 10px;
	}

	#siteInfo a:link, #siteInfo a:visited{
		color: #900;
		text-decoration: none;
		font-weight: normal;
	}

	#siteInfo a:hover{
		text-decoration: underline;
		font-weight: normal;
	}

	#siteInfo img{
		padding: 4px 4px 4px 0px;
		vertical-align: middle;
	}

	#displayUser {
		margin-top: 4.5em;
		margin-left: 2em;
		margin-right: 1em;
		margin-bottom: .7em;
		width: 96%;
		font-size: 12px;
	}

	#duMain {
		width: 45%;
	}

	#duMsg {
		float: right;
		width: 45%;
		text-align: right;
	}

	</style>

	<script type="text/javascript">
		/* When the user clicks on the button, toggle between hiding and showing the dropdown content */
		function showDropdown() {
    		document.getElementById("langDropdown").classList.toggle("show");
		}

		// Close the dropdown menu if the user clicks outside of it
		window.onclick = function(event) {
  			if (!event.target.matches('.lang-dropdown-toggle')) {

    			var dropdowns = document.getElementsByClassName("lang-dropdown-content");
    			var i;
    			for (i = 0; i < dropdowns.length; i++) {
      				var openDropdown = dropdowns[i];
      				if (openDropdown.classList.contains('show')) {
        				openDropdown.classList.remove('show');
      				}
    			}
  			}
		}
	</script>

	<g:layoutHead/>
</head>

<body>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">

		<div class="navbar-header">
			<asset:image id="headerLeft" src="openva_logo.png" />
			<!-- <a class="navbar-brand" href="#">OpenVA</a> -->
		</div>

		<bi:horizontalMenuBar>
			<bi:menu link="${createLinkTo(dir: '')}" label="${message(code: 'default.menu.home.label')}" style="active" />
			<bi:menu link="${createLink(controller: 'user', action: 'index')}" label="${message(code: 'default.menu.users.label')}"/>
			<bi:menu link="${createLink(controller: 'updates', action: 'index')}" label="${message(code: 'default.menu.add.label')}" >
				<!--
				<bi:menu link="${createLink(controller: 'user', action: 'index')}" label="${message(code: 'default.menu.add.death.label')}"/>
				<bi:menu link="${createLink(controller: 'user', action: 'index')}" label="${message(code: 'default.menu.add.individual.label')}"/>
				-->
			</bi:menu>
			<bi:menu link="${createLink(controller: 'importData', action: 'index')}" label="${message(code: 'default.menu.import.label')}" />
			<bi:menu link="${createLink(controller: 'exportFiles', action: 'index')}" label="${message(code: 'default.menu.sync.label')}" />
			<bi:submenu label="${message(code: 'default.menu.tools.label')}" >
				<bi:menu link="${createLink(controller: 'settings', action: 'index')}" label="${message(code: 'default.menu.settings.label')}" />
				<bi:menu link="${createLink(controller: 'settings', action: 'index')}" label="${message(code: 'default.menu.vatools.label')}" />
			</bi:submenu>
			<bi:menu link="${createLink(controller: 'reports', action: 'index')}" label="${message(code: 'default.menu.reports.label')}" />

		</bi:horizontalMenuBar>

	</div>
</nav>

<!-- logout part -->
<div id="displayUser">
	<div id="duMsg">
		<!--
		<sec:ifLoggedIn>
			<asset:image src="msg.png"/>
			<g:link controller="message" action="list"><b><bi:messageStatus/></b></g:link>
		</sec:ifLoggedIn>
		-->

		<div class="lang-dropdown-menu">
		    <a onclick="showDropdown()" href="#" class="lang-dropdown-toggle" data-toggle="dropdown" ><g:message code="default.language.select"/><span class="caret"></span></a>
			<div id="langDropdown" class="lang-dropdown-content">
				<language:selector />
			</div>
		</div>

	</div>
	<div id="duMain">
		<sec:ifLoggedIn>
			<g:message code="login.as.label"/>: <b><bi:showLoggedUser/></b>
			(<g:link controller="logout"><g:message code="login.logout.label"/></g:link>)
		</sec:ifLoggedIn>
		<sec:ifNotLoggedIn>
			<g:link controller="login" action="auth"><g:message code="login.button.label"/></g:link>
		</sec:ifNotLoggedIn>
	</div>
</div>


<div id="middle" class="status_main">
	<g:layoutBody/>
</div>


<section id="footer">
	<div id="footerMenu">
		<div id="siteInfo" class="siteInfo" align="right">
			<a href="#">Paulo Filimone </a> | &copy;2017 OpenVA
		</div>

	</div>
</section>
</body>
</html>