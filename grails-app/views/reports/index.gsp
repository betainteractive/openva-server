<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="default.menu.stats.label" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>

		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" controller="updates" action="addDeaths"><g:message code="default.menu.add.death.label" /></g:link></li>
				<li><g:link class="create" controller="updates" action="addIndividuals"><g:message code="default.menu.add.individual.label" /></g:link></li>
			</ul>
		</div>
		<div id="list-user" class="content scaffold-list" role="main">
			<h1><g:message code="default.menu.stats.label" /></h1>
			<br>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<th><g:message code="logreport.description.label" default="Process Description" /></th>
						<th><g:message code="logreport.start.label" default="Last Synch Start" /></th>
						<th><g:message code="logreport.end.label" default="Last Synch End" /></th>
						<th><g:message code="logreport.status.label" default="Current Status" /></th>
						<th><g:message code="" default="" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${logReports}" status="i" var="logReport">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link controller="logReport" action="show" id="${logReport.id}"><g:message code="${logReport.description}" default="${logReport.description}" /></g:link></td>

						<td><g:formatDate date="${logReport.start}" format="yyyy-MM-dd HH:mm" /></td>

						<td><g:formatDate date="${logReport.end}" format="yyyy-MM-dd HH:mm" /></td>

						<td><g:message code="${logReport.status}" default="${logReport.status}" /></td>

						<td>
							<g:link class="edit" controller="reports" action="executeUpdateStats" id="${logReport.id}">
								<g:message code="default.execute.label" default="Execute" />
							</g:link>
						</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>

			<div>
				<ol class="property-list stats">
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalDeaths.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalDeaths}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalCollected.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalCollected}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalToCollect.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalToCollect}</span>
					</li>
				</ol>
				<ol class="property-list stats">

					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttempts.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttempts}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttemptNi.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttemptNi}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttemptUh.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttemptUh}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttemptWdr.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttemptWdr}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttemptOt.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttemptOt}</span>
					</li>
					<li class="fieldcontain">
						<span id="reportId-label" class="property-label"><g:message code="stats.totalAttemptNc.label" />:</span>
						<span class="property-value" aria-labelledby="reportId-label">${stats.totalAttemptNc}</span>
					</li>

				</ol>
			</div>

			<fieldset class="buttons">
				<g:actionSubmit name="create" class="save" action="exportStats" value="${message(code: "stats.export.label")}" />
			</fieldset>
		</div>
	</body>
</html>
