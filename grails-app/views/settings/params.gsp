<%@ page import="net.openva.server.settings.ApplicationParam" %>
<%@ page import="net.betainteractive.utilities.StringUtil" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'applicationParam.label', default: 'ApplicationParam')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#edit-applicationParam" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="import" controller="importData" action="index"><g:message code="default.menu.add.import.label" args="[entityName]" /></g:link></li>
				<li><g:link class="export" controller="exportFiles" action="index"><g:message code="default.menu.sync.export.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="edit-applicationParam" class="content scaffold-edit" role="main">
			<h1><g:message code="settings.params.title" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>


			<g:form controller="settings" action="update" method="PUT" >

				<g:each in="${paramInstances}" var="applicationParamInstance" status="i" >

					<g:hiddenField name="id" value="${applicationParamInstance?.id}" />

					<fieldset class="form">

						<div class="fieldcontain ${hasErrors(bean: applicationParamInstance, field: 'name', 'error')} required">
							<label for="name">
								<g:message code="${applicationParamInstance.name}" /><span class="required-indicator">*</span>
							</label>

							<g:if test="${applicationParamInstance.type.equals("string")}">
								<g:textField name="value${i}" required="" value="${applicationParamInstance?.value}"/>
							</g:if>
							<g:if test="${applicationParamInstance.type.equals("date")}">
								<g:datePicker name="value${i}" precision="day"  value="${StringUtil.toDate(applicationParamInstance?.value,"yyyy-MM-dd")}" default="none" noSelection="['': '']" />
							</g:if>
						</div>

					</fieldset>
				</g:each>

				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>

			</g:form>
		</div>
	</body>
</html>
