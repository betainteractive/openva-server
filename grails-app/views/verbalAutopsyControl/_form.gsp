<%@ page import="net.openva.server.model.VerbalAutopsyControl" %>


<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'code', 'error')} required">
	<label for="code">
		<g:message code="verbalAutopsyControl.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="code" required="" value="${verbalAutopsyControlInstance?.code}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="verbalAutopsyControl.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${verbalAutopsyControlInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'gender', 'error')} required">
	<label for="gender">
		<g:message code="verbalAutopsyControl.gender.label" default="Gender" />
		<span class="required-indicator">*</span>
	</label>
    <g:select name="gender" required="" from="${['M','F']}" value="${verbalAutopsyControlInstance?.gender}" valueMessagePrefix="default.gender"/>
</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'dateOfBirth', 'error')} ">
	<label for="dateOfBirth">
		<g:message code="verbalAutopsyControl.dateOfBirth.label" default="Date Of Birth" />
		
	</label>
	<g:datePicker name="dateOfBirth" precision="day"  value="${verbalAutopsyControlInstance?.dateOfBirth}" default="none" noSelection="['': '']" />

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'dateOfDeath', 'error')} ">
	<label for="dateOfDeath">
		<g:message code="verbalAutopsyControl.dateOfDeath.label" default="Date Of Death" />
		
	</label>
	<g:datePicker name="dateOfDeath" precision="day"  value="${verbalAutopsyControlInstance?.dateOfDeath}" default="none" noSelection="['': '']" />

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'motherId', 'error')} ">
	<label for="motherId">
		<g:message code="verbalAutopsyControl.motherId.label" default="Mother Id" />
		
	</label>
	<g:textField name="motherId" value="${verbalAutopsyControlInstance?.motherId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'motherName', 'error')} ">
	<label for="motherName">
		<g:message code="verbalAutopsyControl.motherName.label" default="Mother Name" />
		
	</label>
	<g:textField name="motherName" value="${verbalAutopsyControlInstance?.motherName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'fatherId', 'error')} ">
	<label for="fatherId">
		<g:message code="verbalAutopsyControl.fatherId.label" default="Father Id" />
		
	</label>
	<g:textField name="fatherId" value="${verbalAutopsyControlInstance?.fatherId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'fatherName', 'error')} ">
	<label for="fatherName">
		<g:message code="verbalAutopsyControl.fatherName.label" default="Father Name" />
		
	</label>
	<g:textField name="fatherName" value="${verbalAutopsyControlInstance?.fatherName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'householdId', 'error')} ">
	<label for="householdId">
		<g:message code="verbalAutopsyControl.householdId.label" default="Household Id" />
		
	</label>
	<g:textField name="householdId" value="${verbalAutopsyControlInstance?.householdId}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: verbalAutopsyControlInstance, field: 'householdNo', 'error')} ">
	<label for="householdNo">
		<g:message code="verbalAutopsyControl.householdNo.label" default="Household Code" />
		
	</label>
	<g:textField name="householdNo" value="${verbalAutopsyControlInstance?.householdNo}"/>

</div>