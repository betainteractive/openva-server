<%@ page import="net.openva.server.model.VerbalAutopsyControl" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'verbalAutopsyControl.label', default: 'VerbalAutopsyControl')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#edit-verbalAutopsyControl" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="verbalAutopsyControl.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="verbalAutopsyControl.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="edit-verbalAutopsyControl" class="content scaffold-edit" role="main">
			<h1><g:message code="verbalAutopsyControl.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${verbalAutopsyControlInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${verbalAutopsyControlInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:verbalAutopsyControlInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${verbalAutopsyControlInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
