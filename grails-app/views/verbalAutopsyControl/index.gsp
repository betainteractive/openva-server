
<%@ page import="net.openva.server.model.VerbalAutopsyControl" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'verbalAutopsyControl.label', default: 'VerbalAutopsyControl')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#list-verbalAutopsyControl" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="verbalAutopsyControl.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" controller="updates" action="addIndividuals"><g:message code="default.menu.add.individual.label" /></g:link></li>
			</ul>
		</div>
		<div id="list-verbalAutopsyControl" class="content scaffold-list" role="main">
			<h1><g:message code="verbalAutopsyControl.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="extId" title="${message(code: 'verbalAutopsyControl.extId.label', default: 'Ext Id')}" />
					
						<g:sortableColumn property="code" title="${message(code: 'verbalAutopsyControl.code.label', default: 'Code')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'verbalAutopsyControl.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="gender" title="${message(code: 'verbalAutopsyControl.gender.label', default: 'Gender')}" />
					
						<g:sortableColumn property="dateOfBirth" title="${message(code: 'verbalAutopsyControl.dateOfBirth.label', default: 'Date Of Birth')}" />
					
						<g:sortableColumn property="dateOfDeath" title="${message(code: 'verbalAutopsyControl.dateOfDeath.label', default: 'Date Of Death')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${verbalAutopsyControlInstanceList}" status="i" var="verbalAutopsyControlInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${verbalAutopsyControlInstance.id}">${fieldValue(bean: verbalAutopsyControlInstance, field: "extId")}</g:link></td>
					
						<td>${fieldValue(bean: verbalAutopsyControlInstance, field: "code")}</td>
					
						<td>${fieldValue(bean: verbalAutopsyControlInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: verbalAutopsyControlInstance, field: "gender")}</td>
					
						<td><g:formatDate date="${verbalAutopsyControlInstance.dateOfBirth}" format="yyyy-MM-dd" /></td>
					
						<td><g:formatDate date="${verbalAutopsyControlInstance.dateOfDeath}" format="yyyy-MM-dd" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${verbalAutopsyControlInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
