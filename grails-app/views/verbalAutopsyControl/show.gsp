
<%@ page import="net.openva.server.model.VerbalAutopsyControl" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'verbalAutopsyControl.label', default: 'VerbalAutopsyControl')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>

		<asset:stylesheet src="main_content.css"/>
	</head>
	<body>
		<a href="#show-verbalAutopsyControl" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav_menu" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="verbalAutopsyControl.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="verbalAutopsyControl.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-verbalAutopsyControl" class="content scaffold-show" role="main">
			<h1><g:message code="verbalAutopsyControl.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list verbalAutopsyControl">
			
				<g:if test="${verbalAutopsyControlInstance?.extId}">
				<li class="fieldcontain">
					<span id="extId-label" class="property-label"><g:message code="verbalAutopsyControl.extId.label" default="Ext Id" /></span>
					
						<span class="property-value" aria-labelledby="extId-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="extId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.code}">
				<li class="fieldcontain">
					<span id="code-label" class="property-label"><g:message code="verbalAutopsyControl.code.label" default="Code" /></span>
					
						<span class="property-value" aria-labelledby="code-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="code"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="verbalAutopsyControl.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.gender}">
				<li class="fieldcontain">
					<span id="gender-label" class="property-label"><g:message code="verbalAutopsyControl.gender.label" default="Gender" /></span>
					
						<span class="property-value" aria-labelledby="gender-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="gender"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.dateOfBirth}">
				<li class="fieldcontain">
					<span id="dateOfBirth-label" class="property-label"><g:message code="verbalAutopsyControl.dateOfBirth.label" default="Date Of Birth" /></span>
					
						<span class="property-value" aria-labelledby="dateOfBirth-label"><g:formatDate date="${verbalAutopsyControlInstance?.dateOfBirth}" format="yyyy-MM-dd" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.dateOfDeath}">
				<li class="fieldcontain">
					<span id="dateOfDeath-label" class="property-label"><g:message code="verbalAutopsyControl.dateOfDeath.label" default="Date Of Death" /></span>
					
						<span class="property-value" aria-labelledby="dateOfDeath-label"><g:formatDate date="${verbalAutopsyControlInstance?.dateOfDeath}"  format="yyyy-MM-dd"  /></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.ageAtDeath}">
				<li class="fieldcontain">
					<span id="ageAtDeath-label" class="property-label"><g:message code="verbalAutopsyControl.ageAtDeath.label" default="Age At Death" /></span>
					
						<span class="property-value" aria-labelledby="ageAtDeath-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="ageAtDeath"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.motherId}">
				<li class="fieldcontain">
					<span id="motherId-label" class="property-label"><g:message code="verbalAutopsyControl.motherId.label" default="Mother Id" /></span>
					
						<span class="property-value" aria-labelledby="motherId-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="motherId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.motherName}">
				<li class="fieldcontain">
					<span id="motherName-label" class="property-label"><g:message code="verbalAutopsyControl.motherName.label" default="Mother Name" /></span>
					
						<span class="property-value" aria-labelledby="motherName-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="motherName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.fatherId}">
				<li class="fieldcontain">
					<span id="fatherId-label" class="property-label"><g:message code="verbalAutopsyControl.fatherId.label" default="Father Id" /></span>
					
						<span class="property-value" aria-labelledby="fatherId-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="fatherId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.fatherName}">
				<li class="fieldcontain">
					<span id="fatherName-label" class="property-label"><g:message code="verbalAutopsyControl.fatherName.label" default="Father Name" /></span>
					
						<span class="property-value" aria-labelledby="fatherName-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="fatherName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.householdId}">
				<li class="fieldcontain">
					<span id="householdId-label" class="property-label"><g:message code="verbalAutopsyControl.householdId.label" default="Household Id" /></span>
					
						<span class="property-value" aria-labelledby="householdId-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="householdId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.householdNo}">
				<li class="fieldcontain">
					<span id="householdNo-label" class="property-label"><g:message code="verbalAutopsyControl.householdNo.label" default="Household No" /></span>
					
						<span class="property-value" aria-labelledby="householdNo-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="householdNo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.gpsAccuracy}">
				<li class="fieldcontain">
					<span id="gpsAccuracy-label" class="property-label"><g:message code="verbalAutopsyControl.gpsAccuracy.label" default="Gps Accuracy" /></span>
					
						<span class="property-value" aria-labelledby="gpsAccuracy-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="gpsAccuracy"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.gpsAltitude}">
				<li class="fieldcontain">
					<span id="gpsAltitude-label" class="property-label"><g:message code="verbalAutopsyControl.gpsAltitude.label" default="Gps Altitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsAltitude-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="gpsAltitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.gpsLatitude}">
				<li class="fieldcontain">
					<span id="gpsLatitude-label" class="property-label"><g:message code="verbalAutopsyControl.gpsLatitude.label" default="Gps Latitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsLatitude-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="gpsLatitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.gpsLongitude}">
				<li class="fieldcontain">
					<span id="gpsLongitude-label" class="property-label"><g:message code="verbalAutopsyControl.gpsLongitude.label" default="Gps Longitude" /></span>
					
						<span class="property-value" aria-labelledby="gpsLongitude-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="gpsLongitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.deathUuid}">
				<li class="fieldcontain">
					<span id="deathUuid-label" class="property-label"><g:message code="verbalAutopsyControl.deathUuid.label" default="Death Uuid" /></span>
					
						<span class="property-value" aria-labelledby="deathUuid-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="deathUuid"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaType}">
				<li class="fieldcontain">
					<span id="vaType-label" class="property-label"><g:message code="verbalAutopsyControl.vaType.label" default="Va Type" /></span>
					
						<span class="property-value" aria-labelledby="vaType-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaUuid}">
				<li class="fieldcontain">
					<span id="vaUuid-label" class="property-label"><g:message code="verbalAutopsyControl.vaUuid.label" default="Va Uuid" /></span>
					
						<span class="property-value" aria-labelledby="vaUuid-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaUuid"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaCollected}">
				<li class="fieldcontain">
					<span id="vaCollected-label" class="property-label"><g:message code="verbalAutopsyControl.vaCollected.label" default="Va Collected" /></span>
					
						<span class="property-value" aria-labelledby="vaCollected-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaCollected"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaProcessedWith}">
				<li class="fieldcontain">
					<span id="vaProcessedWith-label" class="property-label"><g:message code="verbalAutopsyControl.vaProcessedWith.label" default="Va Processed With" /></span>
					
						<span class="property-value" aria-labelledby="vaProcessedWith-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaProcessedWith"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaProcessed}">
				<li class="fieldcontain">
					<span id="vaProcessed-label" class="property-label"><g:message code="verbalAutopsyControl.vaProcessed.label" default="Va Processed" /></span>
					
						<span class="property-value" aria-labelledby="vaProcessed-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaProcessed"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaResult}">
				<li class="fieldcontain">
					<span id="vaResult-label" class="property-label"><g:message code="verbalAutopsyControl.vaResult.label" default="Va Result" /></span>
					
						<span class="property-value" aria-labelledby="vaResult-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaResult"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.vaAttempts}">
				<li class="fieldcontain">
					<span id="vaAttempts-label" class="property-label"><g:message code="verbalAutopsyControl.vaAttempts.label" default="Va Attempts" /></span>
					
						<span class="property-value" aria-labelledby="vaAttempts-label"><g:fieldValue bean="${verbalAutopsyControlInstance}" field="vaAttempts"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${verbalAutopsyControlInstance?.collectionAttempts}">
				<li class="fieldcontain">
					<span id="collectionAttempts-label" class="property-label"><g:message code="verbalAutopsyControl.collectionAttempts.label" default="Collection Attempts" /></span>
					
						<g:each in="${verbalAutopsyControlInstance.collectionAttempts}" var="c">
						<span class="property-value" aria-labelledby="collectionAttempts-label"><g:link controller="uncompletedVA" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:verbalAutopsyControlInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${verbalAutopsyControlInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
