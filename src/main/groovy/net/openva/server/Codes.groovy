package net.openva.server

/**
 * Created by paul on 4/5/17.
 */
class Codes {

    static final int GROUP_ODK = 1
    static final int GROUP_IMPORT_DATA = 2
    static final int GROUP_UPDATE_VA = 3
    static final int GROUP_INTERPRET_VA = 4
    static final int GROUP_STATS = 5
    static final int GROUP_GENERATE_FILES = 6



    /*Codes for events of Data Import*/
    static final int REPORT_IMPORT_FROM_ODK_CORES = 10
    static final int REPORT_IMPORT_FROM_OPENHDS_FIELDWORKERS = 11
    static final int REPORT_IMPORT_FROM_OPENHDS_INDIVIDUALS = 12
    static final int REPORT_IMPORT_FROM_OPENHDS_DEATHS = 13

    /*Codes for events of VA Final Tables */
    static final int REPORT_UPDATE_VERBAL_AUTOPSY_CONTROL = 20

    /*Codes for events of Interpreting VA Final Tables - Cause of Death */
    static final int REPORT_INTERPRET_VERBAL_AUTOPSY_DATA = 30

    /*Codes for events of Generating VA Stats */
    static final int REPORT_VERBAL_AUTOPSY_STATS = 40

    /*Codes for events of Generating ZIP XML Files */
    static final int REPORT_GENERATE_USERS_ZIP_XML_FILES = 50
    static final int REPORT_GENERATE_INDIVIDUALS_ZIP_XML_FILES = 51
    static final int REPORT_GENERATE_VACONTROL_ZIP_XML_FILES = 52
    static final int REPORT_GENERATE_STATS_ZIP_XML_FILES = 53



    static final String PARAMS_OPENHDS_INDIVIDUAL_CUSTOM_CODE = "openhds.individual.custom_code"
    static final String PARAMS_OPENHDS_MINIMUM_DEATH_DATE = "openhds.death.min_date"
    static final String PARAMS_OPENVA_ODK_FORM_ID = "openva.odk.form_id"

}
