package net.openva.server.io

import net.openva.odk.model.Whova2016Core
import net.openva.odk.model.Whova2016Core2
import net.openva.odk.model.Whova2016Core3
import net.openva.odk.model.Whova2016Core4
import net.openva.odk.model.Whova2016Core5
import net.openva.odk.model.Whova2016Core6
import net.openva.odk.model.Whova2016Core7
import net.openva.odk.model.Whova2016Core8
import net.openva.server.model.WhoVa2016

/**
 * Created by paul on 4/6/17.
 */
class Copy {

    static fromTo(Map<String,Object> odkRawValues, WhoVa2016 to){
        def odkMap = OdkMapping.map

        odkRawValues.each { odkColumn, odkValue ->

            if (odkMap.containsKey(odkColumn)){
                def finalTableColumn = odkMap.get(odkColumn)
                to."${finalTableColumn}" = odkValue
                //println("${odkColumn} = ${odkValue}")

            }else {
                //println "the map doesnt contains ${odkColumn}"
            }
        }
    }

}
