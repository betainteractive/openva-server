package net.openva.server.io

/**
 * Has a map of all columns from ODK Raw tables to OpenVA Final tables columns
 * Was necessary to create this to handle an issue created by ODK Aggregate where the table columns were created on different tables for each ODK form deployment
 * It means for every different ODK Aggregate Server the database will be quite different, to avoid this issue a dynamic copy process was created, and this class is a part of it
 */
class OdkMapping {
    static LinkedHashMap<String, String> map = [
            'META_INSTANCE_ID':         'sysOdkMetaInstanceId',
            'META_INSTANCE_NAME':       'sysOdkMetaInstanceName',
            'DEVICEID':                 'sysOdkDeviceid',
            'START':                    'sysOdkStart',
            'END':                      'sysOdkEnd',
            'PHONENUMBER':              'sysOdkPhonenumber',
            'SIMSERIAL':                'sysOdkSimserial',

            'FIELD_WORKER_ID':	        'fieldWorkerId',
            'IS_PRE_REGISTERED':	    'isPreRegistered',
            'IS_VISIT_POSSIBLE':	    'isVisitPossible',
            'REASON_TO_NOT_COLLECT':	'reasonToNotCollect',
            'REASON_OTHER':	            'reasonOther',
            'HOUSEHOLD_ID':	            'householdId',
            'INDIVIDUAL_ID':	        'individualId',
            'GPS_ACC':	                'gpsAcc',
            'GPS_ALT':	                'gpsAlt',
            'GPS_LAT':	                'gpsLat',
            'GPS_LNG':	                'gpsLng',
            'PRESETS_ID10002':	        'id10002',
            'PRESETS_ID10003':	        'id10003',
            'PRESETS_ID10004':          'id10004',
            'RESPONDENT_BACKGR_ID10007':'id10007',
            'RESPONDENT_BACKGR_ID10008':'id10008',
            'RESPONDENT_BACKGR_ID10009':'id10009',
            'RESPONDENT_BACKGR_ID10010':'id10010',
            'RESPONDENT_BACKGR_ID10011':'id10011',
            'RESPONDENT_BACKGR_ID10012':'id10012',
            'RESPONDENT_BACKGR_ID10013':'id10013',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10017':	'id10017',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10018':	'id10018',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10019':	'id10019',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10020':	'id10020',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10021':	'id10021',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10022':	'id10022',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10023':	'id10023',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10024':	'id10024',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_DAYS':	        'ageInDays',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_DATE_EDIT':	        'dateEdit',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_YEARS':	    'ageInYears',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_YEARS_REMAIN':	'ageInYearsRemain',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_MONTHS':	    'ageInMonths',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_MONTHS_REMAIN':'ageInMonthsRemain',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_WEEKS':	    'ageInWeeks',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_NEONATAL1':	    'isNeonatal1',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_CHILD1':	        'isChild1',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_ADULT1':	        'isAdult1',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_DISPLAY_AGE_NEONATE':	'displayAgeNeonate',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_DISPLAY_AGE_CHILD':	'displayAgeChild',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_DISPLAY_AGE_ADULT':	'ageAdult',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_GROUP':	        'ageGroup',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_NEONATE_UNIT':	'ageNeonateUnit',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_NEONATE_DAYS':	'ageNeonateDays',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_NEONATE_HOURS':	'ageNeonateHours',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_NEONATE_MINUTES':	'ageNeonateMinutes',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_CHILD_UNIT':	    'ageChildUnit',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_CHILD_DAYS':	    'ageChildDays',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_MONTHS_BY_YEAR':	'ageInMonthsByYear',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_YEARS2':	    'ageInYears2',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_NEONATAL2':	    'isNeonatal2',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_CHILD_MONTHS':	'ageChildMonths',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_CHILD_YEARS':	    'ageChildYears',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_ADULT':	        'ageAdult',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_CHILD2':	        'isChild2',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_ADULT2':	        'isAdult2',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_NEONATAL':	        'isNeonatal',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_CHILD':	        'isChild',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_ADULT':	        'isAdult',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_NEONATE_IN_WEEKS':'ageNeonateInWeeks',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_CHILD_IN_WEEKS':	'ageChildInWeeks',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_CHILD_IN_MONTHS':	'illAgeChildInMonths',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_IS_CHILD_AGE_LESS_THAN_ONE':	'isChildAgeLessThanOne',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_IN_MONTH1':	'illAgeInMonth1',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_IN_MONTH2':	'illAgeInMonth2',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_IN_MONTH3':	'illAgeInMonth3',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_IN_MONTH':	'illAgeInMonth',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ILL_AGE_NEONATE_IN_MONTHS':	'illAgeNeonateInMonths',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_AGE_IN_DAYS_NEONATE':	'ageInDaysNeonate',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10051':	'id10051',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10052':	'id10052',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10053':	'id10053',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10054':	'id10054',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10055':	'id10055',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10056':	'id10056',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10057':	'id10057',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10058':	'id10058',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10059':	'id10059',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10060':	'id10060',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10061':	'id10061',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10062':	'id10062',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10063':	'id10063',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10064':	'id10064',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10065':	'id10065',
            'CONSNTD_DECEASED_CRVS_INFO_ON_DECSED_ID10066':	'id10066',
            'CONSENTED_DECEASED_CRVS_VITAL_REG_CERTIF_ID10069':	'id10069',
            'CONSENTED_DECEASED_CRVS_VITAL_REG_CERTIF_ID10070':	'id10070',
            'CONSENTED_DECEASED_CRVS_VITAL_REG_CERTIF_ID10071':	'id10071',
            'CONSENTED_DECEASED_CRVS_VITAL_REG_CERTIF_ID10072':	'id10072',
            'CONSENTED_DECEASED_CRVS_VITAL_REG_CERTIF_ID10073':	'id10073',
            'CONSENTED_INJURIES_ACCIDENTS_ID10077':	'id10077',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10079':	'id10079',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10080':	'id10080',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10081':	'id10081',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10082':	'id10082',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10083':	'id10083',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10084':	'id10084',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10085':	'id10085',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10086':	'id10086',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10087':	'id10087',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10088':	'id10088',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10089':	'id10089',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10090':	'id10090',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10091':	'id10091',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10092':	'id10092',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10093':	'id10093',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10094':	'id10094',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10095':	'id10095',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10096':	'id10096',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10097':	'id10097',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10098':	'id10098',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10099':	'id10099',
            'CONSENTED_INJURIES_ACCIDENTS_INJURIES_ACCIDENTS_YES_ID10100':	'id10100',
            'CONSENTED_STILLBIRTH_ID10104':	'id10104',
            'CONSENTED_STILLBIRTH_ID10105':	'id10105',
            'CONSENTED_STILLBIRTH_ID10106':	'id10106',
            'CONSENTED_STILLBIRTH_ID10107':	'id10107',
            'CONSENTED_STILLBIRTH_ID10108':	'id10108',
            'CONSENTED_STILLBIRTH_ID10109':	'id10109',
            'CONSENTED_STILLBIRTH_ID10110':	'id10110',
            'CONSENTED_STILLBIRTH_ID10111':	'id10111',
            'CONSENTED_STILLBIRTH_ID10112':	'id10112',
            'CONSENTED_STILLBIRTH_ID10113':	'id10113',
            'CONSENTED_STILLBIRTH_ID10114':	'id10114',
            'CONSENTED_STILLBIRTH_ID10115':	'id10115',
            'CONSENTED_STILLBIRTH_ID10116':	'id10116',
            'CONSENTED_ILLHISTORY_ILLNESS_LENGTH':	'illnessLength',
            'CONSENTED_ILLHISTORY_ID10120':	'id10120',
            'CONSENTED_ILLHISTORY_ID10121':	'id10121',
            'CONSENTED_ILLHISTORY_ID10122':	'id10122',
            'CONSENTED_ILLHISTORY_ID10123':	'id10123',
            'CONSENTED_ILLHISTORY_CHECK_DAYS':	'checkDays',
            'CONSENTED_ILLHISTORY_CHECK_MONTH':	'checkMonth',
            'CONSENTED_ILLHISTORY_CHECK_WEEKS':	'checkWeeks',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10125':	'id10125',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10126':	'id10126',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10127':	'id10127',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10128':	'id10128',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10129':	'id10129',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10130':	'id10130',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10131':	'id10131',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10132':	'id10132',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10133':	'id10133',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10134':	'id10134',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10135':	'id10135',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10136':	'id10136',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10137':	'id10137',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10138':	'id10138',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10139':	'id10139',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10140':	'id10140',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10141':	'id10141',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10142':	'id10142',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10143':	'id10143',
            'CONSENTED_ILLHISTORY_MED_HIST_FINAL_ILLNESS_ID10144':	'id10144',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10147':	'id10147',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10148':	'id10148',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10149':	'id10149',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10150':	'id10150',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10151':	'id10151',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10152':	'id10152',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10153':	'id10153',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10154':	'id10154',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10155':	'id10155',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10156':	'id10156',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10157':	'id10157',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10158':	'id10158',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10159':	'id10159',
            'CONSNTD_LLHSTORY_SIGNS_SYMPTOMS_FINAL_LLNSS_BRTHDUR_ID10161_UNIT':	'id10161Unit',
            'CONSNTD_LLHSTORY_SIGNS_SYMPTOMS_FINAL_LLNSS_BRTHDUR_ID10161':	'id10161',
            'CONSNTD_LLHSTORY_SIGNS_SYMPTOMS_FINAL_LLNSS_BRTHDUR_ID10162':	'id10162',
            'CONSNTD_LLHSTORY_SIGNS_SYMPTOMS_FINAL_LLNSS_BRTHDUR_ID10163':	'id10163',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10165':	'id10165',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10166':	'id10166',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10167':	'id10167',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10168':	'id10168',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10169':	'id10169',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10170':	'id10170',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10171':	'id10171',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10172':	'id10172',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10174':	'id10174',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10175':	'id10175',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10176':	'id10176',
            'CONSNTD_LLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNSS_PNDUR_ID10176_UNIT':	'id10176Unit',
            'CONSNTD_LLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNSS_PNDUR_ID10178':	'id10178',
            'CONSNTD_LLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNSS_PNDUR_ID10179':	'id10179',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10181':	'id10181',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10182':	'id10182',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10183':	'id10183',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10184':	'id10184',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10185':	'id10185',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10186':	'id10186',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10187':	'id10187',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10188':	'id10188',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10189':	'id10189',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10190':	'id10190',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10191':	'id10191',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10192':	'id10192',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10193':	'id10193',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10194':	'id10194',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10195':	'id10195',
            'CONSNTD_LLHSTR_SGNS_SYMPTOMS_FINAL_LLNSS_BDMNL_PIN_PAIN_DURATION':	'painDuration',
            'CONSNTD_LLHSTR_SGNS_SYMPTOMS_FINAL_LLNSS_BDMNL_PIN_ID10196':	'id10196',
            'CONSNTD_LLHSTR_SGNS_SYMPTOMS_FINAL_LLNSS_BDMNL_PIN_ID10197':	'id10197',
            'CONSNTD_LLHSTR_SGNS_SYMPTOMS_FINAL_LLNSS_BDMNL_PIN_ID10197B':	'id10197b',
            'CONSNTD_LLHSTR_SGNS_SYMPTOMS_FINAL_LLNSS_BDMNL_PIN_ID10198':	'id10198',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10199':	'id10199',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10200':	'id10200',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10201':	'id10201',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10202':	'id10202',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10203':	'id10203',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10204':	'id10204',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10205':	'id10205',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10206':	'id10206',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10207':	'id10207',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10208':	'id10208',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10209':	'id10209',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10210':	'id10210',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10211':	'id10211',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10212':	'id10212',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10213':	'id10213',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10214':	'id10214',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10215':	'id10215',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10216':	'id10216',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10217':	'id10217',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10218':	'id10218',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10219':	'id10219',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10220':	'id10220',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10221':	'id10221',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10222':	'id10222',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10223':	'id10223',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10224':	'id10224',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10225':	'id10225',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10226':	'id10226',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10227':	'id10227',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10228':	'id10228',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10229':	'id10229',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10230':	'id10230',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10231':	'id10231',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10232':	'id10232',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10233':	'id10233',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10234':	'id10234',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10236':	'id10236',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10237':	'id10237',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10238':	'id10238',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10239':	'id10239',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10240':	'id10240',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10241':	'id10241',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10242':	'id10242',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10243':	'id10243',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10244':	'id10244',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10245':	'id10245',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10246':	'id10246',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10247':	'id10247',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10248':	'id10248',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10249':	'id10249',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10250':	'id10250',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10251':	'id10251',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10252':	'id10252',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10253':	'id10253',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10254':	'id10254',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10255':	'id10255',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10256':	'id10256',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10257':	'id10257',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10258':	'id10258',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10259':	'id10259',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10261':	'id10261',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10262':	'id10262',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10263':	'id10263',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10264':	'id10264',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10265':	'id10265',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10266':	'id10266',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10267':	'id10267',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10268':	'id10268',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10269':	'id10269',
            'CONSENTED_ILLHISTORY_SIGNS_SYMPTOMS_FINAL_ILLNESS_ID10270':	'id10270',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10271':	'id10271',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10272':	'id10272',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10273':	'id10273',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10274':	'id10274',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10275':	'id10275',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10276':	'id10276',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10277':	'id10277',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10278':	'id10278',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10279':	'id10279',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10281':	'id10281',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10282':	'id10282',
            'CONSNTD_LLHSTR_SGNS_SMPTMS_FNL_LLNSS_RSM_F_CHLD_S_NDR_NE_ID10283':	'id10283',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10284':	'id10284',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10285':	'id10285',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10286':	'id10286',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10287':	'id10287',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10288':	'id10288',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10289':	'id10289',
            'CONSNTD_LLHSTRY_SIGNS_SYMPTOMS_FINAL_ILLNSS_NNTL_CHILD_C_ID10290':	'id10290',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10294':	'id10294',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10295':	'id10295',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10296':	'id10296',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10297':	'id10297',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10298':	'id10298',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10299':	'id10299',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10300':	'id10300',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10301':	'id10301',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10302':	'id10302',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10303':	'id10303',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10304':	'id10304',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10305':	'id10305',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10306':	'id10306',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10307':	'id10307',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10308':	'id10308',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10309':	'id10309',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_ID10310':	'id10310',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10312':	'id10312',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10313':	'id10313',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10314':	'id10314',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10315':	'id10315',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10316':	'id10316',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10317':	'id10317',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10318':	'id10318',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10319':	'id10319',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10320':	'id10320',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10321':	'id10321',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10322':	'id10322',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10323':	'id10323',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10324':	'id10324',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10325':	'id10325',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10326':	'id10326',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10327':	'id10327',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10328':	'id10328',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10329':	'id10329',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10330':	'id10330',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10331':	'id10331',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10332':	'id10332',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10333':	'id10333',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10334':	'id10334',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10335':	'id10335',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10336':	'id10336',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10337':	'id10337',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10338':	'id10338',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10339':	'id10339',
            'CONSENTED_ILLHISTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_ID10340':	'id10340',
            'CONSNTD_LLHSTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_DELVRTPE_ID10342':	'id10342',
            'CONSNTD_LLHSTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_DELVRTPE_ID10343':	'id10343',
            'CONSNTD_LLHSTORY_PREGNANCY_WOMEN_GROUP_MATERNAL_DELVRTPE_ID10344':	'id10344',
            'CONSENTED_ILLHISTORY_ID10347':	'id10347',
            'CONSENTED_ILLHISTORY_ID10351':	'id10351',
            'CONSENTED_ILLHISTORY_NEONATAL_CHILD_ID10352':	'id10352',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10354':	'id10354',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10355':	'id10355',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10356':	'id10356',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10357':	'id10357',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_MOTHER_DEATH':	'motherDeath',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10358':	'id10358',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10359':	'id10359',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10360':	'id10360',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10361':	'id10361',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10362':	'id10362',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10363':	'id10363',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10364':	'id10364',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10365':	'id10365',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10366':	'id10366',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10367':	'id10367',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10368':	'id10368',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10369':	'id10369',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10370':	'id10370',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10371':	'id10371',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10372':	'id10372',
            'CONSNTED_ILLHISTORY_NEONATAL_CHILD_NEONATAL_CHLD_A_ID10373':	'id10373',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10376':	'id10376',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10377':	'id10377',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_LAST_TIME_MOVED':	'lastTimeMoved',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10379':	'id10379',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10380':	'id10380',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10382':	'id10382',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10383':	'id10383',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10384':	'id10384',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10385':	'id10385',
            'CONSNTD_LLHSTRY_NEONATAL_CHILD_NEONATL_CHLD_B_MTHR_DLIV_ID10387':	'id10387',
            'CONSNTD_LLHSTRY_NEONATAL_CHILD_NEONATL_CHLD_B_MTHR_DLIV_ID10388':	'id10388',
            'CONSNTD_LLHSTRY_NEONATAL_CHILD_NEONATL_CHLD_B_MTHR_DLIV_ID10389':	'id10389',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10391':	'id10391',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10392':	'id10392',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10393':	'id10393',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10394':	'id10394',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10395':	'id10395',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10396':	'id10396',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10397':	'id10397',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10398':	'id10398',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10399':	'id10399',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10400':	'id10400',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10401':	'id10401',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10402':	'id10402',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10403':	'id10403',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10404':	'id10404',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10405':	'id10405',
            'CONSNTD_ILLHISTORY_NEONATAL_CHILD_NEONTL_CHILD_B_ID10406':	'id10406',
            'CONSENTED_ILLHISTORY_ID10408':	'id10408',
            'CONSENTED_ILLHISTORY_RISK_FACTORS_ID10411':	'id10411',
            'CONSENTED_ILLHISTORY_RISK_FACTORS_ID10412':	'id10412',
            'CONSENTED_ILLHISTORY_RISK_FACTORS_ID10413':	'id10413',
            'CONSENTED_ILLHISTORY_RISK_FACTORS_ID10414':	'id10414',
            'CONSENTED_ILLHISTORY_RISK_FACTORS_ID10415':	'id10415',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10418':	'id10418',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10419':	'id10419',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10420':	'id10420',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10421':	'id10421',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10422':	'id10422',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10423':	'id10423',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10424':	'id10424',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10425':	'id10425',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10426':	'id10426',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10427':	'id10427',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10428':	'id10428',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10429':	'id10429',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10430':	'id10430',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10431':	'id10431',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10432':	'id10432',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10434':	'id10434',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10435':	'id10435',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10436':	'id10436',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10437':	'id10437',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10438':	'id10438',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10439':	'id10439',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10440':	'id10440',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10441':	'id10441',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10442':	'id10442',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10443':	'id10443',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10444':	'id10444',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10445':	'id10445',
            'CONSENTED_ILLHISTORY_HEALTH_SERVICE_UTILIZATION_ID10446':	'id10446',
            'CONSENTED_BACK_CONTEXT_ID10450':	'id10450',
            'CONSENTED_BACK_CONTEXT_ID10451':	'id10451',
            'CONSENTED_BACK_CONTEXT_ID10452':	'id10452',
            'CONSENTED_BACK_CONTEXT_ID10453':	'id10453',
            'CONSENTED_BACK_CONTEXT_ID10454':	'id10454',
            'CONSENTED_BACK_CONTEXT_ID10455':	'id10455',
            'CONSENTED_BACK_CONTEXT_ID10456':	'id10456',
            'CONSENTED_BACK_CONTEXT_ID10457':	'id10457',
            'CONSENTED_BACK_CONTEXT_ID10458':	'id10458',
            'CONSENTED_BACK_CONTEXT_ID10459':	'id10459',
            'CONSENTED_DEATHCERT_ID10462':	'id10462',
            'CONSENTED_DEATHCERT_ID10463':	'id10463',
            'CONSENTED_DEATHCERT_ID10464':	'id10464',
            'CONSENTED_DEATHCERT_ID10465':	'id10465',
            'CONSENTED_DEATHCERT_ID10466':	'id10466',
            'CONSENTED_DEATHCERT_ID10467':	'id10467',
            'CONSENTED_DEATHCERT_ID10468':	'id10468',
            'CONSENTED_DEATHCERT_ID10469':	'id10469',
            'CONSENTED_DEATHCERT_ID10470':	'id10470',
            'CONSENTED_DEATHCERT_ID10471':	'id10471',
            'CONSENTED_DEATHCERT_ID10472':	'id10472',
            'CONSENTED_DEATHCERT_ID10473':	'id10473',
            'CONSENTED_NARRAT_ID10476':	'id10476',
            'ID10481':	'id10481'

    ]
}
