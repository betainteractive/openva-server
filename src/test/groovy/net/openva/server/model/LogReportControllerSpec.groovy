package net.openva.server.model

import grails.test.mixin.TestFor
import net.openva.server.model.logs.LogReportController
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(LogReportController)
class LogReportControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
