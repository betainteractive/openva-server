package net.openva.utilities.service

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(GeneralUtilitiesService)
class GeneralUtilitiesServiceSpec extends Specification {

    def generalUtilitiesService

    def setup() {
        println "using openhds - ${generalUtilitiesService.isUsingOpenHDS()}"
    }

    def cleanup() {
    }

    void "test something"() {
        println "using openhds - ${generalUtilitiesService.isUsingOpenHDS()}"
    }
}
