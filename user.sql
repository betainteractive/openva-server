CREATE USER 'openva'@'localhost' IDENTIFIED BY 'openva_pass';

GRANT ALL ON `openva`.* TO 'openva'@'%';
GRANT ALL ON `dssodk`.* TO 'openva'@'%';
GRANT ALL ON `openhds`.* TO 'openva'@'%';
flush privileges;