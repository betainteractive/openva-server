-- WHO VA 2016
-- Version: CHAMPS v2.004

create table openva.who_va_2016 (
        
        sys_odk_uri varchar(80) collate utf8_general_ci not null,
        sys_odk_creator_uri_user varchar(80) collate utf8_general_ci not null,
        sys_odk_creation_date datetime not null,
        sys_odk_last_update_uri_user varchar(80) collate utf8_general_ci,
        sys_odk_last_update_date datetime not null,
        sys_odk_model_version int(9),
        sys_odk_ui_version int(9),
        sys_odk_is_complete char(1) collate utf8_general_ci,
        sys_odk_submission_date datetime,
        sys_odk_marked_as_complete_date datetime,        
        sys_odk_meta_instance_id varchar(50) collate utf8_general_ci,
        sys_odk_meta_instance_name varchar(50) collate utf8_general_ci,
        
        sys_odk_deviceid varchar(50) collate utf8_general_ci,
        sys_odk_start datetime,
        sys_odk_end datetime,
        
        sys_odk_phonenumber varchar(55) collate utf8_general_ci,
        sys_odk_simserial varchar(100) collate utf8_general_ci,
        
        `field_worker_id` varchar(40) collate utf8_general_ci,
        
        `is_pre_registered` varchar(4) collate utf8_general_ci,        
        `is_visit_possible` varchar(4) collate utf8_general_ci,
        `reason_to_not_collect` varchar(40) collate utf8_general_ci,
        `reason_other` varchar(255) collate utf8_general_ci,
                
        `household_id` varchar(55) collate utf8_general_ci,
        `individual_id` varchar(55) collate utf8_general_ci,
        
        /*gps coordinates*/
        `gps_acc` decimal(38,10),
        `gps_alt` decimal(38,10),
        `gps_lat` decimal(38,10),
        `gps_lng` decimal(38,10),
        
        /*presets*/
        `id10002` varchar(10) collate utf8_general_ci,
        `id10003` varchar(10) collate utf8_general_ci,
        `id10004` varchar(10) collate utf8_general_ci,
        
        /*respondent_backgr*/
        `id10007` varchar(150) collate utf8_general_ci,
        `id10008` varchar(30) collate utf8_general_ci,
        `id10009` varchar(4) collate utf8_general_ci,
        `id10010` varchar(150) collate utf8_general_ci,
        `id10011` datetime,
        `id10012` datetime,
        `id10013` varchar(4) collate utf8_general_ci,
        
        /*info_on_deceased*/
        `id10017` varchar(100) collate utf8_general_ci,
        `id10018` varchar(100) collate utf8_general_ci,
        `id10019` varchar(10) collate utf8_general_ci,
        `id10020` varchar(4) collate utf8_general_ci,
        `id10021` datetime,
        `id10022` varchar(4) collate utf8_general_ci,
        `id10023` datetime,
        `id10024` datetime,
        
        /*calculations - start*/
        `age_in_days` varchar(20) collate utf8_general_ci,
        `date_edit` varchar(2) collate utf8_general_ci, /*note*/
        
        `age_in_years` varchar(20) collate utf8_general_ci,
        `age_in_years_remain` varchar(20) collate utf8_general_ci,
        `age_in_months` varchar(20) collate utf8_general_ci,
        `age_in_months_remain` varchar(20) collate utf8_general_ci,
        `age_in_weeks` varchar(20) collate utf8_general_ci,        
        `is_neonatal1` varchar(20) collate utf8_general_ci,
        `is_child1` varchar(20) collate utf8_general_ci,
        `is_adult1` varchar(20) collate utf8_general_ci,
        
        `display_age_adult` varchar(2) collate utf8_general_ci,   /*note*/
        `display_age_child` varchar(2) collate utf8_general_ci,   /*note*/
        `display_age_neonate` varchar(2) collate utf8_general_ci, /*note*/
        
        `age_group` varchar(20) collate utf8_general_ci,        
        `age_neonate_unit` varchar(20) collate utf8_general_ci,
        `age_neonate_days` int(9),
        `age_neonate_hours` int(9),
        `age_neonate_minutes` int(9),        
        `age_child_unit` varchar(20) collate utf8_general_ci,
        `age_child_days` int(9),        
        `age_in_months_by_year` varchar(20) collate utf8_general_ci,        
        `age_in_years2` varchar(20) collate utf8_general_ci,
        `is_neonatal2` varchar(20) collate utf8_general_ci,
        
        `age_child_months` int(9),        
        `age_child_years` int(9),
        `age_adult` int(9),        
        
        `is_child2` varchar(20) collate utf8_general_ci,        
        `is_adult2` varchar(20) collate utf8_general_ci,
              
        `is_neonatal` varchar(20) collate utf8_general_ci,
        `is_child` varchar(20) collate utf8_general_ci,                
        `is_adult` varchar(20) collate utf8_general_ci,  
        
        `age_neonate_in_weeks` varchar(20) collate utf8_general_ci,
        `age_child_in_weeks` varchar(20) collate utf8_general_ci,        
        
        `ill_age_child_in_months` varchar(20) collate utf8_general_ci,
        `is_child_age_less_than_one` varchar(20) collate utf8_general_ci,
        `ill_age_in_month1` varchar(20) collate utf8_general_ci,
        `ill_age_in_month2` varchar(20) collate utf8_general_ci,
        `ill_age_in_month3` varchar(20) collate utf8_general_ci,
        `ill_age_in_month` varchar(20) collate utf8_general_ci,
        `ill_age_neonate_in_months` varchar(20) collate utf8_general_ci,        
        `age_in_days_neonate` varchar(20) collate utf8_general_ci,
        
        /*calculations - end*/
        
        `id10051` varchar(4) collate utf8_general_ci,
        `id10052` varchar(40) collate utf8_general_ci,
        `id10053` varchar(100) collate utf8_general_ci,
        `id10054` varchar(100) collate utf8_general_ci,
        `id10055` varchar(155) collate utf8_general_ci,
        `id10056` varchar(155) collate utf8_general_ci,
        `id10057` varchar(155) collate utf8_general_ci,
        `id10058` varchar(40) collate utf8_general_ci,
        `id10059` varchar(40) collate utf8_general_ci,
        `id10060` datetime,
        `id10061` varchar(155) collate utf8_general_ci,
        `id10062` varchar(155) collate utf8_general_ci,
        `id10063` varchar(40) collate utf8_general_ci,
        `id10064` varchar(40) collate utf8_general_ci,
        `id10065` varchar(40) collate utf8_general_ci,
        `id10066` varchar(100) collate utf8_general_ci,
        
        /*vital_reg_certif*/
        `id10069` varchar(4) collate utf8_general_ci,
        `id10070` varchar(100) collate utf8_general_ci,
        `id10071` datetime,
        `id10072` varchar(100) collate utf8_general_ci,
        `id10073` varchar(100) collate utf8_general_ci,
        
        /*injuries_accidents*/
        `id10077` varchar(4) collate utf8_general_ci,
        /*injuries_accidents_yes*/
        `id10079` varchar(4) collate utf8_general_ci,
        `id10080` varchar(40) collate utf8_general_ci,
        `id10081` varchar(40) collate utf8_general_ci,
        `id10082` varchar(4) collate utf8_general_ci,
        `id10083` varchar(4) collate utf8_general_ci,
        `id10084` varchar(4) collate utf8_general_ci,
        `id10085` varchar(4) collate utf8_general_ci,
        `id10086` varchar(4) collate utf8_general_ci,
        `id10087` varchar(4) collate utf8_general_ci,
        `id10088` varchar(40) collate utf8_general_ci,
        `id10089` varchar(4) collate utf8_general_ci,
        `id10090` varchar(4) collate utf8_general_ci,
        `id10091` varchar(4) collate utf8_general_ci,
        `id10092` varchar(4) collate utf8_general_ci,
        `id10093` varchar(4) collate utf8_general_ci,
        `id10094` varchar(4) collate utf8_general_ci,
        `id10095` varchar(4) collate utf8_general_ci,
        `id10096` varchar(4) collate utf8_general_ci,
        `id10097` varchar(4) collate utf8_general_ci,
        `id10098` varchar(4) collate utf8_general_ci,
        `id10099` varchar(4) collate utf8_general_ci,
        `id10100` varchar(4) collate utf8_general_ci,
        
        /*stillbirth*/
        `id10104` varchar(4) collate utf8_general_ci,
        `id10105` varchar(4) collate utf8_general_ci,
        `id10106` int(9),
        `id10107` varchar(4) collate utf8_general_ci,
        `id10108` int(9),
        `id10109` varchar(4) collate utf8_general_ci,
        `id10110` varchar(4) collate utf8_general_ci,
        `id10111` varchar(4) collate utf8_general_ci,
        `id10112` varchar(4) collate utf8_general_ci,
        `id10113` varchar(4) collate utf8_general_ci,
        `id10114` varchar(4) collate utf8_general_ci,
        `id10115` varchar(4) collate utf8_general_ci,
        `id10116` varchar(4) collate utf8_general_ci,
        
        /*illhistory*/
        
        /*Id10351 - is here on odk - but on sql is ordered*/
        /*Id10408 - is here on odk - but on sql is ordered*/
        
        `illness_length` varchar(10) collate utf8_general_ci,
        
        `id10120` int(9),        
        `id10121` int(9),
        `id10122` int(9),
        `id10123` varchar(4) collate utf8_general_ci,
        
        `check_days` varchar(2) collate utf8_general_ci,  /*note*/
        `check_month` varchar(2) collate utf8_general_ci, /*note*/
        `check_weeks` varchar(2) collate utf8_general_ci, /*note*/
        
        /*med_hist_final_illness*/
        `id10125` varchar(4) collate utf8_general_ci,
        `id10126` varchar(4) collate utf8_general_ci,
        `id10127` varchar(4) collate utf8_general_ci,
        `id10128` varchar(4) collate utf8_general_ci,
        `id10129` varchar(4) collate utf8_general_ci,
        `id10130` varchar(4) collate utf8_general_ci,
        `id10131` varchar(4) collate utf8_general_ci,
        `id10132` varchar(4) collate utf8_general_ci,
        `id10133` varchar(4) collate utf8_general_ci,
        `id10134` varchar(4) collate utf8_general_ci,
        `id10135` varchar(4) collate utf8_general_ci,
        `id10136` varchar(4) collate utf8_general_ci,
        `id10137` varchar(4) collate utf8_general_ci,
        `id10138` varchar(4) collate utf8_general_ci,
        `id10139` varchar(4) collate utf8_general_ci,
        `id10140` varchar(4) collate utf8_general_ci,
        `id10141` varchar(4) collate utf8_general_ci,
        `id10142` varchar(4) collate utf8_general_ci,
        `id10143` varchar(4) collate utf8_general_ci,
        `id10144` varchar(4) collate utf8_general_ci,
        
        /*signs_symptoms_final_illness*/
        `id10147` varchar(4) collate utf8_general_ci,
        `id10148` int(9),
        `id10149` varchar(4) collate utf8_general_ci,
        `id10150` varchar(10) collate utf8_general_ci,
        `id10151` varchar(10) collate utf8_general_ci,
        `id10152` varchar(4) collate utf8_general_ci,
        `id10153` varchar(4) collate utf8_general_ci,
        `id10154` int(9),
        `id10155` varchar(4) collate utf8_general_ci,
        `id10156` varchar(4) collate utf8_general_ci,
        `id10157` varchar(4) collate utf8_general_ci,
        `id10158` varchar(4) collate utf8_general_ci,
        `id10159` varchar(4) collate utf8_general_ci,
        
        /*breathdur*/
        `id10161_0` int(9),
        `id10161_unit` varchar(10) collate utf8_general_ci,
        `id10161_1` int(9),        
        `id10161` varchar(5) collate utf8_general_ci,
        `id10162` int(9),
        `id10163` int(9),
        
        `id10165` varchar(10) collate utf8_general_ci,
        `id10166` varchar(4) collate utf8_general_ci,
        `id10167` int(9),
        `id10168` varchar(4) collate utf8_general_ci,
        `id10169` int(9),
        `id10170` varchar(4) collate utf8_general_ci,
        `id10171` varchar(4) collate utf8_general_ci,
        `id10172` varchar(4) collate utf8_general_ci,
        /*id10173 - select-multiple odk=select_501 */
        id10173_1_st varchar(4) collate utf8_general_ci, /* option = stridor */        
        id10173_2_gr varchar(4) collate utf8_general_ci, /* option = grunting */
        id10173_3_wz varchar(4) collate utf8_general_ci, /* option = wheezing */
        id10173_4_no varchar(4) collate utf8_general_ci, /* option = no */
        id10173_5_dk varchar(4) collate utf8_general_ci, /* option = dk */
        id10173_6_rf varchar(4) collate utf8_general_ci, /* option = ref */      
        `id10174` varchar(4) collate utf8_general_ci,
        `id10175` varchar(4) collate utf8_general_ci,
        `id10176` int(9),
        
        /*paindur*/
        `id10176_unit` varchar(10) collate utf8_general_ci,        
        `id10178` int(9),
        `id10179` int(9),
        `id10179_1` int(9),
        
        `id10181` varchar(4) collate utf8_general_ci,
        `id10182` int(9),
        `id10183` int(9),
        `id10184` int(9),
        `id10185` varchar(4) collate utf8_general_ci,
        `id10186` varchar(4) collate utf8_general_ci,
        `id10187` varchar(4) collate utf8_general_ci,
        `id10188` varchar(4) collate utf8_general_ci,
        `id10189` varchar(4) collate utf8_general_ci,
        `id10190` int(9),
        `id10191` varchar(4) collate utf8_general_ci,
        `id10192` varchar(4) collate utf8_general_ci,
        `id10193` varchar(4) collate utf8_general_ci,
        `id10194` varchar(4) collate utf8_general_ci,        
        `id10195` varchar(4) collate utf8_general_ci,
        
        /*abdominal_pain*/
        `pain_duration` varchar(10) collate utf8_general_ci, /*for how long did (s)he have belly abdominal pain*/        
        `id10196` int(9),
        `id10197` int(9),
        `id10197b` int(9),
        `id10198` int(9),
        
        `id10199` varchar(40) collate utf8_general_ci,
        `id10200` varchar(4) collate utf8_general_ci,
        `id10201` int(9),
        `id10202` int(9),
        `id10203` varchar(10) collate utf8_general_ci,
        `id10204` varchar(4) collate utf8_general_ci,
        `id10205` int(9),
        `id10206` int(9),
        `id10207` varchar(4) collate utf8_general_ci,
        `id10208` varchar(4) collate utf8_general_ci,
        `id10209` int(9),
        `id10210` varchar(4) collate utf8_general_ci,
        `id10211` int(9),
        `id10212` varchar(4) collate utf8_general_ci,
        `id10213` int(9),
        `id10214` varchar(4) collate utf8_general_ci,
        `id10215` varchar(4) collate utf8_general_ci,
        `id10216` int(9),
        `id10217` varchar(4) collate utf8_general_ci,
        `id10218` varchar(4) collate utf8_general_ci,
        `id10219` varchar(4) collate utf8_general_ci,
        `id10220` varchar(4) collate utf8_general_ci,
        `id10221` int(9),
        `id10222` varchar(4) collate utf8_general_ci,
        `id10223` varchar(4) collate utf8_general_ci,
        `id10224` varchar(4) collate utf8_general_ci,
        `id10225` varchar(4) collate utf8_general_ci,
        `id10226` varchar(4) collate utf8_general_ci,
        `id10227` varchar(4) collate utf8_general_ci,
        `id10228` varchar(4) collate utf8_general_ci,
        `id10229` varchar(4) collate utf8_general_ci,
        `id10230` varchar(4) collate utf8_general_ci,
        `id10231` varchar(4) collate utf8_general_ci,
        `id10232` int(9),
        `id10233` varchar(4) collate utf8_general_ci,
        `id10234` int(9),
        /*id10235 - select-multiple  odk=select_135 */
        id10235_1_fc varchar(4) collate utf8_general_ci, /* option = face */
        id10235_2_tk varchar(4) collate utf8_general_ci, /* option = trunk */
        id10235_3_et varchar(4) collate utf8_general_ci, /* option = extremities */
        id10235_4_ev varchar(4) collate utf8_general_ci, /* option = everywhere */
        
        
        /*Table Splitted from here*/
        
        `processed` int(9),
        
        primary key (sys_odk_uri),
        index sys_odk_last_update_date (sys_odk_last_update_date),
        index sys_odk_marked_as_complete_date (sys_odk_marked_as_complete_date),
        index individual_id (individual_id)
    )
    engine=innodb default charset=latin1;

create table openva.who_va_2016_ext /* extension of who_va_2016 */
    (
        
        -- sys_odk_uri varchar(80) collate utf8_general_ci not null, /**/
        sys_odk_parent_uri varchar(80) collate utf8_general_ci not null,
                
        `id10236` varchar(4) collate utf8_general_ci,
        `id10237` varchar(4) collate utf8_general_ci,
        `id10238` varchar(4) collate utf8_general_ci,
        `id10239` varchar(4) collate utf8_general_ci,
        `id10240` varchar(4) collate utf8_general_ci,
        `id10241` varchar(4) collate utf8_general_ci,
        `id10242` varchar(4) collate utf8_general_ci,
        `id10243` varchar(4) collate utf8_general_ci,
        `id10244` varchar(4) collate utf8_general_ci,
        `id10245` varchar(4) collate utf8_general_ci,
        `id10246` varchar(4) collate utf8_general_ci,
        `id10247` varchar(4) collate utf8_general_ci,
        `id10248` int(9),
        `id10249` varchar(4) collate utf8_general_ci,
        `id10250` int(9),
        `id10251` varchar(4) collate utf8_general_ci,
        `id10252` varchar(4) collate utf8_general_ci,
        `id10253` varchar(4) collate utf8_general_ci,
        `id10254` varchar(4) collate utf8_general_ci,
        `id10255` varchar(4) collate utf8_general_ci,
        `id10256` varchar(4) collate utf8_general_ci,
        `id10257` varchar(4) collate utf8_general_ci,
        `id10258` varchar(4) collate utf8_general_ci,
        `id10259` varchar(4) collate utf8_general_ci,
        /*id10260 - select-multiple  odk=select_161 */
        id10260_1_rs varchar(4) collate utf8_general_ci, /* option = right_side */        
        id10260_2_ls varchar(4) collate utf8_general_ci, /* option = left_side */
        id10260_3_lp varchar(4) collate utf8_general_ci, /* option = lower_part_of_body */
        id10260_4_up varchar(4) collate utf8_general_ci, /* option = upper_part_of_body */
        id10260_5_ol varchar(4) collate utf8_general_ci, /* option = one_leg_only */
        id10260_6_oa varchar(4) collate utf8_general_ci, /* option = one_arm_only */
        id10260_7_wb varchar(4) collate utf8_general_ci, /* option = whole_body */
        id10260_8_ot varchar(4) collate utf8_general_ci, /* option = other */
        `id10261` varchar(4) collate utf8_general_ci,
        `id10262` int(9),
        `id10263` varchar(10) collate utf8_general_ci,
        `id10264` varchar(4) collate utf8_general_ci,
        `id10265` varchar(4) collate utf8_general_ci,
        `id10266` int(9),
        `id10267` varchar(4) collate utf8_general_ci,
        `id10268` varchar(4) collate utf8_general_ci,
        `id10269` varchar(4) collate utf8_general_ci,
        `id10270` varchar(4) collate utf8_general_ci,
        
        /*resume_if_child_is_under_one*/
        `id10271` varchar(4) collate utf8_general_ci,
        `id10272` varchar(4) collate utf8_general_ci,
        `id10273` varchar(4) collate utf8_general_ci,
        `id10274` int(9),
        `id10275` varchar(4) collate utf8_general_ci,
        `id10276` varchar(4) collate utf8_general_ci,
        `id10277` varchar(4) collate utf8_general_ci,
        `id10278` varchar(4) collate utf8_general_ci,
        `id10279` varchar(4) collate utf8_general_ci,
        `id10281` varchar(4) collate utf8_general_ci,
        `id10282` varchar(4) collate utf8_general_ci,
        `id10283` varchar(4) collate utf8_general_ci,
        
        /*neonatal_childC*/
        `id10284` varchar(4) collate utf8_general_ci,
        `id10285` int(9),
        `id10286` varchar(4) collate utf8_general_ci,
        `id10287` varchar(4) collate utf8_general_ci,
        `id10288` varchar(4) collate utf8_general_ci,
        `id10289` varchar(4) collate utf8_general_ci,
        `id10290` varchar(4) collate utf8_general_ci,
        
        /*pregnancy_women*/
        `id10294` varchar(4) collate utf8_general_ci,
        `id10295` varchar(4) collate utf8_general_ci,
        `id10296` varchar(4) collate utf8_general_ci,
        `id10297` varchar(4) collate utf8_general_ci,
        `id10298` varchar(4) collate utf8_general_ci,
        `id10299` varchar(4) collate utf8_general_ci,
        `id10300` varchar(4) collate utf8_general_ci,
        `id10301` varchar(4) collate utf8_general_ci,
        `id10302` varchar(4) collate utf8_general_ci,
        `id10303` int(9),
        `id10304` varchar(4) collate utf8_general_ci,
        `id10305` varchar(4) collate utf8_general_ci,
        `id10306` varchar(4) collate utf8_general_ci,
        `id10307` varchar(4) collate utf8_general_ci,
        `id10308` varchar(4) collate utf8_general_ci,
        `id10309` int(9),
        `id10310` varchar(4) collate utf8_general_ci,
        
        /*group_maternal*/
        `id10312` varchar(4) collate utf8_general_ci,
        `id10313` varchar(4) collate utf8_general_ci,
        `id10314` varchar(4) collate utf8_general_ci,
        `id10315` varchar(4) collate utf8_general_ci,
        `id10316` varchar(4) collate utf8_general_ci,
        `id10317` varchar(4) collate utf8_general_ci,
        `id10318` varchar(4) collate utf8_general_ci,
        `id10319` int(9),
        `id10320` varchar(4) collate utf8_general_ci,
        `id10321` varchar(4) collate utf8_general_ci,
        `id10322` varchar(4) collate utf8_general_ci,
        `id10323` varchar(4) collate utf8_general_ci,
        `id10324` varchar(4) collate utf8_general_ci,
        `id10325` varchar(4) collate utf8_general_ci,
        `id10326` varchar(4) collate utf8_general_ci,
        `id10327` varchar(4) collate utf8_general_ci,
        `id10328` varchar(4) collate utf8_general_ci,
        `id10329` varchar(4) collate utf8_general_ci,
        `id10330` varchar(4) collate utf8_general_ci,
        `id10331` varchar(4) collate utf8_general_ci,
        `id10332` int(9),
        `id10333` varchar(4) collate utf8_general_ci,
        `id10334` varchar(4) collate utf8_general_ci,
        `id10335` varchar(4) collate utf8_general_ci,
        `id10336` varchar(4) collate utf8_general_ci,
        `id10337` varchar(40) collate utf8_general_ci,
        `id10338` varchar(4) collate utf8_general_ci,
        `id10339` varchar(40) collate utf8_general_ci,
        `id10340` varchar(4) collate utf8_general_ci,
        
        /*deliverytype*/
        `id10342` varchar(4) collate utf8_general_ci,
        `id10343` varchar(4) collate utf8_general_ci,
        `id10344` varchar(4) collate utf8_general_ci,
        
        `id10347` varchar(4) collate utf8_general_ci,
        
        /*neonatal_child*/
        `id10351` int(9), /*in odk is not here*/
        `id10352` int(9),
        
        /*neonatal_childA*/
        `id10354` varchar(4) collate utf8_general_ci,
        `id10355` varchar(40) collate utf8_general_ci,
        `id10356` varchar(4) collate utf8_general_ci,        
        `id10357` varchar(40) collate utf8_general_ci,
        `mother_death` varchar(4) collate utf8_general_ci, /* how long after delivery the mother died*/
        `id10358` int(9),
        `id10358_1` int(9),
        `id10359` int(9),
        `id10360` varchar(40) collate utf8_general_ci,
        `id10361` varchar(4) collate utf8_general_ci,
        `id10362` varchar(4) collate utf8_general_ci,
        `id10363` varchar(4) collate utf8_general_ci,
        `id10364` varchar(4) collate utf8_general_ci,
        `id10365` varchar(4) collate utf8_general_ci,
        `id10366` int(9),
        `id10367` int(9),
        `id10368` varchar(4) collate utf8_general_ci,
        `id10369` varchar(4) collate utf8_general_ci,
        `id10370` varchar(4) collate utf8_general_ci,
        `id10371` varchar(4) collate utf8_general_ci,
        `id10372` varchar(4) collate utf8_general_ci,
        `id10373` varchar(4) collate utf8_general_ci,
        /*on odk we have Id10408_1 here - but in sql is ordered*/
        
        /*neonatal_childB*/
        `id10376` varchar(255) collate utf8_general_ci,
        `id10377` varchar(255) collate utf8_general_ci,        
        `last_time_moved` varchar(255) collate utf8_general_ci, /*when did the baby last moved*/        
        `id10379` int(9),
        `id10380` int(9),
        `id10382` int(9),
        `id10383` varchar(4) collate utf8_general_ci,
        `id10384` varchar(4) collate utf8_general_ci,
        `id10385` varchar(20) collate utf8_general_ci,
        
        /*mother_deliv*/
        `id10387` varchar(4) collate utf8_general_ci,
        `id10388` varchar(4) collate utf8_general_ci,
        `id10389` varchar(4) collate utf8_general_ci,
        
        `id10391` varchar(4) collate utf8_general_ci,
        `id10392` int(9),
        `id10393` varchar(4) collate utf8_general_ci,
        `id10394` int(9),
        `id10395` varchar(4) collate utf8_general_ci,
        `id10396` varchar(4) collate utf8_general_ci,
        `id10397` varchar(4) collate utf8_general_ci,
        `id10398` varchar(4) collate utf8_general_ci,
        `id10399` varchar(4) collate utf8_general_ci,
        `id10400` varchar(4) collate utf8_general_ci,
        `id10401` varchar(4) collate utf8_general_ci,
        `id10402` varchar(4) collate utf8_general_ci,
        `id10403` varchar(4) collate utf8_general_ci,
        `id10404` varchar(4) collate utf8_general_ci,
        `id10405` varchar(4) collate utf8_general_ci,
        `id10406` varchar(4) collate utf8_general_ci,
        
        /*risk_factors*/
        `id10408` varchar(4) collate utf8_general_ci,
        `id10408_1` varchar(4) collate utf8_general_ci,        
        `id10411` varchar(4) collate utf8_general_ci,
        `id10412` varchar(4) collate utf8_general_ci,
        `id10413` varchar(4) collate utf8_general_ci,
        `id10414` varchar(40) collate utf8_general_ci,
        `id10415` int(9),
        
        /*health_service_utilization*/
        `id10418` varchar(4) collate utf8_general_ci,
        `id10419` varchar(4) collate utf8_general_ci,
        `id10420` varchar(4) collate utf8_general_ci,
        `id10421` varchar(4) collate utf8_general_ci,
        `id10422` varchar(4) collate utf8_general_ci,
        `id10423` varchar(4) collate utf8_general_ci,
        `id10424` varchar(4) collate utf8_general_ci,
        `id10425` varchar(4) collate utf8_general_ci,
        `id10426` varchar(4) collate utf8_general_ci,
        `id10427` varchar(4) collate utf8_general_ci,
        `id10428` varchar(4) collate utf8_general_ci,
        `id10429` varchar(4) collate utf8_general_ci,
        `id10430` varchar(4) collate utf8_general_ci,
        `id10431` varchar(155) collate utf8_general_ci,
        `id10432` varchar(4) collate utf8_general_ci,
        /*id10433 - select-multiple  odk=select_322*/        
        id10433_1_th varchar(4) collate utf8_general_ci, /* option = traditional_healer */
        id10433_2_hp varchar(4) collate utf8_general_ci, /* option = homeopath */
        id10433_3_rl varchar(4) collate utf8_general_ci, /* option = religious_leader */
        id10433_4_gh varchar(4) collate utf8_general_ci, /* option = government_hospital */
        id10433_5_gc varchar(4) collate utf8_general_ci, /* option = government_health_center_or_clinic */
        id10433_6_ph varchar(4) collate utf8_general_ci, /* option = private_hospital */
        id10433_7_cp varchar(4) collate utf8_general_ci, /* option = community_based_practitionerinsystem */
        id10433_8_ta varchar(4) collate utf8_general_ci, /* option = trained_birth_attendant */
        id10433_9_pp varchar(4) collate utf8_general_ci, /* option = private_physician */
        id10433_10_rf varchar(4) collate utf8_general_ci, /* option = relative_friend */
        id10433_11_ph varchar(4) collate utf8_general_ci, /* option = pharmacy */
        id10433_12_dk varchar(4) collate utf8_general_ci, /* option = dk */
        id10433_13_rf varchar(4) collate utf8_general_ci, /* option = ref */
        `id10434` varchar(200) collate utf8_general_ci,
        `id10435` varchar(4) collate utf8_general_ci,
        `id10436` varchar(200) collate utf8_general_ci,
        `id10437` varchar(4) collate utf8_general_ci,
        `id10438` varchar(4) collate utf8_general_ci,
        `id10439` datetime,
        `id10440` datetime,
        `id10441` datetime,
        `id10442` decimal(38,10),
        `id10443` decimal(38,10),
        `id10444` varchar(200) collate utf8_general_ci,
        `id10445` varchar(4) collate utf8_general_ci,
        `id10446` varchar(4) collate utf8_general_ci,
        
        /*back_context*/
        `id10450` varchar(4) collate utf8_general_ci,        
        `id10451` varchar(4) collate utf8_general_ci,
        `id10452` varchar(4) collate utf8_general_ci,
        `id10453` varchar(4) collate utf8_general_ci,
        `id10454` varchar(4) collate utf8_general_ci,
        `id10455` varchar(4) collate utf8_general_ci,
        `id10456` varchar(4) collate utf8_general_ci,
        `id10457` varchar(4) collate utf8_general_ci,
        `id10458` varchar(4) collate utf8_general_ci,
        `id10459` varchar(4) collate utf8_general_ci,
        
        /*deathcert*/
        `id10462` varchar(4) collate utf8_general_ci,
        `id10463` varchar(4) collate utf8_general_ci,
        `id10464` varchar(255) collate utf8_general_ci,
        `id10465` varchar(255) collate utf8_general_ci,
        `id10466` varchar(255) collate utf8_general_ci,
        `id10467` varchar(255) collate utf8_general_ci,
        `id10468` varchar(255) collate utf8_general_ci,
        `id10469` varchar(255) collate utf8_general_ci,
        `id10470` varchar(255) collate utf8_general_ci,
        `id10471` varchar(255) collate utf8_general_ci,
        `id10472` varchar(255) collate utf8_general_ci,
        `id10473` varchar(255) collate utf8_general_ci,
        `id10476` varchar(1000) collate utf8_general_ci,
        /*id10476a - audio variable not included on final table - requires revision*/
        /*id10477 - select-multiple  odk=select_510 */
        id10477_1_ck varchar(4) collate utf8_general_ci, /* option = Chronic_kidney_disease */        
        id10477_2_dl varchar(4) collate utf8_general_ci, /* option = Dialysis */
        id10477_3_fv varchar(4) collate utf8_general_ci, /* option = Fever */ 
        id10477_4_ha varchar(4) collate utf8_general_ci, /* option = Heart_attack */
        id10477_5_hp varchar(4) collate utf8_general_ci, /* option = Heart_problem */
        id10477_6_jd varchar(4) collate utf8_general_ci, /* option = Jaundice */
        id10477_7_lf varchar(4) collate utf8_general_ci, /* option = Liver_failure */
        id10477_8_ml varchar(4) collate utf8_general_ci, /* option = Malaria */
        id10477_9_pn varchar(4) collate utf8_general_ci, /* option = Pneumonia */
        id10477_10_rk varchar(4) collate utf8_general_ci, /* option = Renal_kidney_failure */
        id10477_11_sc varchar(4) collate utf8_general_ci, /* option = Suicide */
        id10477_12_no varchar(4) collate utf8_general_ci, /* option = None */
        id10477_13_dk varchar(4) collate utf8_general_ci, /* option = dk */ 
        /*id10478 - select-multiple odk=select_511 */
        id10478_1_ab varchar(4) collate utf8_general_ci, /* option = abdomen */  
        id10478_2_cc varchar(4) collate utf8_general_ci, /* option = cancer */
        id10478_3_dh varchar(4) collate utf8_general_ci, /* option = dehydration */
        id10478_4_dg varchar(4) collate utf8_general_ci, /* option = dengue */
        id10478_5_dr varchar(4) collate utf8_general_ci, /* option = diarrhea */
        id10478_6_fv varchar(4) collate utf8_general_ci, /* option = fever */
        id10478_7_hp varchar(4) collate utf8_general_ci, /* option = heart_problem */
        id10478_8_jd varchar(4) collate utf8_general_ci, /* option = jaundice */
        id10478_9_pn varchar(4) collate utf8_general_ci, /* option = pneumonia */
        id10478_10_rs varchar(4) collate utf8_general_ci, /* option = rash */
        id10478_11_no varchar(4) collate utf8_general_ci, /* option = None */
        id10478_12_dk varchar(4) collate utf8_general_ci, /* option = dk */
        /*id10479 - select-multiple odk=select_512 */        
        id10479_1_ax  varchar(4) collate utf8_general_ci, /* option = asphyxia */
        id10479_2_ib  varchar(4) collate utf8_general_ci, /* option = incubator */
        id10479_3_lp  varchar(4) collate utf8_general_ci, /* option = lung_problem */
        id10479_4_pn  varchar(4) collate utf8_general_ci, /* option = pneumonia */
        id10479_5_pd  varchar(4) collate utf8_general_ci, /* option = preterm_delivery */
        id10479_6_rd  varchar(4) collate utf8_general_ci, /* option = respiratory_distress */
        id10479_7_no  varchar(4) collate utf8_general_ci, /* option = None */
        id10479_8_dk  varchar(4) collate utf8_general_ci, /* option = dk */
        `id10481` datetime,
    
        primary key (sys_odk_parent_uri),       
        foreign key (sys_odk_parent_uri) references who_va_2016(sys_odk_uri) on delete cascade
    )
    engine=innodb default charset=utf8;